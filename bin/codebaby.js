#!/usr/bin/env node
'use strict';
const path = require( 'path' );
const fs = require( 'fs' );


let pathRoot = path.normalize( __dirname + '/../../../' );
let index = path.normalize( pathRoot + '/codebaby/core/index' );

fs.exists( index + '.js', exist => {

    if ( exist ) {
        require( index );
    } else {
        require( '../core/index' );
    }

} );











