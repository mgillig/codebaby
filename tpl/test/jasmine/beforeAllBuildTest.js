const {buildTest, endTest} = require('../../../../../codebaby/handler/test/libs/buildTest');
const bundleTest = require('../../../../../codebaby/handler/test/libs/bundleTest');
const path = require('path');

let jsdom = require( 'jsdom' );
const { JSDOM } = jsdom;
const { window } = new JSDOM( '<html> </html>' );
const $ = require( 'jquery' )( window );
let pathDirRel = path.relative('', path.resolve(__dirname, '../../../'));

let testConfig = {
    name: 'tape.test.js',
    rollup: 'src/js/app/rollup-config.js',
    input: 'src/js/app/core/test/entryTest.js',
    pathProject: pathDirRel,
    inject: [ '$', 'window' ]
};

// TYPESCRIPT !!!!
// testConfig.typescript = 'src/js/app/tsconfig.json';
// testConfig.input = 'src/js/app/_compiled/test/entryTest.js';




module.exports = {
    beforeAllBuildTest: function () {

        return new Promise((resolve => {
            bundleTest(testConfig).then((result) => {

                let code = require('../../../../../'+result.path);
                resolve(code());
            });
        }));

    },
    complete: function () {
        endTest();
    }
};