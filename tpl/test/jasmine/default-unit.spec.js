const {beforeAllBuildTest} = require('./beforeAllBuildTest');

describe( 'signals', function() {

    let signals;

    beforeAll( function( done ) {

        beforeAllBuildTest().then(code => {
            signals = code.signals;
            done();
        });
    } );

    it( '------------------------ 01 interface ', function() {

        expect( typeof signals.on ).toBe( 'function' );
        expect( typeof signals.off ).toBe( 'function' );
        expect( typeof signals.send ).toBe( 'function' );
        expect( typeof signals.send ).not.toBe( undefined );

        // jasmine-expect
        expect( signals.send ).toBeFunction();
    } );

    it( '------------------------ 02 equal ', function() {

        expect( true ).not.toBe(false);
        expect( true ).toBe(true);
        // jasmine-expect
        expect( true ).toBeTruthy();


    } );
} );


// JASMINE EXPECTATIONS
// https://jasmine.github.io/edge/introduction.html#section-Expectations
// https://www.npmjs.com/package/jasmine-expect


