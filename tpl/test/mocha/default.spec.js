const  assert = require('assert');



describe('Array', function() {

    describe('#indexOf()', function() {

        it('------------------------ 01 test ', function() {

            assert.equal([1,2,3].indexOf(4), -1);
        });
        it('------------------------ 02 test ', function() {

            assert.equal([1,2,3].indexOf(2), 1);
        });
    });
});


// mocha
// https://www.npmjs.com/package/mocha