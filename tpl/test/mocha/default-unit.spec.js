const {beforeAllBuildTest} = require('./beforeAllBuildTest');
const assert = require('assert');

let signals;

describe('signals', function () {

    beforeEach(function (done) {
        beforeAllBuildTest().then(code => {


            signals = code.signals;
            done();
        });
    });


    it('------------------------ 01 interface ', function () {

        assert.equal(typeof signals.on, 'function');
        assert.equal(typeof signals.on, 'function');
        assert.equal(typeof signals.off, 'function');
        assert.equal(typeof signals.send, 'function');
        assert.notEqual(typeof signals.send, undefined);

    });


});


// mocha
// https://www.npmjs.com/package/mocha

