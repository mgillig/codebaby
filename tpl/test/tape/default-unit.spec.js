let test = require('colored-tape');
const {beforeAllBuildTest, complete} = require('./beforeAllBuildTest');






beforeAllBuildTest().then((code) => {

    let signals = code.signals;
    test('------------------------ 01 signals:interface ', function (t) {
        t.equal(typeof signals.on, 'function', 'on');
        t.equal(typeof signals.off, 'function', 'off');
        t.equal(typeof signals.send, 'function', 'send');
        t.notEqual(typeof signals.send, undefined, 'send:undefined');
        t.end();
        complete();
    });



});


// TAPE
// https://www.npmjs.com/package/tape
