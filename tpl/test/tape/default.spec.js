let test = require( 'colored-tape' );

test( '------------------------ 01 simple test ', function( t ) {

    let fd = { isFile: true };
    t.equal( fd.isFile, true, 'isFile' );

    t.end()
} );


test( '------------------------ 02 simple test ', function( t ) {

    let fd = { isFile: false };
    t.equal( fd.isFile, false, 'isFile' );

    t.end()
} );

// TAPE
// https://www.npmjs.com/package/tape
