const pathUtil = require('../../../../codebaby/core/utils/path');
const {copyTemplate, initConfig} = require('../../../../codebaby/core/process/commands');

let paths = pathUtil.getProjectPaths(__dirname);
let cache = initConfig(__dirname);
// use paths & cache for custom functions


let commands = [
    {
        cmd: 'create',
        arg: 'class',
        path: 'classes',
        cb: copyTemplate,
        replace: /{#CLASS-NAME#}/g,
        pattern: '{#CLASS-NAME#}',
        fileType: '.ts',
        syntax: 'create class={name} path={pathToApp}',
        info: 'create a new class'
    }, {
        cmd: 'create',
        arg: 'module',
        path: 'modules',
        cb: copyTemplate,
        replace: /{#MODULE-NAME#}/g,
        pattern: '{#MODULE-NAME#}',
        fileType: '.ts',
        syntax: 'create module={name} path={pathToApp}',
        info: 'create a new module'
    }
];

module.exports = {
    list: commands
};




