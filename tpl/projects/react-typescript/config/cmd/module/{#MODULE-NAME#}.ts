import {logger, signals, watch} from '../core/utils';
import {global} from '../core/global';


export default (function() {


    let _log = logger( true, 'modules.{#MODULE-NAME#}' );
    let facade = { init: _init };



    signals.on( 'app.ready', function() {
        _log( 'on app.ready ' );

    } );

    function _init(  ) {
        _log( 'init ' );
    }


    // @if NODE_ENV='develop'
    watch( false, function() {
        return [
            [ 'var', 'val' ]
        ]
    } );
    //@endif


    return facade;
})();








