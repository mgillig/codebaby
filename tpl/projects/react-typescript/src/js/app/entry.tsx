import React from "react";
import ReactDOM from "react-dom";
import {logger, signals, watch} from './core/utils';
import ExampleCssModule from './core/examples/cssModule/example'
import ExampleJasmine from './core/examples/unitTest/jasmine'

// ExampleJasmine();
interface Props {
    name: string
}

class Welcome extends React.Component<Props, {}> {

    props: Props;

    constructor(props: Props) {
        super(props)
    }

    render() {
        return <div>
            <div className={'container'}>
                <h2>Hello React Typescript {this.props.name}</h2>
            </div>
            <ExampleCssModule/>
        </div>;
    }
}

ReactDOM.render(
    <Welcome name=" <tsx> !"/>,
    document.getElementById('app')
);


// let _log = logger( true, 'app' );

// @if NODE_ENV='develop'
//@endif

// @if NODE_ENV='production'
//@endif

//@if NODE_ENV='develop'

watch(true, function () {

    let windowH = window.innerHeight;
    let windowW = window.innerWidth;

    return [
        ['windowH', windowH],
        ['windowW', windowW]
    ]
});
//@endif














