const path = require('path');
const alias = require( 'rollup-plugin-alias' );
const babel = require( 'rollup-plugin-babel' );
const preprocess = require( 'rollup-plugin-preprocess' ).default;
const nodeResolve = require( 'rollup-plugin-node-resolve' );
const commonjs = require( 'rollup-plugin-commonjs' );
const replace = require('rollup-plugin-replace');
// const uglify = require( 'rollup-plugin-uglify' ) // syntax before versio 4.0.0 ;
const {uglify} = require( 'rollup-plugin-uglify' );
const uglifyEs = require('rollup-plugin-uglify-es');
const minifyOnlyEs5 = false;


const postcss = require('rollup-plugin-postcss');
const autoprefixer = require('autoprefixer');
const postcssNested = require('postcss-nested');
const proj = path.basename(path.resolve(__dirname, '../../../'));
const pathDist = 'dist/' + proj;
const pathSrc = 'projects/' + proj + '/src';
let pathCssModule;

/**
 * -> pathCssModule
 * COPY CSS TO DIST
 */
// pathCssModule = pathDist + '/css/css-modules.css';
// if extracted to dist folder uncomment <link rel="stylesheet" href="css/css-modules.css">
/**
 * -> pathCssModule
 * OR INSIDE SASS FOLDER
 */
// pathCssModule = pathSrc + '/scss-style/css-modules.css';

let sourcemap = 'inline';
let minify = false;
let minifyCssModules = false;

if (process.env.NODE_ENV === 'production') {
    sourcemap = false;
    minify = true;
    minifyCssModules = {
        safe: true
    }

}



let plugins = [
    nodeResolve({extensions: ['.js', '.jsx']}),
    preprocess({include:['**/*.js','**/*.jsx']}),
    postcss({
        sourceMap: sourcemap,
        // extract : false | true | pathCssModule
        extract: false,
        modules:true,
        minimize: minifyCssModules,
        extensions: ['.scss', '.css'],
        use: ['sass'],
        plugins: [postcssNested(), autoprefixer()]
    }),
    // babel( {
    //     "exclude": 'node_modules/**',
    //     "presets": [
    //         ["@babel/preset-env", {
    //             /**
    //              *  https://browserl.ist/?q=last+2+version
    //              *  https://babeljs.io/docs/en/next/babel-preset-env.html
    //              *  http://kangax.github.io/compat-table/es5/
    //              *
    //              *  if no targets are defined  the behavior is like using
    //              *  the old preset-es-2015
    //              */
    //             "ignoreBrowserslistConfig": true,
    //             // "targets": {
    //             //    "browsers": [
    //             //        "> 1%",
    //             //        "last 2 versions"
    //             //    ]
    //             // },
    //             "debug": false,
    //             // polyfill
    //             "useBuiltIns": "false" // "entry" , "usage"
    //         }]
    //     ],
    // } )
];


if (minify) {

    if (minifyOnlyEs5) {
        plugins.push(uglify());
    } else {
        plugins.push(uglifyEs());
    }
}

module.exports = {
    "input": "_compiled/entry.js",
    "output": {
        "format": 'iife',
        "name": "app",
        "sourcemap": sourcemap
    },
    "plugins": plugins,
    "globals": {
        "react": "vendor.React",
        "prop-types": "vendor.PropTypes",
        "react-dom": "vendor.ReactDOM",
        // "react-router-dom": "vendor.ReactDomRouter",
        // "react-router-component": "vendor.ReactRooterComponent",
        // "semantic-ui-react": "vendor.SemanticUiReact",
        // "material-ui": "vendor.MaterialUi",
        // "jquery": "vendor.$"
    },
    "external": [
        "react",
        "prop-types",
        "react-dom",
        // "react-router-dom",
        // "react-router-component",
        // "semantic-ui-react",
        // "jquery",
        // "material-ui"
    ]
};
