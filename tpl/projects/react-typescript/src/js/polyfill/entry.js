import * as Promise from 'core-js/modules/es6.promise';
import * as Fetch from 'whatwg-fetch/dist/fetch.umd';
import * as Raf from 'window.requestanimationframe/requestanimationframe';

// Polyfills have to be accessable through the global namespace !!!


//https://babeljs.io/docs/en/next/babel-runtime-corejs2.html
// https://babeljs.io/docs/en/v7-migration
