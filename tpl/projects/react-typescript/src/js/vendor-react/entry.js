import * as React from "react";
import * as ReactDOM from "react-dom";
// import * as PropTypes from 'prop-types'

// ---------------------------------------   REACT ROUTER DOM
// import * as ReactDomRouter  from 'react-router-dom/index'
// import {HashRouter, Route, Link, NavLink,}  from 'react-router-dom'

// ---------------------------------------   REACT ROUTER COMPONENT
// import * as ReactRooterComponent  from 'react-router-component'


// ---------------------------------------   REACT REDUX
// import * as Redux from "redux";
// import * as ReactRedux from "react-redux/es/index";

// ---------------------------------------   REACT MATERIAL
// import * as MaterialUi  from 'material-ui'
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
// import RaisedButton from 'material-ui/RaisedButton/RaisedButton'
// import TextField from 'material-ui/TextField'
// import getMuiTheme from 'material-ui/styles/getMuiTheme'
// import cyan500 from 'material-ui/styles/colors'

// ---------------------------------------   REACT SEMANTIC UI
// import * as SemanticUiReact  from "semantic-ui-react"
// import {Button, Icon, Menu}  from "semantic-ui-react"


export default {
    React: React,
    ReactDOM: ReactDOM,
    // PropTypes: PropTypes,

    // ---------------------------------------   REACT ROUTER
    // ReactDomRouter: ReactDomRouter,
    // ReactRooterComponent: ReactRooterComponent.default,
    // ReactDomRouter: { HashRouter: HashRouter, Route: Route, Link: Link, NavLink: NavLink }

    // ---------------------------------------   REACT REDUX
    // ReactRedux: ReactRedux,
    // Redux: Redux,

    // ---------------------------------------   REACT MATERIAL
    // MaterialUi: MaterialUi,
    // MaterialUi: { RaisedButton: RaisedButton, MuiThemeProvider: MuiThemeProvider, TextField: TextField,getMuiTheme:getMuiTheme,cyan500:cyan500 },

    // ---------------------------------------   REACT SEMANTIC UI
    // SemanticUiReact: SemanticUiReact,
    // SemanticUiReact: { Button: Button, Icon: Icon, Menu: Menu },

}
