import {logger, signals, watch} from '../core/utils';

var _log = logger(true, 'classes.{#CLASS-NAME#}');
var instances = [];


function {#CLASS-NAME#}() {
    instances.push(this);
}
{#CLASS-NAME#}.prototype.init = function () {
    _log('init');
};









signals.on('app.ready', function (data) {
    _log('on app.ready');
});



// @if NODE_ENV='develop'
watch(false, function () {
    var instance = instances[0];
    if (instance) {
        return [
            ['var', 'val']
        ];
    }
    else {
        return [];
    }
});
//@endif

export default {#CLASS-NAME#};



