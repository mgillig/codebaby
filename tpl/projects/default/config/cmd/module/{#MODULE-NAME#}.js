import {logger, signals, watch} from '../core/utils';
import {global} from '../core/global';


export default (function() {


    var _log = logger( true, 'modules.{#MODULE-NAME#}' );
    var facade = { init: _init };



    signals.on( 'app.ready', function() {
        _log( 'on app.ready ' );

    } );

    function _init(  ) {
        _log( 'init ' );
    }


    // @if NODE_ENV='develop'
    watch( false, function() {
        return [
            [ 'var', 'val' ]
        ]
    } );
    //@endif


    return facade;
})();








