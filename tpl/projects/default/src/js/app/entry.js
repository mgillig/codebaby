import { logger, signals, watch } from './core/utils';
import * as  examples from './core/examples/index';



// var _log = logger(true, 'app');
// signals.on('app.ready', function (data) {
//
//     _log('app.ready');
//     signals.off('app.ready', this);
// });
// signals.send('app.ready');


// @if NODE_ENV='production'
//@endif

//@if NODE_ENV='develop'

watch(true, function () {
    var windowH = window.innerHeight;
    var windowW = window.innerWidth;
    return [
        ['windowH', windowH],
        ['windowW', windowW]
    ];
});
//@endif
