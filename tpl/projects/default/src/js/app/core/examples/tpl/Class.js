import {logger, signals, watch} from '../../utils';

var _log = logger(true, 'classes.Blank');
var instances = [];


function Blank() {
    instances.push(this);
}
Blank.prototype.init = function () {
    _log('init');
};









signals.on('app.ready', function (data) {
    _log('on app.ready');
});



// @if NODE_ENV='develop'
watch(false, function () {
    var instance = instances[0];
    if (instance) {
        return [
            ['var', 'val']
        ];
    }
    else {
        return [];
    }
});
//@endif

export default Blank;

