import Styles from './example.mod.scss'

export default function exampleCssModule() {

    var $body = document.querySelector('body');
    var $example = document.createElement('div');
    var html ='<div class="container"><h2 class="'+Styles.headline+'">Css Modules</h2>';
    html +='<p class="'+Styles.p+'">Lorem ipsum dolor sit amet, consectetur adipisicing elit.' +
        'Ab amet distinctio dolorem ducimus, esse, eum exercitationem expedita explicabo facilis impedit' +
        'laudantium mollitia nesciunt non, quaerat quibusdam sint sunt temporibus ullam.</p>';
    html +='<hr>';
    html +='<div class="'+Styles.btn+'">BTN</div><div class="'+Styles.btn+' active">BTN ACTIVE</div></div>';


    $example.innerHTML = html;
    $body.append($example)


}