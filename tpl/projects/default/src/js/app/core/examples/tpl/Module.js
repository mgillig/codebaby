import {logger, signals, watch} from '../../utils';
import {global} from '../../global';


export default (function() {
    
    var _log = logger( true, 'modules.blank' );
    var facade = { init: _init };



    function _init(  ) {
        _log( 'init ' );
    }







    signals.on( 'app.ready', function() {
        _log( 'on app.ready ' );

    } );


    return facade;
})();




// @if NODE_ENV='develop'
watch( false, function() {
    return [
        [ 'var', 'val' ]
    ]
} );
//@endif





