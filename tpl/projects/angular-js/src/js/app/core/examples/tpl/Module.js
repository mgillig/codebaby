import {logger, signals, watch} from '../core/utils';
import {global} from '../core/global';


export default (function () {

    const _log = logger(true, 'modules.blank');


    const _init = () => {
        _log('init ');
    };


    signals.on('app.ready', () => {
        _log('on app.ready ');

    });


    return {init: _init};
})();


// @if NODE_ENV='develop'
watch(false, () => {
    return [
        ['var', 'val']
    ]
});
//@endif





