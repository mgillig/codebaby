import {logger, signals, watch} from '../core/utils';

const _log = logger(true, 'classes.Blank');
const instances = [];


class Blank {
    constructor() {




    }

    init() {

        _log('init');
    }
}


signals.on('app.ready', data => {
    _log('on app.ready');
});


// @if NODE_ENV='develop'
watch(false, () => {
    const instance = instances[0];
    if (instance) {
        return [
            ['var', 'val']
        ];
    }
    else {
        return [];
    }
});
//@endif

export default Blank;

