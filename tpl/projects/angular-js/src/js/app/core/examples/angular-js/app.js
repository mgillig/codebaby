import rooter from "./rooter"

const app = angular
	.module("app", ["ngRoute"])
	.config(["$routeProvider", rooter])

export default app
