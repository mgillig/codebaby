import app from "../../app"

const ctrl = app.controller("NavController", function($scope) {
	$scope.compName = "NavController"
})
ctrl.$inject = ["$scope"]
app.directive("navController", function() {
	return {
		restrict: "E",
		templateUrl: "ng-templates/nav-controller.html"
	}
})
export default ctrl
