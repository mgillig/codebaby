export default function exampleJasmine() {

    if (window.describe === undefined) {

        console.log('To Run Jasmine Tests \n');
        console.log('enable test flag in build.js');
        console.log('enable include(\'includes/test/jasmine.html\') in .html');
        console.log('restart build task');

        return
    }

    describe('Jasmine Test', function () {


        it('example test', function () {

            expect(1).toBe(1);
            expect(1).not.toBe(2)

        })
    });

}