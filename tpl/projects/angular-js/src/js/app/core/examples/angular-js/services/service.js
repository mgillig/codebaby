import app from "../app"

export default app.service("Service", function($http) {
	// console.log("Service")
	// console.log($http)

	let service = this

	service.getArticles = function(cb) {
		const success = onComplete(cb, true, "articles-success")
		const fail = onComplete(cb, false, "articles")
		$http.get("data/articles.json").then(success, fail)
	}
	service.getArticle = function(id, cb) {
		const success = onComplete(cb, true, "article-success")
		const fail = onComplete(cb, false, "article-fail")
		$http.get("data/article.json").then(success, fail)
	}

	return service
})

function onComplete(cb, success, type) {
	return function(result) {
		cb(createResult(result, success, type))
	}
}

function createResult(result, success, type) {
	// console.log(type + " " + success)
	// console.log(result)

	return {
		success: success,
		data: result.data,
		status: result.status,
		statusText: result.statusText,
		type: type
	}
}
