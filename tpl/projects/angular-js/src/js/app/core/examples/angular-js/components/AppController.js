import app from "../app"

const appController = function($scope) {
	$scope.compName = "AppController"
}

appController.$inject = ["$scope"]
app.controller("AppController", appController)

app.directive("appController", function() {
	return {
		restrict: "E",
		templateUrl: "ng-templates/app.html"
	}
})
export default appController
