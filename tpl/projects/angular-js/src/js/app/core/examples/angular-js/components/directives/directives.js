import app from "../../app"

export default app.directive("error", function() {
	return {
		restrict: "E",
		template:
			'<div class="error">ERROR [ {{errorCode}} ] :<br> {{errorText}}</div>'
	}
})

let templateComp = ""

// @if NODE_ENV='develop'
templateComp = '<div class="comp">{{compName}}</div>'
//@endif

app.directive("comp", function() {
	return {
		restrict: "E",
		template: templateComp
	}
})
