import { watch } from "./core/utils"
import * as angularExample from "./core/examples/angular-js/entry"

// signals.on('app.ready', (data) => {
//     _log('app.ready');
//     signals.off('app.ready', this);
// });
// signals.send('app.ready');

// @if NODE_ENV='production'
//@endif

//@if NODE_ENV='develop'
watch(true, () => {
	var windowH = window.innerHeight
	var windowW = window.innerWidth
	return [["windowH", windowH], ["windowW", windowW]]
})
//@endif
