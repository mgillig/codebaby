const pathUtil = require('../../../../codebaby/core/utils/path');
const {copyTemplate, initConfig} = require('../../../../codebaby/core/process/commands');

let paths = pathUtil.getProjectPaths(__dirname);
let cache = initConfig(__dirname);
// use paths & cache for custom functions


let commands = [
    {
        cmd: 'create',
        arg: 'class',
        from: 'class',
        to: 'classes',
        cb: copyTemplate,
        replace: '{#CLASS-NAME#}',
        fileType: '.js',
        syntax: 'create class={name} path={pathToApp}',
        info: 'create a new class'
    }, {
        cmd: 'create',
        arg: 'mod',
        from: 'module',
        to: 'modules',
        cb: copyTemplate,
        replace: '{#MODULE-NAME#}',
        fileType: '.js',
        syntax: 'create mod={name} path={pathToApp}',
        info: 'create a new module'
    }
];

module.exports = {
    list: commands
};




