module.exports = {
	//----------------------------------------
	"handler-copy": [
		{
			// copy files into root of project dist
			path: "copy"
		}
	],
	//----------------------------------------
	"handler-image": [
		{
			path: "img",
			active: false,
			to: "img"
		}
	],
	//----------------------------------------
	"handler-html": [
		{
			path: "html"
		},
		{
			path: "js",
			to: "ng-templates",
			flatPaths: true,
			ejs: false
		}
	],
	//----------------------------------------
	"handler-javascript": [
		{
			path: "js/header",
			to: "js",
			concat: true,
			minify: true,
			es5: true // default value
			// if es5 = false minification  wil go on with uglify-es
		},
		{
			path: "js/polyfill",
			to: "js",
			rollup: true,
			active: false
		},
		{
			path: "js/vendor",
			to: "js",
			active: true,
			concat: true,
			es5: true // default value
			// if es5 = false minification  wil go on with uglify-es
		},
		{
			path: "js/app",
			to: "js",
			typescript: true,
			rollup: true
		}
	],
	//----------------------------------------
	"handler-scss": [
		{
			path: "scss-style/main.scss",
			to: "css",
			name: "style",
			minify: true,
			active: false
		},
		{
			path: "js/app/app.scss",
			to: "css",
			name: "style"
			// minify: true
		}
	],
	//----------------------------------------
	"handler-json": [
		{
			// if minify = true , comments will be removed from .json
			path: "data",
			to: "data",
			active: false,
			minify: true
		}
	]
}
