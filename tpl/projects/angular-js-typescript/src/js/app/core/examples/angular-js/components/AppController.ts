class AppController {
	static $inject: string[] = ["$scope"]

	constructor($scope) {
		$scope.compName = "AppController"
		$scope.test = ""
	}
}

export function init(app: ng.IModule) {
	app.controller("AppController", AppController)
	app.directive("appController", function() {
		return {
			restrict: "E",
			templateUrl: "ng-templates/app.html"
		}
	})
}
