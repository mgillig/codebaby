let templateComp = ""

// @if NODE_ENV='develop'
templateComp = '<div class="comp">{{compName}}</div>'
//@endif

export function init(app: ng.IModule) {
	app.directive("error", function() {
		return {
			restrict: "E",
			template:
				'<div class="error">ERROR [ {{errorCode}} ] :<br> {{errorText}}</div>'
		}
	})
	app.directive("comp", function() {
		return {
			restrict: "E",
			template: templateComp
		}
	})
}
