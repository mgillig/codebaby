import app from "./app"

import * as appController from "./components/AppController"
import * as navController from "./components/navigation/NavController"
import * as exampleComp from "./components/exampleComp/exampleComp.component"
import * as directives from "./directives/directives"
import * as service from "./services/service"

export function init() {
	directives.init(app)
	service.init(app)
	appController.init(app)
	navController.init(app)
	exampleComp.init(app)
}
