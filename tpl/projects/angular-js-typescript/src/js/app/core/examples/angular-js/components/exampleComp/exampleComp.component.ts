class ExampleComp {
	static $inject: string[] = ["$routeParams", "$scope", "Service"]

	constructor($routeParams, $scope, Service) {
		const { id } = $routeParams
		$scope.compName = "exampleComp"
		$scope.id = id
		$scope.success = true
		$scope.errorCode = ""
		$scope.errorText = ""
		$scope.price = ""

		Service.getArticle(id, result => {
			$scope.success = result.success
			if (result.success) {
				$scope.name = result.data.name
				$scope.description = result.data.description
				$scope.price = result.data.price
			} else {
				$scope.errorCode = result.statusText
				$scope.price = ""
			}
		})

		$scope.onClick = function() {
			console.log("onClick")
		}
	}
}

export function init(app: ng.IModule) {
	app.component("exampleComp", {
		controller: ExampleComp,
		templateUrl: "ng-templates/example-comp.html"
	})
}
