import {logger, signals, watch} from '../../utils';


let _log = logger(true, 'HelloWorld');
let instances = [];

export default class HelloWorld {

    time:string = ''+new Date().getSeconds();
    id: string;

    constructor(id) {
        _log('HelloWorld : ' + id);
        let _this = this;

        this.id = id;

        instances.push(this);
        signals.on('app.ready', function (data) {
            _log('on app.ready');
            _log(_this)
            // signals.of( 'app.ready', this )
        });
        setInterval(function () {
            let date = new Date();
            _this.time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        }, 1000)

    }

    init() {
        console.log(this.id + ' init');
    }
}




// @if NODE_ENV='develop'
watch(true, function () {
    let instance = instances[0];
    if (instance) {
        return [
            ['time', instance.time]
        ]
    } else {
        return [];
    }
});
//@endif