const rooter = function($routeProvider) {
	$routeProvider
		.when("/home", {
			template:
				'<a  class="btn" href="https://docs.angularjs.org/tutorial/step_01" target="_blank">tutorial</a>'
		})
		.when("/example-comp", {
			template: "<example-comp></example-comp>"
		})
		.when("/404", {
			template: "<div>Not found</div>"
		})

		.otherwise("/404")
}
export default rooter
