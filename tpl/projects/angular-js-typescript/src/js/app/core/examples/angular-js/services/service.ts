class Service {
	static $inject: string[] = ["$http"]
	$http: ng.IHttpService
	constructor($http) {
		this.$http = $http
	}
	getArticles(cb: any): void {
		const success = onComplete(cb, true, "articles-success")
		const fail = onComplete(cb, false, "articles")
		this.$http.get("data/articles.json").then(success, fail)
	}
	getArticle(id: string, cb: any): void {
		const success = onComplete(cb, true, "article-success")
		const fail = onComplete(cb, false, "article-fail")
		this.$http.get("data/article.json").then(success, fail)
	}
}

function onComplete(cb, success, type): any {
	return function(result) {
		cb(createResult(result, success, type))
	}
}

function createResult(result, success, type): any {
	return {
		success: success,
		data: result.data,
		status: result.status,
		statusText: result.statusText,
		type: type
	}
}

export function init(app: ng.IModule): void {
	app.service("Service", Service)
}
