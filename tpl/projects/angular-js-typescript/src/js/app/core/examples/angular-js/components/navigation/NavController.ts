class NavController {
	static $inject: string[] = ["$scope"]
	constructor($scope) {
		$scope.compName = "NavController"
	}
}

export function init(app: ng.IModule) {
	app.controller("NavController", NavController)
	app.directive("navController", function() {
		return {
			restrict: "E",
			templateUrl: "ng-templates/nav-controller.html"
		}
	})
}
