import * as angular from "angular"
import rooter from "./core/examples/angular-js/rooter"
const app: ng.IModule = angular
	.module("app", ["ngRoute"])
	.config(["$routeProvider", rooter])
export default app
