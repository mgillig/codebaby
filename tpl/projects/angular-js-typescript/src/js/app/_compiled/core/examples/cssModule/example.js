import Styles from './example.mod.scss';
export default function exampleCssModule() {
    var $body = document.querySelector('body');
    var $example = document.createElement('div');
    $example.innerHTML = "<div class=\"container\">\n<h2 class=\"" + Styles.headline + "\">Css Modules</h2>\n<p class=\"" + Styles.p + "\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. \nAb amet distinctio dolorem ducimus, esse, eum exercitationem expedita explicabo facilis impedit \nlaudantium mollitia nesciunt non, quaerat quibusdam sint sunt temporibus ullam.</p>\n<hr>\n\n<div class=\"" + Styles.btn + "\">BTN</div>\n<div class=\"" + Styles.btn + " active\">BTN ACTIVE</div>\n</div>";
    $body.append($example);
}
