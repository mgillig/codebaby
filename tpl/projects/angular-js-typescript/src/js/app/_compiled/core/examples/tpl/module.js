import { logger, signals, watch } from '../../utils';
export default (function () {
    var _log = logger(true, 'module.helloWorld');
    var facade = { init: _init };
    var items = [1, 2, 3];
    signals.on('app.ready', function () {
        _log('on app.ready ');
    });
    function _init(mess) {
        if (mess === void 0) { mess = undefined; }
        _log('init : ');
    }
    // @if NODE_ENV='develop'
    watch(true, function () {
        return [
            ['items', items]
        ];
    });
    //@endif
    return facade;
})();
