var Service = /** @class */ (function () {
    function Service($http) {
        this.$http = $http;
    }
    Service.prototype.getArticles = function (cb) {
        var success = onComplete(cb, true, "articles-success");
        var fail = onComplete(cb, false, "articles");
        this.$http.get("data/articles.json").then(success, fail);
    };
    Service.prototype.getArticle = function (id, cb) {
        var success = onComplete(cb, true, "article-success");
        var fail = onComplete(cb, false, "article-fail");
        this.$http.get("data/article.json").then(success, fail);
    };
    Service.$inject = ["$http"];
    return Service;
}());
function onComplete(cb, success, type) {
    return function (result) {
        cb(createResult(result, success, type));
    };
}
function createResult(result, success, type) {
    return {
        success: success,
        data: result.data,
        status: result.status,
        statusText: result.statusText,
        type: type
    };
}
export function init(app) {
    app.service("Service", Service);
}
