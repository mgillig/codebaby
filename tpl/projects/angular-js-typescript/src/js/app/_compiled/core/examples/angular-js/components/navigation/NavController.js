var NavController = /** @class */ (function () {
    function NavController($scope) {
        $scope.compName = "NavController";
    }
    NavController.$inject = ["$scope"];
    return NavController;
}());
export function init(app) {
    app.controller("NavController", NavController);
    app.directive("navController", function () {
        return {
            restrict: "E",
            templateUrl: "ng-templates/nav-controller.html"
        };
    });
}
