import * as angular from "angular";
import rooter from "./rooter";
var app = angular
    .module("app", ["ngRoute"])
    .config(["$routeProvider", rooter]);
export default app;
