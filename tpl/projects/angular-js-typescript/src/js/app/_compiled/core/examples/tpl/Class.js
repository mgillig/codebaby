import { logger, signals, watch } from '../../utils';
var _log = logger(true, 'HelloWorld');
var instances = [];
var HelloWorld = /** @class */ (function () {
    function HelloWorld(id) {
        this.time = '' + new Date().getSeconds();
        _log('HelloWorld : ' + id);
        var _this = this;
        this.id = id;
        instances.push(this);
        signals.on('app.ready', function (data) {
            _log('on app.ready');
            _log(_this);
            // signals.of( 'app.ready', this )
        });
        setInterval(function () {
            var date = new Date();
            _this.time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        }, 1000);
    }
    HelloWorld.prototype.init = function () {
        console.log(this.id + ' init');
    };
    return HelloWorld;
}());
export default HelloWorld;
// @if NODE_ENV='develop'
watch(true, function () {
    var instance = instances[0];
    if (instance) {
        return [
            ['time', instance.time]
        ];
    }
    else {
        return [];
    }
});
//@endif
