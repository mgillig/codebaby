console.log("AppController");
var AppController = /** @class */ (function () {
    function AppController($scope) {
        // console.log(this)
        // console.log($scope)
        $scope.compName = "AppController";
        $scope.test = "";
    }
    AppController.$inject = ["$scope"];
    return AppController;
}());
export function init(app) {
    app.controller("AppController", AppController);
    app.directive("appController", function () {
        return {
            restrict: "E",
            templateUrl: "ng-templates/app.html"
        };
    });
}
