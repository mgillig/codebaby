var ExampleComp = /** @class */ (function () {
    function ExampleComp($routeParams, $scope, Service) {
        console.log("ExampleComp");
        var id = $routeParams.id;
        $scope.compName = "exampleComp";
        $scope.id = id;
        $scope.success = true;
        $scope.errorCode = "";
        $scope.errorText = "";
        $scope.price = "";
        Service.getArticle(id, function (result) {
            $scope.success = result.success;
            if (result.success) {
                $scope.name = result.data.name;
                $scope.description = result.data.description;
                $scope.price = result.data.price;
            }
            else {
                $scope.errorCode = result.statusText;
                $scope.price = "";
            }
        });
        $scope.onClick = function () {
            console.log("onClick");
        };
    }
    ExampleComp.$inject = ["$routeParams", "$scope", "Service"];
    return ExampleComp;
}());
// function ctrl($routeParams, $scope, Service) {
// 	const { id } = $routeParams
// 	$scope.compName = "exampleComp"
// 	$scope.id = id
// 	$scope.success = true
// 	$scope.errorCode = ""
// 	$scope.errorText = ""
// 	$scope.price = ""
// 	Service.getArticle(id, result => {
// 		$scope.success = result.success
// 		if (result.success) {
// 			$scope.name = result.data.name
// 			$scope.description = result.data.description
// 			$scope.price = result.data.price
// 		} else {
// 			$scope.errorCode = result.statusText
// 			$scope.price = ""
// 		}
// 	})
// 	$scope.onClick = function() {
// 		console.log("onClick")
// 	}
// }
// ctrl.$inject = ["$routeParams", "$scope", "Service"]
export function init(app) {
    console.log("ExampleComp:init");
    app.component("exampleComp", {
        controller: ExampleComp,
        templateUrl: "ng-templates/example-comp.html"
    });
}
