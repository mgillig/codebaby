// CDN EXAMPLE
!require('https://code.jquery.com/jquery-3.2.1.min.js');

// PROJECT DEFAULTS

//@if NODE_ENV='develop'
require('vue/dist/vue.js');
// @endif

//@if NODE_ENV='production'
require('vue/dist/vue.min.js');
// @endif

!require('jquery/dist/jquery.min.js');
!require('lodash/lodash.js');


// OPTIONAL
!require('vue-material/dist/vue-material.js');


!require('ramda/dist/ramda.min.js');
!require('moment/min/moment.min.js');

// npm i gsap --save
!require('gsap/umd/TweenMax.js');



