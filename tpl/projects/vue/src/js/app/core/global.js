export var global =  (function () {

    var global = {
        val: 'your data',
        host_url: '/* @echo HOST_URL */',
        // @if NODE_ENV='develop'
        production: false
        //@endif
    };
    // @if NODE_ENV='production'
    global.production = true;
    //@endif
    return global;

})();
