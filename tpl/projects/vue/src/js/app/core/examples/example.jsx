const ExampleJsx = Vue.component('exampleJsx', {
    props: ['message'],
    data: function () {
        return {
            type: ' <jsx>'
        }
    },
    created() {
        console.log('jsx created');
    },
    mounted() {
        console.log('jsx mounted');
        console.log(this.$refs);
    },
    render: function (h) {
        return (
            <div>
                <h2 ref="headline"> {this.message} {this.type} !</h2>
                //@if NODE_ENV='develop'
                <p class="flag">develop</p>
                //@endif
                //@if NODE_ENV='production'
                <p class="flag">production</p>
                //@endif
            </div>
        )
    }
});
export default ExampleJsx;