import Styles from './example.mod.scss';

const CssModule = Vue.component('cssModule', {
    render: function (h) {
        return (
            <div>
                <h2 class={Styles.headline}>Css Modules</h2>
                <p class={Styles.p}>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Ab amet distinctio dolorem ducimus, esse, eum exercitationem expedita explicabo facilis impedit
                    laudantium mollitia nesciunt non, quaerat quibusdam sint sunt temporibus ullam.</p>
                <hr/>
                <div class={Styles.btn}>BTN</div>
                <div class={`${Styles.btn} active`}>BTN ACTIVE</div>
            </div>
        )
    }
});

export default CssModule;

