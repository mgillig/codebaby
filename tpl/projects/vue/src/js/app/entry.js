import {logger, signals, watch} from './core/utils';
import ExampleJsx from './core/examples/example.jsx'
import ExampleCssModule from './core/examples/cssModule/example'
import VueExample from './core/examples/example-vue'


Vue.component('vue-example', VueExample);

const vueApp = new Vue( {

    el: '#app',
    data: {
        message: 'Hello Vue!'
    },
    template:'<div class="container"><example-jsx :message="message"/><vue-example/><css-module/></div>',
} );

// const _log = logger(true, 'app');

// signals.on('app.ready', (data) => {
//     _log('app.ready');
//     signals.off('app.ready', this);
// });
// signals.send('app.ready');

// @if NODE_ENV='production'
//@endif

//@if NODE_ENV='develop'

watch(true, () => {
    let windowH = window.innerHeight;
    let windowW = window.innerWidth;
    return [
        ['windowH', windowH],
        ['windowW', windowW]
    ];
});
//@endif
