const alias = require( 'rollup-plugin-alias' );
const babel = require( 'rollup-plugin-babel' );
const preprocess = require( 'rollup-plugin-preprocess' ).default;
const nodeResolve = require( 'rollup-plugin-node-resolve' );
const commonjs = require( 'rollup-plugin-commonjs' );
const {uglify} = require( 'rollup-plugin-uglify' );
// const uglify = require( 'rollup-plugin-uglify' ) // syntax before versio 4.0.0 ;

const replace = require( 'rollup-plugin-replace' );



let sourcemap = true;
let plugins = [
    nodeResolve( { jsnext: true, module: true, browser: true, preferBuiltins: true } ),
    commonjs( {
        // include: [ "../../../../../node_modules/**"],
        "namedExports": {
            // left-hand side can be an absolute path, a path
            // relative to the current directory, or the name
            // of a module in node_modules
            // 'react': [ 'Component', 'Children', 'isValidElement', 'cloneElement', 'createElement' ],
            // 'react-dom': [ 'findDOMNode' ]
        }
    } ),
    replace( {
        "process.env.NODE_ENV": JSON.stringify( process.env.NODE_ENV ),
    } ),
    // babel( {
    //     exclude: 'node_modules/**',
    //     "presets": [["@babel/env"]]
    // } ),

];

if ( process.env.NODE_ENV === 'production' ) {
    plugins.push( uglify() );
    sourcemap = false;
}


module.exports = {
    "input": "entry.js",
    "output": {
        "format": 'iife',
        "name": "polyfill",
        "sourcemap": sourcemap
    },
    plugins: plugins
};

// https://github.com/rollup/rollup-plugin-node-resolve#usage
// http://kangax.github.io/compat-table/es6/