import {logger, signals, watch} from '../core/utils';
import {global} from '../core/global';


export function test() {


    const toggleActive = (obj) => {

        return Object.assign({}, obj, {active: !obj.active});
    };

    const toggleActiveEs6 = (obj) => {

        return {...obj, active: !obj.active};
    };


    describe('tesst', function () {
        it('object toogle', function () {

            let obj1 = {active: true};
            let obj2 = {active: false};

            Object.freeze(obj1);

            expect(toggleActive(obj1)).toEqual(obj2);
            expect(toggleActiveEs6(obj1)).toEqual(obj2);
        })
    })

}








