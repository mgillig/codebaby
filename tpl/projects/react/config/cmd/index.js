const pathUtil = require('../../../../codebaby/core/utils/path');
const {copyTemplate, initConfig} = require('../../../../codebaby/core/process/commands');

let paths = pathUtil.getProjectPaths(__dirname);
let cache = initConfig(__dirname);
// use paths & cache for custom functions


let commands = [
    {
        cmd: 'create',
        arg: 'comp',
        from:'component',
        to:'components',
        cb: copyTemplate,
        replace: '{#COMP-NAME#}',
        fileType: '.jsx',
        syntax: 'create comp={name} path={pathToApp}',
        info: 'create a jsx component'
    },
    {
        cmd: 'create',
        arg: 'test',
        from:'test',
        to:'test',
        cb: copyTemplate,
        replace: '{#TEST-NAME#}',
        fileType: '.jsx',
        syntax: 'create test={name} path={pathToApp}',
        info: 'create a jasmine test'
    }
];

module.exports = {
    list: commands
};




