import React,{Component} from 'react'
import PropTypes from 'prop-types';


class {#COMP-NAME#} extends Component {


    constructor(props) {

        super(props);

    }

    render() {
        return <div>{#COMP-NAME#}</div>;
    }
}

//{#COMP-NAME#}.propTypes = {
//};


export default {#COMP-NAME#};


// LIFECYCLE EVENTS IN ORDER

//constructor()
//getChildContext()
//componentWillMount() // can be called multiple times
//componentDidMount() // only on client not server, handle Ajax
//componentWillReceiveProps()
//shouldComponentUpdate()
//componentWillUpdate()
//componentDidUpdate() // Ajax
//componentWillUnmount()
// componentDidCatch()