export {logger, signals, watch}


// @if NODE_ENV='develop'
var colors = ['#FFFACD', '#cbebab', '#E6E6FA', '#D3D3D3', '#B0E0E6', '#FFF5EE', '#FFE4E1', '#ADFF2F', '#FAA460', '#FABC35', '#37FAB3', '#11B0FA', '#11B0FA'];
var styles = {};

function getStyle(module) {

    if (styles[module] === undefined) {

        var color = colors[0];
        if (colors.length > 1) {
            colors.shift();
        }
        styles[module] = color;
    } else {
        color = styles[module] || '';
    }
    return "color: black;  background-color: " + color + ";padding: 2px;"

}


//@endif


function logger(state, id, colors = undefined) {
    // @if NODE_ENV='develop'
    if (colors === undefined) {
        colors = true;
    }
    if (id === undefined) {
        id = '';
        colors = false;
    }
    //@endif
    return function (mess, flag = undefined) {
        // @if NODE_ENV='develop'
        if (state) {
            flag = ( flag === undefined ) ? ' ' : ' >  ' + flag;
            if (colors) {
                console.log("%c " + id + flag, getStyle(id), mess);
            } else {
                if (id === '') {
                    console.log(mess);
                } else {
                    console.log(mess, id);
                }

            }
        }
        //@endif
    }
}
var listeners = {};
var signals = (function () {
    function _on(type, fn) {

        var currListener = listeners[type];
        if (currListener === undefined) {
            listeners[type] = [];
            currListener = listeners[type]
        }

        currListener.unshift(fn);
        return fn;

    }

    function _off(type, fn) {

        var list = listeners[type];

        if (list !== undefined) {

            var listN = list.length;
            for (var i = 0; i < listN; i++) {
                if (fn === list[i]) {
                    list[i] = undefined;
                    list.splice(i, 1);
                    if (list.length === 0) {
                        delete listeners[type];
                    }
                    break;
                }
            }
        }
        return undefined;
    }

    function _send(type, data = undefined, fn) {

        var list = listeners[type];
        if (list !== undefined) {
            var listN = list.length;
            var i = listN - 1;
            do {
                // BIND (this) TO FUNCTION
                var cb = list[i];
                if (typeof(cb) === 'function') {
                    list[i].call(list[i], data);
                }

            } while (i--);

        }
        //callback after send
        if (fn !== undefined && typeof fn === 'function') {
            fn();
        }
    }

    return {on: _on, off: _off, send: _send};

})();


var watch = (function () {

    /* @if NODE_ENV='develop' */
    var ID = 'core.util.watch';
    var debug = false;
    var isTouch = false;
    if ("ontouchstart" in document.documentElement) {
        isTouch = true;
    }

    var id = 'debugPanel';
    var myObserveData = [];
    var $_view;
    var positions = ['pos-bl', 'pos-br', 'pos-tr', 'pos-tl'];
    var pos = 0;

    var entrys = [];
    var $_entrys = [];


    var panelState = localStorage[ID];
    var panelError = false;
    if (panelState === undefined) {
        panelState = {'pos': 0, 'open': true};
    } else {
        panelState = JSON.parse(panelState);
    }
    if (panelState.pos === undefined) {
        panelState.pos = 0;
        panelError = true;
    }
    if (!panelError) {
        pos = parseInt(panelState.pos);
    } else {
        localStorage[ID] = JSON.stringify(panelState)
    }


    var changeReady = true;
    var $_watcher;
    var $_tpl_entry;
    var $_list;
    var init = false;
    var panelClassList = [];


    function _init() {

        if (!init) {
            init = true;

            $_tpl_entry = document.createElement('li');
            $_tpl_entry.innerHTML = '<span class="debug-label"></span><span class="debug-points">:</span><span class="debug-val"></span>';


            var divWrapper = document.createElement('div');
            var divWatcher = document.createElement('div');
            divWrapper.id = id;
            divWatcher.id = 'debug-watcher';
            divWatcher.innerHTML = '<strong>[ Debug ] </strong>';
            divWrapper.appendChild(divWatcher);

            document.body.appendChild(divWrapper);
            $_watcher = document.getElementById('debug-watcher');
            $_list = document.createElement('ul');
            divWatcher.appendChild($_list);

            start();
            updateList();
        }
    }

    function changePos() {

        if (changeReady) {
            // changeReady = false;
            pos++;

            if (pos === 4) {
                pos = 0;
            }

            panelClassList[1] = positions[pos];
            $_view.clasName = panelClassList.join(' ');
            panelState.pos = pos;
            localStorage.setItem(ID, JSON.stringify(panelState));

        }

    }

    function myState() {

        var entrys = [];
        myObserveData.forEach(function (cb) {
            var data = cb();

            if (data !== undefined) {
                data.map(function (data) {
                    entrys.push(data)
                });
            }
        });

        return entrys;
    }

    function start() {


        var style = '<style>#debug-watcher {background-color: rgba(0, 0, 0,1);color: #67776e;font-size: 14px;font-family: sans-serif;}';
        style += '#debugPanel.pos-bl{left:0;bottom:0}';
        style += '#debugPanel.pos-br{right:0;bottom:0}';
        style += '#debugPanel.pos-tl{left:0;top:0}';
        style += '#debugPanel.pos-tr{right:0;top:0}';
        style += '#debug-watcher strong { display: none;}#debug-watcher .debug-points {color: #a9a9a9 !important;}';
        style += '#debug-watcher .debug-label {color: #32a91a !important;}';
        style += '#debug-watcher .debug-val {color: #ffffff !important;}';
        style += '#debug-watcher ul {display: block;padding: 10px;margin: 0;}';
        style += '#debug-watcher li {list-style: none;font-size:14px;line-height:16px;}';
        style += '#debug-watcher button {width: 100%;margin-top:10px;margin-bottom:10px,backgroundColor:inherit;}';
        style += '#debug-watcher button:last-child {margin-bottom:20px!important;}';
        // style += '#debug-watcher button:first {color:"#ff0000";margin-top:0!important;}';
        style += '#debug-watcher .debug-toggle {display:none;}';
        style += '#debug-watcher .debug-toggle.active {display:block;}';
        style += '#debug-watcher input {width:97%;}';
        // style += '#debug-watcher button:first-child {margin-top:10px}';
        //style += '#debug-watcher li {user-select: none;pointer-events: none;cursor: default;list-style: none;}';
        style += '.debug-closed strong {padding: 5px;color: #32a91a !important;display: block !important;}';
        style += '.debug-closed  ul {display: none !important;}';
        style += '.debug-closed  li {display: none;}</style>';
        var head = document.getElementsByTagName('head');
        var curr = head[0].innerHTML;
        head[0].innerHTML = curr + style;
        $_view = document.getElementById(id);
        $_view.style.position = 'fixed';
        $_view.style.float = 'left';
        $_view.style.zIndex = '100000';

        panelClassList[0] = '';

        if (!panelState.open) {
            panelClassList[0] = 'debug-closed';
        }
        panelClassList[1] = positions[pos];

        $_view.className = panelClassList.join(' ');

        $_view.addEventListener('click', function (e) {
            if (e.target.nodeName !== "BUTTON" && e.target.nodeName !== "INPUT") {

                panelState.open = !panelState.open;
                panelState.pos = pos;
                localStorage[ID] = JSON.stringify(panelState);

                if (panelState.open) {
                    panelClassList[0] = '';
                } else {
                    panelClassList[0] = 'debug-closed';
                }
                $_view.className = panelClassList.join(' ');

            }
        });


        var startTouch;
        if (!isTouch) {
            $_view.addEventListener('dblclick', function (e) {
                changePos();
            });

        } else {

            $_view.addEventListener('touchstart', function (e) {
                // $_view.on( 'touchstart', function( e ) {

                if (e.originalEvent) {
                    startTouch = e.originalEvent.pageX;
                } else {

                    startTouch = e.changedTouches[0].clientX;
                }


            });
            $_view.addEventListener('touchend', function (e) {
                // $_view.on( 'touchend', function( e ) {

                var endTouch;
                if (e.originalEvent) {
                    endTouch = e.originalEvent.pageX;
                } else {

                    endTouch = e.changedTouches[0].clientX;
                }

                var diff = Math.abs(endTouch - startTouch);
                // console.log('diff : '+diff)
                if (diff > 50) {
                    changePos();
                }

            })

        }


        window.setInterval(function () {

            entrys = myState();
            updateList()

        }, 100)
    }


    function updateList() {

        var n = entrys.length;
        var o = $_entrys.length;
        var entry;
        for (var i = 0; i < n; i++) {

            var entrydata = entrys[i];
            if (i + 1 > o) {

                entry = {
                    $_label: undefined,
                    $_val: undefined,
                    $_el: $_tpl_entry.cloneNode(true),
                    label: entrydata[0],
                    val: '',
                    update: function (newVal) {

                        if (newVal !== this.val) {
                            this.val = newVal;
                            // this.$_val.text( this.val );
                            this.$_val.innerHTML = this.val;
                        }
                    }
                };

                // console.log( entry );

                $_entrys.push(entry);
                $_list.appendChild(entry.$_el);
                entry.$_label = entry.$_el.querySelector('.debug-label');
                // console.log(  entry.$_label );
                entry.$_label.innerHTML = entry.label;
                entry.$_val = entry.$_el.querySelector('.debug-val');
                entry.update(entrydata[1]);


            } else {

                entry = $_entrys[i];
                if (entrydata) {
                    entry.update(entrydata[1])
                }
            }
        }
    }

    /* @endif */

    return function (state, fn) {
        /* @if NODE_ENV='develop' */
        if (state) {
            _init();
            myObserveData.push(fn);
        }
        /* @endif */
    }
})();
