import React from 'react';
import Styles from './example.mod.scss';

export default function ExampleCssModule() {

    return (
        <div className="container">
            <h2 className={Styles.headline}>Css Modules</h2>
            <p className={Styles.p}>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Ab amet distinctio dolorem ducimus, esse, eum exercitationem expedita explicabo facilis impedit
                laudantium mollitia nesciunt non, quaerat quibusdam sint sunt temporibus ullam.</p>
            <hr/>
            <div className={Styles.btn}>BTN</div>
            <div className={`${Styles.btn} active`}>BTN ACTIVE</div>
        </div>
    )
}

