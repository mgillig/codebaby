import {logger, signals, watch} from './core/utils';
import React from 'react'
import ReactDOM from 'react-dom'
import * as examples from './core/examples/index'
import ExampleCssModule from './core/examples/cssModule/example'


// let _log = logger( true, 'app' );

class Welcome extends React.Component {
    render() {
        return <div>
            <div className={'container'}>
                <h2>Hello React {this.props.name}</h2>
            </div>
            <ExampleCssModule/>
        </div>;
    }
}


ReactDOM.render(
    <Welcome name=" <jsx> !"/>,
    document.getElementById('app')
);


// @if NODE_ENV='develop'
//@endif

// @if NODE_ENV='production'
//@endif

//@if NODE_ENV='develop'
watch(true, function () {

    let windowH = window.innerHeight;
    let windowW = window.innerWidth;

    return [
        ['windowH', windowH],
        ['windowW', windowW]
    ]
});
//@endif


