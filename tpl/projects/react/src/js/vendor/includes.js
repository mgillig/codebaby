// CDN EXAMPLE
!require('https://code.jquery.com/jquery-3.2.1.min.js');

// PROJECT DEFAULTS
!require('jquery/dist/jquery.min.js');
!require('lodash/lodash.js');


// OPTIONAL
!require('ramda/dist/ramda.min.js');
!require('moment/min/moment.min.js');

// npm i gsap --save
!require('gsap/umd/TweenMax.js');

