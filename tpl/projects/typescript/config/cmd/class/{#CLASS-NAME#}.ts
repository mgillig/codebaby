import {logger, signals, watch} from '../core/utils';


let _log = logger(true, '{#CLASS-NAME#}');
let instances = [];

export default class {#CLASS-NAME#} {


    constructor() {

    }
}




// @if NODE_ENV='develop'
watch(false, function () {
    let instance = instances[0];
    if (instance) {
        return [
            ['var', 'val']
        ]
    } else {
        return [];
    }
});
//@endif

