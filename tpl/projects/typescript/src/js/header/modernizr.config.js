module.exports = {

    active: false,
    settings:{
        "minify": false,
        "options": [
            "addTest", // required for detectizr
            "setClasses"
        ],
        "feature-detects": [
            "test/css/flexbox",
            "test/es6/collections",
            "test/es6/generators",
            "test/es6/math",
            "test/es6/number",
            "test/es6/object",
            "test/es6/promises",
            "test/es6/string",
            "test/network/fetch"
        ]
    }

};

// USAGE
// https://modernizr.com/download?setclasses
// build -> copy command line config in settings : {}
