import {logger, signals, watch} from '../../utils';
import {global} from '../../global';


export default (function() {


    let _log = logger( true, 'module.helloWorld' );
    let facade = { init: _init };
    let items = [ 1, 2, 3 ];



    signals.on( 'app.ready', function() {
        _log( 'on app.ready ' );

    } );

    function _init( mess=undefined ) {
        _log( 'init : ' );
    }


    // @if NODE_ENV='develop'
    watch( true, function() {
        return [
            [ 'items', items ]
        ]
    } );
    //@endif


    return facade;
})();








