module.exports = {
    'project': {
        name: '{#PROJ#}',
        buildFolder: 'dist/{#PROJ#}',
        srcFolder: 'src',
        deployFolder: 'deploy/{#PROJ#}',
        deployIgnore:['favicon.ico'],
        deployZip: true,
        cleanFolder: true,
        // if cleanfolder == true the dist folder gets cleaned
        // from previous compiled files,
        ignoreFolder: [],
        ignoreFiles: [],
        // exclude files & folders from cleaning - example:
        // ignoreFolder: ['css','js/app']
        // ignoreFiles: ['index.html','js/app.js' ]
        trackSize: false,
        remoteLog: false,
        commands: true


    },
    'browserSync': {
        active: true,
        ui: false,
        open: 'local',
        logLevel: "silent", // Can be either "info", "debug", "warn", or "silent"
        browser: 'google chrome',
        startPath: "",
        https: false,
        ghostMode: false,
        codeSync: true,
        proxy: undefined,
        spaRouting: false,
        // SPA Routing [ true , false, path ]
        // if true path will be set to  '/index.html'
        // otherwise set path manually e.g '/app.html'
        // for assets set basePath = <base href="/"> in .html header
    },
    'uglify': {
        // usage : https://www.npmjs.com/package/uglify-js
        warnings: true
    },
    'preprocess': {
        // global options : depending on your build task
        // usage : https://github.com/onehealth/preprocess
        VERSION: '0.1', // set your production version
        TIMESTAMP: '', // will be set while compiling
        NODE_ENV: '', // production || develop
        NODE_ENV_LANG: '', // set your lang flags
        NODE_ENV_TEST: '', // true || false
        NODE_ENV_VARIANT: '', // set your variant flag
        REMOTE_LOG: '',// true || false
        REMOTE_HOST: '',// true || false
        REMOTE_LOG_URL: '', // '' ( REMOTE_DEBUG == false ) || your running browser url + log port  ( REMOTE_DEBUG == true )
        HOST_URL: '' // '' ( production && develop ) || your running browser url ( REMOTE_HOST == true )
    },
    'test': {
        active: false,
        type: 'jasmine' // Can be either "tape", "jasmine" or "mocha"
    },
    'dependencies': [
        {name: '@babel/preset-env'},
        // header
        {name: 'modernizr'},
        {name: 'detectizr'},
        // vendor
        {name: 'jquery'},
        {name: 'gsap'},
        {name: 'lodash'},
        // polyfill
        {name: 'core-js'},
        {name: 'whatwg-fetch'},
        {name: 'window.requestanimationframe'},
    ]
};