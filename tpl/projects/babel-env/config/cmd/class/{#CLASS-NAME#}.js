import {logger, signals, watch} from '../core/utils';

const _log = logger(true, 'classes.{#CLASS-NAME#}');
const instances = [];


class {#CLASS-NAME#}{


    constructor(){
        instances.push(this);
    }
    init(){
        _log('init');
    }
}


signals.on('app.ready', data => {
    _log('on app.ready');
});


// @if NODE_ENV='develop'
watch(false, () => {
    const instance = instances[0];
    if (instance) {
        return [
            ['var', 'val']
        ];
    }
    else {
        return [];
    }
});
//@endif

export default {#CLASS - NAME#};



