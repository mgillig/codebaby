import {logger, signals, watch} from '../core/utils';
import {global} from '../core/global';


export default (function() {


    const _log = logger( true, 'modules.{#MODULE-NAME#}' );


    signals.on( 'app.ready', ()=> {
        _log( 'on app.ready ' );

    } );


    const _init = ()=>{
        _log( 'init ' );
    };


    // @if NODE_ENV='develop'
    watch( false, ()=> {
        return [
            [ 'var', 'val' ]
        ]
    } );
    //@endif


    return { init: _init };
})();








