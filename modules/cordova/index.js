const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const fileUtil = require('../../core/utils/file');
const FileData = require('../../core/components/FileData');
const handler = require('../../handler/copy/handler');
const FileTree = require('../../core/components/FileTree');

const _log = require('../../core/utils/logger').log('mod cordova index', false);
const _info = require('../../core/utils/logger').info();

let pathWs = path.resolve();


module.exports = {
    setup: _setup,
    build: _build,
    getOptions: _getOptions
};

function _setup(proj) {

    _log('_setup ' + proj);

    let pathDest = pathWs + '/projects/' + proj + '/modules/cordova/';
    let pathConfigDest = pathDest + 'module.config.js';

    return new Promise((resolve) => {
        fileUtil.mkdir('/projects/' + proj + '/modules/cordova').then(() => {

            checkFileExist(pathConfigDest).then(() => {
                console.log(chalk.red('file exist :\n/projects/' + proj + '/modules/cordova/config.js'));
                resolve();
            }).catch(() => {
                fileUtil.copyFile('codebaby/modules/cordova/tpl/project/module.config.js', pathConfigDest).then(() => resolve())
            });
        });
    })
}

function _build(options, proj) {

    _log('_build ');
    _log(options);
    _log(proj);


    let pathConfig = options.pathProj + '/modules/cordova/module.config.js';

    return new Promise((resolve) => {


        checkFileExist(pathConfig).then(() => {

            let config = require(pathConfig);
            let ft = new FileTree();
            ft.read({
                path: options.pathCodebaby + '/modules/cordova/tpl/app',
                transform: options.pathProj
            }).then((result) => {

                handleApps(options, config, proj, result).then(() => {
                    resolve();
                });
            });
        }).catch(() => {
            console.log(chalk.red('No cordova config.js :\n' + pathConfig));
            resolve();
        });
    });
}

function checkFileExist(path) {

    return new Promise((resolve, reject) => {

        fs.exists(path, (exist) => {
            if (exist) {
                resolve()
            } else {
                // console.log( chalk.red( 'file not found :\n' + path ) );
                reject();
            }
        })
    })
}


function handleApps(options, config, proj, fileTree) {


    _log('handleApps');
    //_log( options );
    //_log( config );
    //_log( proj );

    return new Promise((resolve) => {
        let promises = config.apps.map(entry => {

            if (entry.active === undefined) {
                entry.active = true;
            }
            if (entry.active) {

                return new Promise((resolve) => {

                    let appFolderName = entry.platform + '-' + entry.device;
                    let appPath = options.pathWatch + 'modules/cordova/apps/' + appFolderName;

                    checkFileExist(appPath).then(() => {
                        resolve();
                    }).catch(() => {

                        let ft = new FileTree();
                        let config = {
                            path: 'codebaby/modules/cordova/tpl/app',
                            transform: options.pathProj + '/modules/cordova/apps/' + appFolderName
                        };
                        ft.read(config).then((result) => {

                            let fd = new FileData({path: ''});
                            let handlerData = {fileData: fd, compileList: result.transform};
                            fd.replace = [[/{#PROJ#}/gi, proj.name, '{#PROJ#}'], [/{#APPID#}/gi, entry.appId, '{#APPID#}'], [/{#NAME#}/gi, entry.name, '{#NAME#}'], [/{#PLATFORM#}/gi, entry.platform, '{#PLATFORM#}'], [/{#DEVICE#}/gi, entry.device, '{#DEVICE#}']];
                            handler(handlerData, () => {
                                console.log(' -> ' + chalk.green('cordove module created : ') + chalk.blue(appPath));
                                resolve();
                            })
                        });

                    });
                });
            }
        });

        Promise.all(promises).then(() => {
            console.log(' ');
            resolve();

        })
    })
}

function _getOptions(project, platform, device) {


    let configs = require('../../../projects/' + project + '/modules/cordova/module.config');
    let options;
    configs.apps.some(entry => {

        if (entry.device === device && entry.platform === platform) {
            entry.project = project;
            options = entry;
            return true;
        }
    });
    if (options === undefined) {

        console.log(chalk.red('No config could be found with device = ' + device + ' , platform = ' + platform + ' !'));
        console.log(chalk.yellow('If you have changed these values, delete your cordova apps folder and run build again.'));

    }
    return options;
}
