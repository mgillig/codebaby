const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const child = require('child_process');
const fileUtil = require('../../core/utils/file');
const FileWorks = require('../../core/components/FileWorks');
const _log = require('../../core/utils/logger').log('cordova', false);
const _info = require('../../core/utils/logger').info();


let pathRelease;
let pathCordovaProj;
let pathWs;
let pathWsRel;
let appId;
let appName;
let pathProj;
let pathProjDist;
let proj;
let app;
let appFolderName;
let platform;
let cwdCordova;
let ignore;


module.exports = function (action, options) {

    _log('EXEC CORDOVA  : ' + action);
    _log(options);
    _log(options.project);

    proj = options.project;
    appName = options.name;
    appId = options.appId;
    appFolderName = options.appFolderName;
    // app = options.name + '-' + options.platform + '-' + options.device;
    app = options.platform + '-' + options.device;

    platform = options.platform;

    // console.log( options );

    ignore = options.ignore || [];
    // console.log( process.cwd() );
    // GET WS ROOT RELATIVE TO NODE CWD
    let cwd = process.cwd();
    let index = cwd.lastIndexOf('/projects/' + proj + '/');
    let pathRel = cwd.substr(index, cwd.length);
    let pathFolder = pathRel.split('/');

    pathRel = pathFolder.reduce((pV, cV) => {
        return '../' + pV
    });

    pathWs = path.resolve(pathRel);
    // console.log( path.resolve( pathRel ) );
    // console.log( path.resolve( '../../../../../../' ) );

    pathProj = pathWs + '/projects/' + proj;
    pathProjDist = pathWs + '/dist/' + proj;
    // let pathConfigXml = pathProj + '/module/cordova/apps/' + app + '/config.xml';
    // console.log( __dirname );
    pathWsRel = pathRel;
    // pathRelease = codebaby.paths.pathWs + configBuild.project.deployFolder;
    pathRelease = path.normalize(pathWsRel + '/deploy/cordova/' + proj + '/' + app + '/');
    pathCordovaProj = path.normalize(pathWsRel + '/deploy/cordova/' + proj + '/' + app + '/' + appFolderName);
    // pathCordovaProj = path.normalize( pathWsRel + '/deploy/cordova/' + proj + '/' + app + '/' );
    // console.log( pathRelease );
    cwdCordova = {cwd: pathCordovaProj};

    child.exec('cordova -v ', (error, stdout, stderr) => {
        if (error) {
            console.error(chalk.red(error));
            return;
        }

        if (stderr) {
            console.error(chalk.red(stderr));
            return;
        }
        console.log(chalk.yellow('cordova version') + ' : ' + stdout);
        handle(action);
    });

};

function handle(action) {
    switch (action) {

        case 'create':
            create();
            break;
        case 'platform add':
            updatePlatform('add', platform);
            break;
        case 'platform remove':
            updatePlatform('remove', platform);
            break;
        case 'plugins add':
            updatePlugins('add', platform);
            break;
        case 'plugins remove':
            updatePlugins('remove');
            break;
        case 'prepare':
            prepare();
            break;
        case 'compile':
            cli('cordova compile ', cwdCordova);
            break;
        case 'clean':
            cli('cordova clean ', cwdCordova);
            break;
        default:
            console.log(chalk.red('ation not found :' + action));
    }
    console.log(chalk.yellow(action) + ' ...');
}

function create() {

    fileUtil.mkdir(pathRelease).then(() => {


        let command = 'cordova create ' + appFolderName;
        if (appId) {
            command += ' ' + appId;
        }
        if (appName !== undefined) {
            command += ' \'' + appName + '\'';
        }

        // if ( configCordova.cordova.template !== undefined ) {
        //     // options += ' --copy-from|src=' + data.config.ext_sources;
        //     command += ' --template=' + configCordova.cordova.template;
        // }
        cli(command, {cwd: pathRelease}, (err, code) => {
            updateConfigXML();
            // console.log( 'Ready create' );
        })
    });


}

function updatePlugins(action, platform) {

    let asyncList = [];
    let promises = [];

    function next() {
        let action = asyncList.shift();
        if (action) {
            action();
        }
    }

    let cordovaJs = require(pathProj + '/modules/cordova/apps/' + app + '/cordova.js');
    if (cordovaJs.plugins) {

        cordovaJs.plugins.forEach(plugin => {

            if (plugin.active) {
                let promiseResolve;
                let promiseReject;

                let promise = new Promise((resolve, reject) => {
                    promiseResolve = resolve;
                    promiseReject = reject
                });
                promises.push(promise);
                asyncList.push(function () {
                    cli('cordova plugin ' + action + ' ' + plugin.add, cwdCordova, (err, code) => {
                        if (err) {
                            promiseReject(err)
                        } else {
                            promiseResolve();
                        }
                        next();
                    });
                })
            }
        });

        next();

        // console.log( promises );

        Promise.all(promises).then(() => {
            console.log(chalk.green('all plugins ready !'));
        }).catch((err) => {
            console.log('Error : ' + err);
        });

    } else {

        console.log(chalk.red(' eror : no plugins definend in cordova.js'));

    }


}

function updatePlatform(action, platform) {

    cli("cordova platform " + action + " " + platform, cwdCordova)

}

function prepare() {


    let cordovaJs = require(pathProj + '/modules/cordova/apps/' + app + '/cordova.js');
    let filter = [];
    if (cordovaJs.prepare.ignoreWWW) {
        cordovaJs.prepare.ignoreWWW.forEach(entry => {

            let fileData = path.parse(entry);
            if (fileData.ext === '') {
                filter.push('!' + entry);
            } else {
                filter.push('!' + entry);
            }
        })
    }

    // console.log(cordovaJs);
    // console.log('ignoreWWW');
    // console.log(filter);
    // filter = ['!js','!css'];

    //console.log('remove : ' + pathCordovaProj + '/www/');
    let fw = new FileWorks();
    fw.remove({path: pathCordovaProj + '/www/', filter: filter}).then((result) => {

        let promises = [updateConfigXML(), updateFolderWWW()];
        Promise.all(promises).then(() => {
            cli('cordova prepare ', cwdCordova)
        }).catch(() => {
            console.log(chalk.red('error cordova prepare ! '));
        });
    })
}

function updateConfigXML() {

    return new Promise((resolve, reject) => {
        let pathConfigXml = pathProj + '/modules/cordova/apps/' + app + '/config.xml';
        // console.log( pathConfigXml );
        fs.exists(pathConfigXml, exist => {

            if (exist) {
                // console.log( 'copy config.xml :  ' + pathConfigXml );
                fileUtil.copyFile(pathConfigXml, pathCordovaProj + '/config.xml', () => {
                    resolve()
                });
            } else {
                _log.warn('CONFIG XML DOESNT EXIST !\n' + pathConfigXml);
                reject();
            }
        })
    })
}

function updateFolderWWW() {


    let cordovaJs = require(pathProj + '/modules/cordova/apps/' + app + '/cordova.js');

    let ignoredFolder = [];
    let ignoredFiles = [];
    let filter = [];
    if
    (cordovaJs.prepare.ignoreDist) {

        cordovaJs.prepare.ignoreDist.forEach(entry => {

            let fileData = path.parse(entry);
            if (fileData.ext === '') {
                ignoredFolder.push(entry);
            } else {
                ignoredFiles.push(entry);
            }
        })
    }

    // console.log( 'ignoreDist' );
    // console.log( ignoredFolder );
    // console.log( ignoredFiles );

    return new Promise((resolve, reject) => {
        fs.exists(pathProjDist, (exist) => {
            if (exist) {


                let fw = new FileWorks();
                fw.copy({path: pathProjDist, filter: filter, dest: pathCordovaProj + '/www/', cwd: ''}).then(() => {
                    resolve();
                });
            } else {
                let pathWs = path.resolve('../../../../../../');
                let pathDist = pathProjDist.substr(pathWs.length + 1);
                console.log(chalk.red('!!! ') + chalk.yellow('no compiled project found') + ' : ' + pathDist + '\n-> ' + chalk.yellow('Build your project!'));
                reject();
            }
        });
    })
}

// ------------------------------------- HELPER
function cli(cmd, cwd, cb, log) {

    if (log === undefined) {
        log = true;
    }
    if (process.env.PATH.indexOf(':/usr/local/bin') === -1) {
        process.env.PATH += ':/usr/local/bin';
    }

    cwd.env = process.env;
    let error = false;
    let childProcess = child.exec(cmd, cwd, (error, stdout, stderr) => {

        console.log('\ncmd : ' + chalk.yellow(cmd) + '\n');
        if (stdout) {
            console.log(`${stdout}`);
        }
        if (stderr) {
            console.log(chalk.red(`${stderr}`));
            return
        }
        if (error) {
            console.log('error');
            console.log(error);
            console.log(chalk.red(`${stderr}`));
        }
    });

    childProcess.on('close', function (code) {

        if (code !== 0) {
            error = true;
            code = 'close with code : ' + code;
        }
        if (error) {
            console.log(code);
        } else {
            if (log) {
                console.log(chalk.green('cmd : complete !\n'));
            }

        }

        if (cb) {
            cb(error, code);
        }
    });
}
