const gulp = require( 'gulp' );
const chalk = require( 'chalk' );
const cordova = require( '../../../../../../codebaby/modules/cordova/exec' );
const cordovaModule = require( '../../../../../../codebaby/modules/cordova/index' );

function getOptions() {

    return cordovaModule.getOptions( '{#PROJ#}', '{#PLATFORM#}', '{#DEVICE#}' );
}

gulp.task( 'cordova : 01 create ', function() {
    cordova( 'create', getOptions() );
} );
gulp.task( 'cordova : 02 plugins add ', function() {
    cordova( 'plugins add', getOptions() );
} );
gulp.task( 'cordova : 03 platform add ', function() {
    cordova( 'platform add', getOptions() );
} );
gulp.task( 'cordova : platform remove ', function() {
    cordova( 'platform remove', getOptions() );
} );
gulp.task( 'cordova : plugins remove ', function() {
    cordova( 'plugins remove', getOptions() );
} );
gulp.task( '> app 01 : clean', function() {
    cordova( 'clean', getOptions() );
} );
gulp.task( '> app 02 : prepare', function() {
    cordova( 'prepare', getOptions() );
} );






