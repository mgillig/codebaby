// appName & appId values must match to the values in configXml


module.exports = {

    cordova: {
        active: true,
        path: "dist/{#PROJ#}",
        folder: "{#PROJ#}.{#NAME#}",
        platform: "{#PLATFORM#}",
        appId: "my.app.id",
        appName: "{#NAME#}",
        update: true,
        configXml:'{#PROJ#}/cordova/{#PLATFORM#}-{#NAME#}/config.xml'
    },
    prepare: {
        "ignoreWWW": [ 'cordova.js' ],
        "ignoreDist": [ 'cordova.js','res/icon/android/','res/screen/android/']
    },
    plugins: [
        {
            active: true,
            add: "cordova-custom-config --fetch --save",
            url: "https://github.com/dpa99c/cordova-custom-config"
        }
        ,


        // PERFORMANCE , SECURITY
        {
            active: false,
            add: "cordova-plugin-wkwebview-engine",
            url: "https://github.com/apache/cordova-plugin-wkwebview-engine"
        },
        {
            active: false,
            add: "cordova-plugin-whitelist",
            "depend": "default",
            url: "https://github.com/apache/cordova-plugin-whitelist"
        },
        {
            active: false,
            add: "cordova-plugin-crosswalk-webview",
            url: "https://github.com/crosswalk-project/cordova-plugin-crosswalk-webview"
        },
        {
            active: false,
            add: "cordova-plugin-webserver",
            url: "https://github.com/icenium/cordova-plugin-webserver"
        }
        ,

        // DEVICE ACCESS , CAPABILITYS
        {
            active: false,
            add: "cordova-plugin-splashscreen",
            url: "https://github.com/apache/cordova-plugin-splashscreen"
        },
        {
            active: true,
            add: "cordova-sqlite-storage",
            url: "https://github.com/litehelpers/Cordova-sqlite-storage"
        }
        ,
        {
            active: false,
            add: "cordova-plugin-device-motion",
            url: "https://github.com/apache/cordova-plugin-device-motion"
        },
        {
            active: true,
            add: "cordova-plugin-statusbar",
            url: "https://github.com/apache/cordova-plugin-statusbar"
        },
        {
            active: false,
            add: "cordova-plugin-device",
            url: "https://github.com/apache/cordova-plugin-device"
        },
        {
            active: true,
            add: "cordova-plugin-network-information --save",
            url: "https://github.com/apache/cordova-plugin-network-information"
        }
        ,
        {
            active: true,
            add: "cordova-plugin-geolocation",
            url: "https://github.com/apache/cordova-plugin-geolocation.git"
        }
        ,
        {
            active: false,
            add: "cordova-plugin-x-socialsharing",
            url: "https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git"
        },


        // FILE HANDLING

        {
            active: true,
            add: "cordova-plugin-file-transfer",
            url: "https://github.com/apache/cordova-plugin-file-transfer"
        },
        {
            active: true,
            add: "cordova-plugin-file",
            "depend": "cordova-plugin-file-transfer",
            url: "https://github.com/apache/cordova-plugin-file"
        }
        ,
        // CAMARA , LIBRARY
        {
            active: true,
            add: "cordova-plugin-image-picker",
            url: "https://github.com/wymsee/cordova-imagePicker"
        }
        ,
        {
            active: true,
            add: "cordova-plugin-camera-with-exif",
            url: "https://github.com/remoorejr/cordova-plugin-camera-with-exif"
        }
        ,
        {
            active: false,
            add: "cordova-plugin-camera",
            url: "https://github.com/apache/cordova-plugin-camera.git"
        },
        {
            active: false,
            add: "cordova-plugin-photo-library",
            url: "https://github.com/terikon/cordova-plugin-photo-library"
        }
    ]
}