module.exports = {
    apps: [


        // only appId & ignore can be changed after a cordova project has been build
        // for changeing other values , you have to run project build again.
        // !!! copy changes of your config.xml & plugins.js to the new folder, then delete the old one !!!

        //----------------------------------------
        {
            active: false,
            name: 'MyApp', // no spaces in app name
            appFolderName: 'myApp',
            platform: 'ios',
            device: 'mobile',
            appId: 'my.app.id',
            ignore: ['cordova.js']
        },
        //----------------------------------------
        {
            active: false,
            name: 'MyApp', // no spaces in app name
            appFolderName: 'myApp',
            platform: 'android',
            device: 'mobile',
            appId: 'my.app.id',
            ignore: ['cordova.js']
        }
    ],

    dependencies: [{name: "gulp"}]
};