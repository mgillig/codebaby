module.exports = {
    active: false,
    watch: true,
    childProcess: true,
    type: 'module-express',
    dependencies: [
        { name: "express" },
        { name: "body-parser" }
    ]
};
