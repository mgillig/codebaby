'use strict';
const express = require( 'express' );
const http = require( 'http' );
const chalk = require( 'chalk' );
const bodyParser = require( 'body-parser' );
let _log = require( '../../../../codebaby/core/utils/logger' ).log( 'express', false, 1 );

let port = 5000;
let server;
let app;




//http://expressjs.com/de/
app = express();
app.use( bodyParser.json( { limit: '50mb' } ) );
app.use( bodyParser.urlencoded( { limit: '50mb', extended: true } ) );
// app.use( bodyParser.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded

app.get( '/', function( req, res ) {
    res.send( '<h1>Hello World !</h1>' );
} );


app.post( '/test', function( req, res, next ) {

    // Handle the post for this route
    res.header( "Access-Control-Allow-Origin", "*" );
    res.header( "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept" );


    _log( req.body );

    let responseData = { errorCode: 0, message: '' };

    if ( req.body ) {
        try {
            let data = JSON.parse( req.body.data );
            _log( data, 1 );
            responseData.message = data.name + ' hallo! '
        } catch ( err ) {

            responseData.errorCode = 1;
            _log( 'json parse error' );
            _log( err );

        }

    }
    res.format( {
        'application/json': function() {
            res.send( responseData );
        }
    } );
    // let data = { name: 'michael' };
    // console.log(  JSON.stringify( data ) );
    // res.send(  JSON.stringify( data ) )
    _log( 'ende', 2 );
} );


try {
    server = http.createServer( app );
    // server.setTimeout( 1000 );
    server.listen( port, () => {
        console.log( ' -> ' + chalk.green( 'module express running on port ' ) + ' : ' + port + '\n' );
    } );

} catch ( err ) {

    _log( err )

}


