const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const fileUtil = require('../../core/utils/file');
const FileWorks = require('../../core/components/FileWorks');
const _log = require('../../core/utils/logger').log('module-express', false);

let pathDest;
let moduleName = 'express';

module.exports = {
    setup: _setup,
    build: _build
};

function _setup(proj) {

    _log('_setup ' + proj);


    pathDest = '/projects/' + proj + '/modules/' + moduleName + '/';
    let pathConfigDest = pathDest + 'config.js';

    return new Promise((resolve) => {
        fileUtil.mkdir('/projects/' + proj + '/modules/' + moduleName).then(() => {

            checkFileExist(pathConfigDest).then(() => {
                console.log(chalk.red('file exist :\n/projects/' + proj + '/modules/' + moduleName + '/config.js'));
                resolve();
            }).catch(() => {
                let fw = new FileWorks();
                fw.copy({path: 'codebaby/modules/express/tpl', dest: pathDest}).then(() => resolve());
            });
        });
    })
}

function _build(options, proj) {

    _log('_build ');
    _log(options);
    _log(proj);

    // pathDest = pathWs + '/projects/' + proj;
    let pathTo = path.normalize(options.pathProj + '/modules/' + moduleName + '/express.js');
    let pathFrom = path.normalize(options.pathCodebaby + '/modules/' + moduleName + '/express.js');

    return new Promise((resolve) => {
        resolve();
    });
}

function checkFileExist(path) {

    return new Promise((resolve, reject) => {

        fs.exists(path, (exist) => {
            if (exist) {
                resolve()
            } else {
                // console.log( chalk.red( 'file not found :\n' + path ) );
                reject();
            }
        })
    })
}
