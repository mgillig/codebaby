
![codebaby](https://gitlab.com/mgillig/codebaby/raw/master/core/process/assets/codebaby.png)

codebaby
=========

Frontend lab.

##info
codebaby is a out of the box frontend lab to easy setup frontend
projects. 

codebaby is the working horse of a real word agency, which means it is stable and build for productivity.
Sadly theres no documentation at the moment.

##Requirements
* os : macOS
* node : >=6.0.0
* npm  : >=3.3.4
* curiosity

##Capabilities
* ready for creating react, vue, typescript or classic ES5 projects.
* multiple projects with different project types in the same workspace.
* automated project creation , with tasks for development, production, remote and testing.
* global deployment script to create versioned project builds ( optional as .zip files ).
* integrated preprocessing options, to simplify project modifications.
* javascript bundle with rollup.js & babel.js.
* html templating with ejs.
* css creation with node.sass & postcss.
* img optimisation with imagemin.
* testing with tape, jasmine or mocha.
* integrated cordova module to build apps.
* integrated express module to handle server interaction.




## Installation

  ````
  npm install codebaby
  ````

## Usage

after installion a npm script is added to your packacke.json
````
  cb:setup
````
these task will setup all active projects defined in codebaby.config.js :


````js
  module.exports = {
         "projects": {
             "foo": "", // es5
             "foo-babel-env": "tpl=babel-env", // es6++
             "foo-vue": "tpl=vue",
             "foo-react": "tpl=react",
             "foo-react-typescript": "tpl=react-typescript",
             "foo-angular-js": "tpl=angular-js",
             "foo-angular-js-typescript": "tpl=angular-js-typescript",
             "foo-typescript": "tpl=typescript",
         }
     };
````

After runing the script 'cb:setup' your
package.json will be updated with new scripts to lanch your project(s).

````
  foo:dev
  foo:production
  foo:remote
  foo:test
````
( task options )
- dev -> NODE_ENV = 'develop'
- production -> NODE_ENV = 'production'
- remote -> remote mode 
- test -> execute all tests (if tests enabled in projects/../config/build.js)

A new project 'foo' is created under /projects/foo.
````
Run script foo : dev
````

Codebaby asks you for installation of additional dependencies which are required ( depending on your project type ). Confirm installation in your console window, and the project will be launched in chrome browser.


Play with the code under project/foo/src. 

