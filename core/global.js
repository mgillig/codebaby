'use strict';
const path = require('path');
const chalk = require('chalk');


const version = '1.0.3';
const messurePoints = {};

class Codebaby {

    constructor(){
        this.preprocessEnv = undefined;
        this.build = undefined;
        this.version = version;

        this.state = {
            buildComplete: false,
            production: false,
            proxy: false,
            env: '',
            remote: false,
            debug: false,
            test: false,
            npmSaveExact: true,
            npmTaskMode: 'd:p:r:t' // d=dev, p=productiom ,r=remote, t=test
        };

        this.paths = {
            pathWs: undefined,
            pathProj: undefined,
            pathSrc: undefined,
            pathDist: undefined,
            pathCodebaby: undefined,
            pathWatch: undefined
        };
        this.remote = {
            port: undefined,
            log: true,
            host: undefined,
            logUrl: undefined,
            logPort: undefined
        };

        this.browser = {
            hostUrl: undefined,
            urlExtern: '',
            ip: undefined,
            logPort: undefined
        };


        this.preprocessReplace = [];
    }
    initArgs(args) {


        let src = '/src';
        let dist = '/dist';
        if (args.project) {

            // process.chdir( path.normalize( process.cwd() + '/projects/' + args.project ) );
            if (process.env.PATH.indexOf(':/usr/local/bin') === -1) {
                process.env.PATH += ':/usr/local/bin';
            }
            this.build = require('../../projects/' + args.project + '/config/build');


            if (this.build) {
                if (this.build.dependencies === undefined) {
                    this.build.dependencies = [];
                }
                if (this.build.tests === undefined) {
                    this.build.tests = [];
                }
                if (this.build.preprocess) {

                    if( this.preprocessEnv ===undefined){
                        this.preprocessEnv = {};
                    }


                    //INIT DEFAULTS
                    Object.keys(this.build.preprocess).forEach(entry => {

                        if (this.preprocessEnv[entry] === undefined) {
                            this.preprocessEnv[entry] = '' + this.build.preprocess[entry];
                        }

                        process.env[entry] = this.preprocessEnv[entry];
                    })
                }


                if (this.build.project) {
                    if (this.build.project.srcFolder) {
                        src = this.build.project.srcFolder;
                    } else {
                        console.log(chalk.red('error - no srcFolder defined inside build.js'));
                    }
                    if (this.build.project.buildFolder) {
                        dist = this.build.project.buildFolder;
                    } else {
                        console.log(chalk.red('error - no buildFolder defined inside build.js'));
                    }
                } else {
                    console.log(chalk.red('error - no projet defined inside build.js'));
                }

            } else {
                console.log(chalk.red('error - no projet defined as build arg!'));
            }

            this.remote.log = args.remote || false;
            this.state.production = args.production || false;
            this.state.remote = args.remote || false;

            if (process.cwd().indexOf('/projects/') > -1) {
                this.paths.pathProj = path.normalize(process.cwd() + '/');
                this.paths.pathWs = path.normalize(path.resolve('../../') + '/');
                this.paths.pathSrc = path.normalize(this.paths.pathProj + '/' + src + '/');
                this.paths.pathDist = path.normalize(this.paths.pathWs + '/' + dist + '/');
                this.paths.pathCodebaby = path.normalize(this.paths.pathWs + '/codebaby/');
                this.paths.pathWatch = '';

            } else {
                this.paths.pathProj = path.normalize(process.cwd() + '/projects/' + args.project + '/');
                this.paths.pathWs = path.normalize(process.cwd() + '/');
                this.paths.pathSrc = path.normalize(this.paths.pathProj + '/' + src + '/');
                this.paths.pathDist = path.normalize(this.paths.pathWs + '/' + dist + '/');
                this.paths.pathCodebaby = path.normalize(this.paths.pathWs + '/codebaby/');
                this.paths.pathWatch = 'projects/' + args.project + '/';

            }

            // console.log( process.cwd() );
            // console.log( this.paths );


            this.state.env = this.state.production ? 'production' : 'develop';

            process.env['NODE_ENV'] = this.state.env;
            this.preprocessEnv['NODE_ENV'] = this.state.env;

            if (this.preprocessEnv !== undefined) {
                Object.keys(this.preprocessEnv).forEach(key => {

                        process.env[key] = this.preprocessEnv[key];

                    }
                );
            }


        }
    };
    messureStart(flag = 'time') {
        if (this.state.test) {
            messurePoints[flag] = new Date().getTime();
        }
    }
    messureEnd(flag = 'time') {

        if (this.state.test && messurePoints[flag]) {
            console.log(flag + ' : ' + chalk.yellow((new Date().getTime() - messurePoints[flag])));
        }
    }
    update(data) {

        // console.log( this.remote.log);
        // console.log( data );

        if (data.ip !== undefined && data.port !== undefined) {

            this.browser.urlExtern = data.external;
            this.browser.ip = data.ip;
            this.remote.port = data.port;
            this.remote.logUrl = (this.state.remote === true) ? 'http://' + data.ip + ':' + (data.port + 1) + '/' : '';
            this.remote.host = (this.state.remote === true) ? data.external : '';
            this.remote.logPort = data.port + 1;
            this.browser.url = (this.state.remote === true) ? this.browser.urlExtern : '';

        }
        if (this.build !== undefined) {


            Object.keys(this.preprocessEnv).forEach(key => {

                switch (key) {
                    case 'NODE_ENV':
                        this.preprocessEnv.NODE_ENV = process.env['NODE_ENV'];
                        break;
                    case 'TIMESTAMP':
                        this.preprocessEnv.TIMESTAMP = new Date().getTime();
                        break;
                    case 'REMOTE_LOG':
                        this.preprocessEnv.REMOTE_LOG = this.remote.log;
                        this.preprocessEnv.REMOTE_LOG = 'false';
                        process.env['REMOTE_LOG'] = this.remote.log;
                        break;
                    case 'REMOTE_HOST':
                        this.preprocessEnv.REMOTE_HOST = this.remote.host;
                        process.env['REMOTE_HOST'] = this.remote.host;
                        break;
                    case 'REMOTE_LOG_URL':
                        this.preprocessEnv.REMOTE_LOG_URL = this.remote.logUrl;
                        process.env['REMOTE_LOG_URL'] = this.remote.logUrl || '';
                        break;
                    case 'HOST_URL':
                        this.preprocessEnv.HOST_URL = this.browser.url;
                        process.env['HOST_URL'] = this.browser.url || '';
                        break;
                    default:
                }

                this.preprocessReplace.push({
                    regex: new RegExp("{#" + key + "#}", "g"),
                    val: this.preprocessEnv[key],
                    key: key
                });
            });

            this.preprocessReplace.forEach(entry => {
                if (entry.key === 'TIMESTAMP') {
                    entry.val = this.preprocessEnv.TIMESTAMP;
                    process.env['TIMESTAMP'] = this.preprocessEnv.TIMESTAMP || '';
                }
            });
            // console.log(this.preprocessEnv);
        }
    }

}
module.exports = new Codebaby();





