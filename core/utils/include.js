const fs = require( 'fs' );
const path = require( 'path' );
const httpUtil = require( './http' );
const stringUtil = require( './string' );

module.exports = function( str, includePattern, cb ) {

    let error = '';

    if ( includePattern === undefined || includePattern === '' ) {
        includePattern = [ 'require(', ')' ];
    }

    let pathNodeModules = process.cwd() + '/node_modules/';
    let code = stringUtil.extractPattern( str, includePattern, '!' );

    let promises =  code.map( entry => {
        if ( entry.extract ) {
            let pathClean = entry.extract.replace( /['"\s]/g, '' );
            if ( pathClean.indexOf( 'https://' ) > -1 || pathClean.indexOf( 'http://' ) > -1 ) {


                return new Promise( ( resolve ) => {

                    return httpUtil.curl( pathClean ).then( result => {

                        entry.pattern = result;
                        resolve();
                    } ).catch( e => {
                        console.log( e );
                        resolve();
                    } );
                } )

            } else {

                let pathInclude = path.normalize( pathNodeModules + '/' + pathClean );
                if ( fs.existsSync( pathInclude ) ) {
                    entry.pattern = fs.readFileSync( pathInclude, 'utf-8' );
                }
            }
        }
    } );


    Promise.all( promises ).then( () => {
        let result = code.reduce( ( acc, entry ) => {
            return acc + entry.pattern;
        }, '' );

        if ( error === '' ) {
            error = false;
        }
        cb( error, result );

    } )


};