const http = require( 'http' );

module.exports = {
    curl: _curl
};


function _curl( url, port ) {

    let host = '';
    let hostPath = '';
    if ( url.indexOf( 'https://' ) > -1 ) {
        url = url.replace( /https:\/\//, '' )
    }
    if ( url.indexOf( 'http://' ) > -1 ) {
        url = url.replace( /http:\/\//, '' )
    }
    let pathStart = url.indexOf( '/' );
    if ( pathStart > -1 ) {
        host = url.substring( 0, pathStart );
        hostPath = url.substring( pathStart, url.length );
    }

    let options = {
        host: host,
        path: hostPath
    };
    if ( port ) {
        options.port = port;
    }

    return new Promise( ( resolve, reject ) => {

        let req = http.request( options, function( res ) {
            let data='';
            res.setEncoding( 'utf8' );
            res.on( 'data', function( chunk ) {
                data +=chunk;
            } );

            res.on( 'end', function( e ) {
                resolve( data )
            } );

        } );

        req.on( 'error', function( e ) {
            reject( 'problem with request: ' + e.message );
        } );
        req.end();

    } )

}