'use strict';
module.exports = {

    parse: function( params ) {

        let argsList;
        let args;
        if ( params === undefined ) {
            argsList = process.argv.slice( 2, process.argv.length );
        } else {
            if ( params !== '' ) {
                args = {};
                argsList = params.split( ' ' );
                argsList.forEach( arg => {

                    let split = arg.split( '=' );
                    let key = split[ 0 ];
                    let val = split[ 1 ];

                    if ( val === 'false' ) {
                        val = false;
                    }
                    if ( val === 'true' ) {
                        val = true;
                    }
                    // if ( key === 'p' || key === 'production' ) {
                    //     key = 'production';
                    //
                    //     if ( val === undefined ) {
                    //         val = true
                    //     }
                    // }
                    // if ( key === 'r' || key === 'remote' ) {
                    //     key = 'remote';
                    //     if ( val === undefined ) {
                    //         val = true
                    //     }
                    // }

                    if ( args[ key ] !== undefined ) {
                        let currArgs = args[ key ];
                        // console.log( 'MULTIPLE ARGS : ' + key );
                        // console.log( currArgs );
                        // console.log( typeof currArgs );
                        if ( typeof args[ key ] !== 'object' ) {
                            // console.log( 'CREATE LIST ARGS' );
                            args[ key ] = [ currArgs, val ];
                            // console.log( args );
                        } else {
                            // console.log( 'PUSH ARGS' );
                            // console.log( currArgs );
                            args[ key ].push( val )

                        }


                    } else {
                        // console.log( '----> ADD  ARGS : ' + key + ' :' + val );
                        args[ key ] = val;
                    }

                } );

                // if ( args[ 'production' ] == undefined ) {
                //     args[ 'production' ] = false;
                // }
                // if ( args[ 'remote' ] === undefined ) {
                //     args[ 'remote' ] = false;
                // }
                // console.log( '##################' );
                // console.log( args );

            }

        }

        return args;
    }
    ,
    split: function( prefix ) {


        // node uses - as prefix for options
        // npm uses -- as seperator for options


        if ( prefix === undefined ) {
            prefix = '-';
        }


        let args = process.argv.slice( 2, process.argv.length );
        // args = ['cmd1','--','option1','--','option2','cmd2','--','option3','--','option4','comd4','cmd5','--','option43']
        // console.log( args );

        // let test = args.concat();
        // console.log( test );
        let commands = [];
        let options = [];
        let isCommand = true;
        let indexCommand = 0;
        let currCmd;

        args.forEach( ( flag, indexFlag ) => {
                if ( flag !== prefix ) {

                    if ( isCommand ) {
                        // console.log('comand : '+ flag );
                        currCmd = [ flag, [] ];
                        commands.push( currCmd );
                    } else {
                        // console.log('option : '+ flag );
                        if ( currCmd !== undefined ) {
                            currCmd[ 1 ].push( flag );
                        }
                        isCommand = true;
                    }

                } else {
                    isCommand = false;
                }
            }
        );
        // console.log( commands );
        // console.log( '--------------' );
        // console.log( commands );

        return commands;

    }
};


