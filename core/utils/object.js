module.exports = {


    cloneDeep: _cloneDeep


};


function _cloneDeep( src ) {


    function getDefaultType( src ) {
        if ( typeof src === 'object' ) {
            if ( src.length !== undefined ) {
                return [];
            } else {
                return {};
            }
        }
    }

    function walk( obj, newObj ) {

        if ( typeof obj === 'object' ) {
            if ( obj.length !== undefined ) {
                newObj = obj.map( listEntry => walk( listEntry, getDefaultType( listEntry ) ) )
            } else {

                Object.keys( obj ).forEach( prop => {
                    let data = obj[ prop ];
                    newObj[ prop ] = walk( data, getDefaultType( data ) );
                } )
            }
        } else {
            newObj = obj;
        }
        return newObj;
    }

    return walk( src, getDefaultType( src ) );
}