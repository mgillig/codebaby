
const _log = require( './logger' ).log( 'stringUtils', false );
const _info = require( './logger'  ).info();


module.exports = {
    extractPattern: _extractPattern
};


function _extractPattern( sourceStr, pattern, ignorePattern ) {

    // let sourceStr = 'require( "monika");require("thomas")require( "petra") ; require( "sabine" );';
    // let pattern = [ 'require(', ')' ];
    let code = [];
    if(ignorePattern ===undefined){
        ignorePattern ='!';
    }

    function getPattern( str ) {

        // console.log( '-------------' );
        // console.log( str );
        let patternStart = str.indexOf( pattern[ 0 ] );
        let patternEnd = str.indexOf( pattern[ 1 ] );
        let patternStartIgnore;
        let strExtract;
        let strBefore;
        let strAfter;

        if ( patternStart > -1 && patternEnd > -1 ) {
            // console.log( 'patternStart : ' + patternStart );
            // console.log( 'patternEnd : ' + patternEnd );
            // console.log( '--' );
            let hasPattern = ( patternStart > -1 );
            let validPattern = true;

            if ( hasPattern ) {
                let patternIgnore = str.substring( 0, patternEnd );
                patternStartIgnore = patternIgnore.indexOf( ignorePattern + pattern[ 0 ] );
                validPattern = patternStartIgnore === -1;

                if ( validPattern ) {

                    let closedCorrect = str.substring( patternStart, patternEnd );
                    let indexPatterStart = closedCorrect.lastIndexOf( pattern[ 0 ] );

                    strExtract = str.substring( patternStart + pattern[ 0 ].length, patternEnd );
                    if ( indexPatterStart > 0 ) {
                       _log( 'not closed correctly' );
                       _log( closedCorrect );
                       _log( 'indexPatterStart :  ' + indexPatterStart );
                       _log( 'patternStart :  ' + patternStart );
                        strExtract = strExtract.substring( strExtract.indexOf( pattern[ 0 ] ) + pattern[ 0 ].length, strExtract.length )
                        validPattern = false;
                    } else {
                    }

                   _log( 'strExtract : ' + strExtract );
                } else {
                    patternStart = patternStartIgnore;
                }
                //_log.log( 'patternStartIgnore : ' + patternStartIgnore );
                //_log.log( 'validPattern : ' + validPattern );
            }
            strBefore = str.substring( 0, patternStart );
            strAfter = str.substring( patternEnd + 1, str.length );


            //_log( '' );
            //_log( 'strBefore : ' + strBefore );
            //_log( 'strAfter : ' + strAfter );
            code.push( { pattern: strBefore } );
            if ( validPattern ) {
                code.push( { extract: strExtract } );
            }

            if ( strAfter.length > 0 ) {
                getPattern( strAfter )
            } else {
                code.push( { pattern: strAfter } )
            }
        } else {
            //_log( '------------------------ ready' );
            code.push( { pattern: str } )
        }
    }

    if ( sourceStr && pattern ) {
        getPattern( sourceStr );
    } else {
        console.log( 'stringUtil : error' );
        if ( !sourceStr ) {
            console.log( 'empty str' );
        }
        if ( !pattern ) {
            console.log( 'empty pattern' );
        }
    }

    return code;

}