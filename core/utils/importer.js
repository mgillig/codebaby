const path = require( 'path' );
const fs = require( 'fs' );
const { spawn } = require( 'child_process' );
const libRollup = require( '../../handler/javascript/libs/libRollup' );
// const typescript = require( '../../handler/javascript/libs/libtypescript' );

let importType;
const ROLLUP = 'ROLLUP';
const outDirTest = '_compiled_test';

module.exports = {
    ROLLUP: ROLLUP,
    init: _init,
    get: function( path, cb ) {
        return rollup( path ).then( code => cb( code ) );
    }
};


function _init( pathHost, pathCompileRoot, type = ROLLUP ) {


    // console.log( 'compile root : ' + pathCompileRoot );

    importType = type;
    // let pathWs = path.resolve( pathHost, '../../../../' );
    let pathProj = path.resolve( pathHost, '../../../' );

    // let proj = pathProj.substr( pathProj.lastIndexOf( '/' ) + 1, pathProj.length );
    let pathSrcRoot = path.normalize( pathProj + '/' + pathCompileRoot );
    // console.log( 'proj : ' + proj );
    // console.log( 'pathWs : ' + pathWs );
    // console.log( 'pathProj : ' + pathProj );
    // console.log( 'pathSrcRoot : ' + pathSrcRoot );
    let config;
    let pathRollupConfig = path.normalize( pathSrcRoot + '/rollup-config.js' );
    if ( fs.existsSync( pathRollupConfig ) ) {
        config = require( pathRollupConfig );
    }
    // if ( type === ROLLUP ) {
    //     config = require( path.normalize( pathWs + '/' + pathCompileRoot + '/rollup-config.js' ) );
    // }
    let tsConfig;
    let pathTsConfig = path.normalize( pathSrcRoot + '/tsconfig.json' );
    if ( fs.existsSync( pathTsConfig ) ) {
        tsConfig = require( pathTsConfig );
    }

    // console.log( config );
    // console.log( tsConfig );


    return function( pathTest ) {
        return new Promise( resolve => {
            // console.log( config );

            config.output = { format: 'cjs' };

            // console.log( config );

            if ( type === ROLLUP ) {

                let pathData = path.parse( pathTest );
                if ( pathData.ext === '.ts' ) {
                    // console.log( 'TYPESCRIPT : ' + pathTest );
                    // config.input = pathWs + '/' + pathCompileRoot + outDirTest + config.input.replace( /.ts/, '.js' );

                    let strArray = parseTsconfigToCli( tsConfig, [ 'watch', 'outDir', 'sourceMap' ] );
                    let options = [ path.normalize( pathSrcRoot + '/' + pathTest ) ].concat( strArray );
                    let pathCompiled = path.normalize( pathSrcRoot + '/' + outDirTest + '/' + pathTest );

                    // console.log( 'pathCompiled : '+pathCompiled );
                    // console.log( 'pathCompiled : '+pathCompiled.replace( /\.ts/, '.js' ) );
                    options.push( '--outDir' );
                    options.push( path.normalize( pathSrcRoot + '/' + outDirTest ) );
                    // console.log( options );

                    typescript( options, ( err ) => {

                        // console.log( 'TS READY ' );
                        // console.log( err );
                        if ( err ) {
                            console.log( 'typescript ready : error' );
                        } else {
                            // config.input = pathTest;
                            config.input = pathCompiled.replace( /\.ts/, '.js' );
                            bundleRollup( config, resolve );
                        }


                    } );
                } else {
                    config.input = pathSrcRoot + pathTest;
                    bundleRollup( config, resolve );
                }

                // bundleRollup( config, resolve );
                // libRollup( config, {}, null, ( err, result ) => {
                //
                //
                //     if ( err ) {
                //         console.log( err );
                //     } else {
                //         // console.log( result );
                //         let code = eval( result.code );
                //         // console.log( code );
                //         resolve( code );
                //     }
                // } )

            } else {

                console.log( 'UNKNOWN BUNDLE TYPE' );
            }

        } );
    }

}

function typescript( options, cb ) {
    // let options = [];
    // options.push( '--project' );
    // options.push( configPath );
    // console.log( options );

    const ls = spawn( 'tsc', options, {} );
    let output = '';
    let errors = '';
    let error = false;

    // console.log( options );
    ls.stdout.on( 'data', ( data ) => {
        output += data;
        error = false;
        let textChunk = handleOnData( 'stdout', data, cb );
        console.log( textChunk );


    } );

    ls.stderr.on( 'data', ( data ) => {
        errors += data;
        let textChunk = handleOnData( 'stderr', data, cb );
        // console.log( textChunk );
    } );
    ls.on( 'close', ( code ) => {

        // console.log( 'CLOSE' );
        // console.log( typeof  code );
        // console.log( code );
        if ( code === 0 ) {
            cb( null );
        } else {
            cb( 'error code : ' + code );
        }
    } );


}
function bundleRollup( config, cb ) {
    libRollup( config, {}, null, ( err, result ) => {


        if ( err ) {
            console.log( err );
        } else {
            // console.log( result );
            let code = eval( result.code );
            // console.log( code );
            cb( code );
        }
    } )

}
function handleOnData( type, data, cb ) {
    let textChunk = data.toString( 'utf8' );
    // console.log( type + ' data ' );
    // console.log( textChunk );
    return textChunk;

}

function parseTsconfigToCli( data, ignoreList = [] ) {


    let keys = Object.keys( data.compilerOptions ).filter( prop => ignoreList.indexOf( prop ) === -1 );
    let compilerOptions = data.compilerOptions;
    let list = [];
    keys.forEach( ( prop ) => {
        let val = compilerOptions[ prop ];
        if ( typeof val === 'object' && val.length ) {
            list.push( '--' + prop );
            list.push( '[' + val.toString() + ']' );

        } else {
            list.push( '--' + prop );
            list.push( val );
        }
    } );
    return list;

}



