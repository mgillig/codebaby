'use strict';
const chalk = require( 'chalk' );
let globalState = false;
let globalInfo = true;

let states = {};


let logger = {
    info: function() {
        return function( mess, newLine ) {
            if ( globalInfo ) {
                process.stdout.write( mess );
                if ( newLine ) {
                    process.stdout.write( '\n' );
                }
            }
        }
    },

    log: function( id, state, level ) {

        if ( level === undefined ) {
            level = 0;
        }
        states[ id ] = state;

        return function( mess, myLevel, inline ) {

            if ( inline === undefined ) {
                inline = false;
            }

            if ( globalState ) {
                state = globalState;
            }
            if ( state ) {
                globalInfo = false;
            }
            if ( myLevel === undefined ) {
                myLevel = 0;
            }

            if ( state && myLevel >= level ) {
                if ( inline ) {
                    console.log( '\n' + myLevel + ' - ' + chalk.magenta( id ) + ' :  ' + mess );
                } else {

                    console.log( '\n' + myLevel + ' - ' + chalk.magenta( id ) );
                    console.log( '' );
                    console.log( mess );

                }
            }
        }
    }
};

logger.debug = ( state ) => {
    globalState = state;
    return logger;
};
module.exports = logger;
