module.exports = {

    promList: _promList,
    promListSync: _promListSync


};

function _promList( functions, max ) {

    return new Promise( resolve => {


        let list = [];
        let promisesAll = [];
        let stackSeq;
        let promResolve;

        if ( max === undefined ) {
            max = functions.length;
        }

        function next() {

            let stack = list.shift();
            if ( stack ) {
                stack.list.map( func => func() );
                Promise.all( stack.promises ).then( () => next() )
            }
        }

        functions.forEach( ( func, i ) => {
            if ( i % max === 0 ) {
                stackSeq = { list: [], promises: [] };
                list.push( stackSeq );
            }

            let prom = new Promise( resolve => promResolve = resolve );
            stackSeq.promises.push( prom );
            promisesAll.push( prom );
            stackSeq.list.push( func( promResolve ) );

        } );

        next();
        Promise.all( promisesAll ).then( () => resolve() )
    } )

}

function _promListSync( functions ) {

    return new Promise( resolve => {


        let list = [];
        let promisesAll = [];


        function next() {
            let func = list.shift();
            if ( func ) {
                func();
            }
        }

        list = functions.map( ( func ) => {
            let promResolve;
            let prom = new Promise( resolve => promResolve = resolve );
            let ready = () => {
                promResolve();
                next();
            };
            promisesAll.push( prom );
            return func( ready )
        } );

        next();
        Promise.all( promisesAll ).then( () =>  resolve())
    } )

}