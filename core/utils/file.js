const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const {promListSync} = require('./promise');
const _log = require('./logger').log('fileUtil', false);
const _info = require('./logger').info();


function _pathContains(pathA, pathB) {
    // let include = true;
    // let folderA = path.normalize('/' + pathA + '/').split('/').filter(entry => {
    //     if (entry !== '') {
    //         return entry
    //     }
    // });
    // let folderB = path.normalize('/' + pathB + '/').split('/').filter(entry => {
    //     if (entry !== '') {
    //         return entry
    //     }
    // });
    // // console.log( folderA );
    // // console.log( folderB );
    //
    // folderA.some((entry, index) => {
    //
    //     if (folderB[index] !== entry) {
    //         include = false;
    //         return true;
    //     }
    // });
    //
    // // console.log( folderA );
    // // console.log( folderB );
    // // console.log( '######## ' +include);
    //
    // return include;


    if(pathB[pathB.length] !=='/'){
        pathB +='/';
    }
    return pathB.indexOf(pathA) !== -1;
}

function _writeFileSync(pathDest, data) {


    _checkPathTo(pathDest, () => {
        fs.writeFileSync(pathDest, data)
    })

}


function _copyStream(pathFrom, pathDest) {

    let from = pathFrom;
    let to = pathDest;


    return new Promise(resolve => {

        function ready(err) {

            if (err) {
                console.log(err);
            }
            resolve(err)
        }

        let streamWrite = fs.createWriteStream(to);
        streamWrite.on('close', () => ready());
        streamWrite.on('error', err => ready(err));
        fs.createReadStream(from).pipe(streamWrite);

    })

}

function _mkdir(paths, cb) {





    let regexCwd = new RegExp(process.cwd());

    return new Promise(resolve => {


        if (typeof paths === 'string') {
            paths = [paths];
        }
        
        // parse paths in a complete
        // folder structure
        // filter double entrys


        let pathList = [];
        let collection = {};

        paths.forEach(entry => {

            let pathRel = entry.replace(regexCwd,'');
            // console.log( 'entry : '+entry +' ->  ');
            // console.log( 'pathRel : '+pathRel);
            // console.log( '' );


            let pathEntry = pathRel.split(path.sep);
            let pathStr = '';

            pathEntry.forEach(base => {

                if (base !== '') {
                    pathStr += base + path.sep;
                    if (collection[pathStr] === undefined) {
                        pathList.push(pathStr);
                        collection[pathStr] = pathStr;
                    }
                }
            });
        });


        let functionList = pathList.sort(sort).map(entry => {

            return (next) => {

                return () => {
                    fs.exists(entry, exist => {
                        // console.log( 'entry : '+entry +' ->  '+exist);
                        if (exist) {
                            next();
                        } else {



                            fs.mkdir(entry, err => {
                                if (err) {
                                    if (err.code !== 'EEXIST') {
                                        console.log(err);
                                    }
                                }
                                next();
                            })
                        }
                    });
                }
            }
        });


        promListSync(functionList).then(() => {

            // console.log( 'READY _mkdir : '+path.resolve()+paths );
            // console.log( fs.existsSync(path.resolve()+paths) );
            if (cb) {
                cb()
            }
            resolve();
        })
    });
}

// ----------------------------- helper
function sort(a, b) {
    if (a > b) {
        return 1
    }
    if (a < b) {
        return -1
    }
    if (a === b) {
        return 0
    }
}

function _checkPathTo(pPath, cb) {

    let paths;
    let indexFile = pPath.lastIndexOf('.');
    if (indexFile > -1) {
        pPath = pPath.substr(0, pPath.lastIndexOf('/'));
    }
    if (fs.existsSync(pPath)) {

        cb();
    } else {

        paths = pPath.split('/');
        let pathBase = '';
        let exist = true;

        paths.forEach(part => {

            pathBase += part + '/';
            if (exist) {
                exist = fs.existsSync(pathBase);
            }
            if (!exist) {
                fs.mkdirSync(pathBase);
            }
        });
        cb();
    }
}

function _getJson(pPath, cb) {
    fs.exists(pPath, exist => {

        if (exist) {

            fs.readFile(pPath, 'utf-8', (err, result) => {
                if (err === null) {
                    try {
                        let data = JSON.parse(result);
                        cb(null, data);
                    } catch (e) {

                        cb(e);
                    }
                } else {
                    cb(err);
                }
            })
        } else {
            cb({'error': 'file not found !', pPath: pPath});
        }
    });

}

function _copyFile(from, to) {

    return new Promise(resolve => {

        let pathTo = path.parse(to).dir;

        _mkdir(pathTo).then(() => {
            _copyStream(from, to).then(() => {
                resolve();
            });
        })
    })
}

function _writeFile(to, data, options) {

    return new Promise(resolve => {

        let pathTo = path.parse(to).dir;

        // console.log( '_writeFile : '+to );

        _mkdir(pathTo).then(() => {
            fs.writeFile(to, data, options, err => resolve(err))
            // fs.writeFile(to, data, options, err => resolve(err))
        })
    })
}

module.exports = {

    getJson: _getJson,
    pathContains: _pathContains,
    mkdir: _mkdir,
    writeFile: _writeFile,
    copyStream: _copyStream,
    writeFileSync: _writeFileSync,
    copyFile: _copyFile
};



