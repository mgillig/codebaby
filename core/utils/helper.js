module.exports = {
    formatTime: _formatTime


};

function _formatTime( t ) {
    let timeUnit = ' ms';
    if ( t > 1000 && t < 60000 ) {
        timeUnit = ' s';
        t = t / 1000;
        t = Math.floor(t)
    }
    if ( t > 60000 ) {
        timeUnit = ' m';
        t = t / 60000;
    }

    return t + timeUnit;

}