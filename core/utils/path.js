let pathWs = process.cwd();
let pathWslength = pathWs.length;


module.exports = {

    getPathRelWs: _getPathRelWs,
    getProjectPaths: _getProjectPaths
};

function _getPathRelWs( path ) {
    return path.substr( pathWslength+1, path.length )

}

function _getProjectPaths( dirname ) {

    let pathArr = dirname.split( '/' );
    let proj = pathArr[ pathArr.indexOf( 'projects' ) + 1 ];

    return {
        pathWs: pathWs,
        pathProj: pathWs+'/projects/'+proj+'/',
        pathSrc: pathWs+'/projects/'+proj+'/src/',
        pathDist: pathWs+'/dist/'+proj+'/',
        pathCodebaby: pathWs + '/codebaby/',
        pathWatch: 'projects/'+proj
    }

}