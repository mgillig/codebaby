const path = require('path');
const chalk = require('chalk');
const codebaby = require('./global');
const argsParser = require('./utils/args');
const configuration = require('./process/configuration');
const dependencies = require('./process/dependencies');
const cbInterface = require('./components/readline');
const build = require('./process/build');
const modules = require('./process/modules');
const globalHandler = require('../handler/global-handler');
const logger = require('./utils/logger').debug(codebaby.state.debug);
const _log = logger.log('index', false);


let args;
let exit = false;
let remote = false;
let production = false;
let params = process.argv.splice(2, process.argv.length).join(' ');
args = argsParser.parse(params);
_log(args);


if (args) {
    if (args.hasOwnProperty('build')) {

        if (args.hasOwnProperty('exit')) {
            exit = true;
        }
        if (args.hasOwnProperty('production')) {
            production = true;
        }
        if (args.hasOwnProperty('remote')) {
            remote = true;
        }

        buildProject();

    } else {


        if (args.hasOwnProperty('install')) {

            // console.log( 88888 );
            // console.log( __dirname );
            let install;
            if(__dirname.indexOf('node_modules')!==-1){

                install = require('./process/install');
            }else{
                install = require('../../node_modules/codebaby/core/process/install');
            }

            let force = args['install'] === 'force';
            // let install = require('../../node_modules/codebaby/core/process/install');
            install(force);

        } else {

            if (args.hasOwnProperty('test')) {
                // console.log( args );
                let build = require('../../projects/' + args.test + '/config/build');
                codebaby.initArgs({'action': 'build', 'project': args.test, 'production': false, 'remote': false});
                require('./process/test')(args.test, build.test);

            } else {
                let project = require('./process/project');
                project.initUi(codebaby.state.npmTaskMode);
                if (args.hasOwnProperty('setup')) {

                    let proj = args.setup || 'all';
                    project.setupProj(proj)
                }
                if (args.hasOwnProperty('create')) {
                    let tpl = args.tpl || 'default';
                    project.createProj(args.create, tpl);
                }
                if (args.hasOwnProperty('delete')) {
                    if (args.delete) {
                        project.deleteProj(args.delete)
                    } else {
                        console.log(chalk.red('!!! ') + chalk.yellow('provide a project name') + ' : delete=foo !');
                    }
                }
            }
        }
    }
} else {
    //QUICK HELP
    require('./components/info').info();
}


function buildProject() {


    let preprocessEnv = {};

    Object.keys(args).forEach(key => {
            if ((key.indexOf('NODE_ENV_') > -1)) {
                preprocessEnv[key] = ''+args[key];
            }
        }
    );

    globalHandler.initPreprocess(preprocessEnv);
    codebaby.preprocessEnv = preprocessEnv;
    codebaby.initArgs({'action': 'build', 'project': args.build, 'production': production, 'remote': remote});
    cbInterface.init();

    function runBuild(result) {

        cbInterface.unregister('install');

        modules(codebaby.paths, codebaby.build.project, (watchedModules) => {

            build(result.handler, watchedModules, () => {

                if (exit) {
                    console.log('EXIT');
                    process.exit();
                }
            });
        })
    }

    codebaby.nodeVersion = process.version.replace(/v/gi, '');

    configuration(codebaby.paths, codebaby.build.dependencies, codebaby.build.test, codebaby.state.production,codebaby.build, result => {

        if (result.install.length > 0) {
            dependencies(result.install, codebaby.state.npmSaveExact, () => {
                runBuild(result)
            })
        } else {
            runBuild(result);
        }
    });

}


