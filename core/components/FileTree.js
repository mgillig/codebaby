const fs = require("fs")
const path = require("path")
const chalk = require("chalk")

class FileTree {
	constructor(debug) {
		this.debug = debug || false
	}

	init(config) {
		this.resolve = undefined
		this.reject = undefined
		this.pathRead = this.getBasePathRead(config.path)
		this.treeN = 0
		this.treeComplete = 0
		this.invisible = config.invisible || false
		this.stat = config.stat || false
		this.filterFolder = []
		this.filterFiles = []
		this.transform = config.transform
		this.files = []
		this.filter = config.filter || []
		this.fileTypes = {}
		this.folder = []
		this.ignoredFiles = []
		this.ignoredFolder = []
		this.paths = { rel: [], abs: [] }
		this.collection = { rel: {}, abs: {} }
		this.ignoredFileTypes = []
		this.ignoreFolder = []
		this.ignoreSet = false
		this.collect = true

		if (config.cwd === undefined) {
			this.cwd = process.cwd()
		} else {
			this.cwd = config.cwd
		}

		this.initFilter()
		this.setFilter()
	}

	read(config) {
		if (config) {
			this.init(config)
		}
		this.activateFilter()

		return new Promise((resolve, reject) => {
			this.resolve = resolve
			this.reject = reject

			fs.exists(this.pathRead, exists => {
				if (exists) {
					fs.stat(this.pathRead, (err, stat) => {
						if (stat.isFile()) {
							console.log(chalk.red("path is not a dirctory"))
							this.treeComplete = -1
							this.complete()
						} else {
							this.checkPath(this.pathRead, 0).then(result => {
								if (result.fileData.isFile) {
									this.handleFile(result, 0)
								} else {
									this.handleFolder(result, 0)
								}
							})
						}
					})
				} else {
					//TODO UGLY
					this.treeComplete = -1
					this.error = "path :" + this.pathRead + " does not exist"
					this.complete()
				}
			})
		})
	}

	complete() {
		this.treeComplete++
		this.ignoreSet = false

		if (this.debug) {
			console.log(
				chalk.green("complete ") + this.treeComplete + path.sep + this.treeN
			)
		}

		if (this.treeComplete === this.treeN) {
			if (this.debug) {
				console.log(chalk.green("Filetree complete ") + " : " + this.pathRead)
			}

			let result = {
				files: this.files,
				folder: this.folder,
				fileTypes: this.fileTypes,
				paths: this.paths,
				collection: this.collection
			}
			if (this.error) {
				result.error = this.error
			}

			if (this.transform) {
				result.transform = this.files.map(entry => {
					// console.log( entry );
					return {
						path: entry.pathRel,
						from: entry.pathAbs,
						to: path.normalize(this.transform + path.sep + entry.pathRel),
						fileType: entry.fileType,
						name: entry.name
					}
				})

				this.files.forEach(entry => {
					entry.from = entry.pathAbs
					entry.to = path.normalize(this.transform + path.sep + entry.pathRel)
				})
			}
			// console.log(   result );

			this.resolve(result)
		}
	}

	checkPath(pathCheck, id) {
		return new Promise(resolve => {
			fs.stat(pathCheck, (err, stat) => {
				let result = { err: err, isFile: false, path: pathCheck }
				if (err) {
					console.log(err)
				} else {
					let patData = path.parse(pathCheck)
					let pathAbs = path.normalize(this.cwd + path.sep + pathCheck)
					let pathRel = path.relative(this.pathRead, pathCheck)
					let fileData = {
						id: id,
						path: pathCheck,
						pathRel: pathRel,
						pathAbs: pathAbs,
						name: patData.base,
						isFile: stat.isFile()
					}

					if (fileData.isFile) {
						fileData.fileType = patData.ext
						result.path = pathCheck
					} else {
						result.path = pathCheck + path.sep
					}

					if (this.stat) {
						fileData.stat = stat
					}
					result.fileData = fileData
				}
				resolve(result)
			})
		})
	}

	runFilter(fileData, list) {
		let pass = true

		list.some(test => {
			// console.log( test );
			if (!test(fileData)) {
				// console.log(chalk.red('fail ') + ' ' + fileData.path);
				pass = false
				return true
			} else {
				// console.log(chalk.green('pass ') + ' ' + fileData.path);
			}
		})

		if (this.collect && pass) {
			this.collectFileData(fileData)
		}
		// console.log(chalk.blue('###### pass : ') +pass+' - > '+fileData.path);
		return pass
	}

	handleFile(result) {
		if (this.runFilter(result.fileData, this.filterFiles)) {
			this.files.push(result.fileData)
		}
	}

	handleFolder(result, id) {
		// if (this.debug) {
		//     console.log(chalk.green('handleFolder  ') + result.path + ' id : ' + chalk.magenta(id));
		// }
		// console.log( chalk.green( 'handleFolder  ' ) + result.path + ' id : ' + chalk.magenta( id ) );
		result.fileData.id = id

		if (this.runFilter(result.fileData, this.filterFolder)) {
			this.folder.push(result.fileData)
			this.treeN++
			// if (this.debug) {
			//     console.log(chalk.blue('register tree ') + result.path + ' ' + chalk.yellow(this.treeN));
			// }

			fs.readdir(result.path, (err, resultDir) => {
				if (err) {
					console.log(err)
				} else {
					// if ( this.debug ) {
					//     console.log( chalk.yellow( 'folder content' ) + ' : ' + result.path );
					// }

					let indexSubFolder = 0
					let promises = resultDir.map(entry => {
						// if ( this.debug ) {
						//     console.log( chalk.yellow( '  -> ' ) + entry );
						// }
						return new Promise(resolve => {
							this.checkPath(path.normalize(result.path + entry), id).then(
								result => {
									if (result.fileData.isFile) {
										this.handleFile(result, 0)
									} else {
										indexSubFolder++
										this.handleFolder(result, id + ":" + indexSubFolder)
									}
									resolve()
								}
							)
						})
					})
					Promise.all(promises).then(() => this.complete(result.path))
				}
			})
		}
	}

	initFilter() {
		if (this.ignoreSet) {
			return
		}
		this.ignoreSet = true
		this.testFile = []
		this.testFolder = []

		if (this.fileType) {
			// this.filterFileTypes.push(this.setPathRegex(this.fileType, false));
			// this.testFile.push( this.setPathRegexNew(this.fileType,path.parse(this.pathRead), true,false) );
		}

		if (this.fileType === undefined) {
			// this.testFile.push({regex: new RegExp('(.*)'), type: 'wildCard', ignore: false});
		} else {
			this.filter.push(this.fileType)
			// this.testFile.push(this.setPathRegexNew(this.fileType, false));
		}

		// console.log(  this.setPathRegexNew(this.fileType,path.parse(this.pathRead), true,false) );
		// console.log(chalk.blue('##### this.fileType :  ') + this.fileType);
		// console.log(chalk.blue('##### this.pathRead :  ') + this.pathRead);
		// console.log(this.filter);

		let filterEntrys = []
		let ignoreEntrys = []

		this.filter.forEach(entry => {
			let pathData = path.parse(entry)
			let isFile = pathData.ext !== ""
			let ignore = entry[0] === "!"
			let regexEntry

			if (isFile) {
				// this.testFile.push(this.setPathRegex(entry.replace(/!/, ''), ignore));
				// filterEntry = this.setPathRegexNew(entry.replace(/!/, ''), pathData, isFile, ignore);
				regexEntry = this.setRegexPattern(
					entry.replace(/!/, ""),
					pathData,
					isFile,
					ignore
				)
				if (ignore) {
					ignoreEntrys.push(regexEntry)
				} else {
					filterEntrys.push(regexEntry)
				}
			} else {
				regexEntry = this.setPathRegex(entry.replace(/!/, ""), ignore, false)
				this.testFolder.push(regexEntry)
			}
		})

		// console.log(this.testFile);
		// console.log('-----');

		let filterTest = filterEntrys.map(entry => entry.pattern).join("|")
		let filterIgnore = ignoreEntrys.map(entry => entry.pattern).join("|")

		// console.log('filterTest -> ' + filterTest);
		// console.log('filterIgnore -> ' + filterIgnore);

		if (filterTest !== "") {
			this.testFile.push({
				regex: new RegExp(filterTest),
				ignore: false,
				pattern: filterTest
			})
		}
		if (filterIgnore !== "") {
			this.testFile.push({
				regex: new RegExp(filterIgnore),
				ignore: true,
				pattern: filterIgnore
			})
		}

		let ignoreTest = this.testFile.reduce((acc, entry) => {
			if (entry.ignore) {
				return acc + "|" + entry.regex
			}
		}, "")

		// this.testFile = [];

		// if (filterTest) {
		//     this.testFile.push({regex: new RegExp(filterTest), ignore: false});
		// }
		// if (ignoreTest) {
		//     this.testFile.push({regex: new RegExp(ignoreTest), ignore: true});
		// }

		if (this.debug) {
			console.log(this.testFile)
			// console.log(this.testFolder);
			console.log(chalk.yellow("*************"))
		}
		// console.log('');
		// console.log(this.testFile);
		// // console.log(this.testFolder);
		// console.log(chalk.yellow('*************'));
	}

	setRegexPattern(strPattern, pathData, isFile, ignore = true) {
		// console.log( pathData );
		// console.log( isFile );
		strPattern = strPattern.replace(/\*./, ".*\\.")
		// strPattern = strPattern.replace(/\./, '\.');
		strPattern = strPattern.toLowerCase()
		if (!isFile) {
			// let folderHallo = '\/hallo\/|\/hallo$|hallo\/$';
			let firstChar = strPattern[0]
			let lastChar = strPattern[strPattern.length - 1]
			let patternFolder = strPattern
			let patternFolderStart = strPattern
			let patternFolderEnd = strPattern
			if (firstChar !== path.sep) {
				patternFolder = path.normalize(path.sep + strPattern)
				patternFolderStart = path.normalize(path.sep + strPattern)
			}
			if (lastChar !== path.sep) {
				patternFolder += path.sep + "$"
				patternFolderStart = path.normalize(path.sep + strPattern)
				patternFolderEnd += path.sep
			}

			// console.log(chalk.green(patternFolder));
			// console.log(chalk.green(patternFolderStart));
			// console.log(chalk.green(patternFolderEnd));
			strPattern =
				patternFolder + "|" + patternFolderStart + "|" + patternFolderEnd
			// + '|' + path.normalize(path.sep + strPattern);
		} else {
			if (pathData.dir === path.sep) {
				strPattern = path.normalize(path.sep + strPattern) + "$"
			} else {
				strPattern = strPattern + "$"
			}
		}
		return { pattern: strPattern, ignore: ignore, isFile: isFile }
	}

	setPathRegexNew(strPattern, pathData, isFile, ignore = true) {
		console.log(pathData)
		console.log(isFile)
		strPattern = strPattern.replace(/\*./, ".*\\.")
		// strPattern = strPattern.replace(/\./, '\.');
		strPattern = strPattern.toLowerCase()
		if (!isFile) {
			// let folderHallo = '\/hallo\/|\/hallo$|hallo\/$';
			let firstChar = strPattern[0]
			let lastChar = strPattern[strPattern.length - 1]
			let patternFolder = strPattern
			let patternFolderStart = strPattern
			let patternFolderEnd = strPattern
			if (firstChar !== path.sep) {
				patternFolder = path.normalize(path.sep + strPattern)
				patternFolderStart = path.normalize(path.sep + strPattern)
			}
			if (lastChar !== path.sep) {
				patternFolder += path.sep + "$"
				patternFolderStart = path.normalize(path.sep + strPattern)
				patternFolderEnd += path.sep
			}

			// console.log(chalk.green(patternFolder));
			// console.log(chalk.green(patternFolderStart));
			// console.log(chalk.green(patternFolderEnd));
			strPattern =
				patternFolder + "|" + patternFolderStart + "|" + patternFolderEnd
			// + '|' + path.normalize(path.sep + strPattern);
		} else {
			if (pathData.dir === path.sep) {
				strPattern = path.normalize(path.sep + strPattern) + "$"
			} else {
				strPattern = strPattern + "$"
			}
		}
		// type = '/' + type;
		let pattern = "" + strPattern
		return { regex: new RegExp(pattern), type: strPattern, ignore: ignore }
	}

	checkFilterFileType(fileData) {
		// console.log(chalk.yellow('checkFilterFileType : ') + fileData.path);
		let pass = true
		this.testFile.some(entry => {
			// console.log( entry );

			let match = this.testRegex(entry, fileData.path)
			// console.log(entry);
			let matchresult
			if (match) {
				matchresult = chalk.green("true ")
			} else {
				matchresult = chalk.red(false)
			}
			let type = entry.ignore ? "ignore" : "filter"

			let mess =
				matchresult +
				"  " +
				fileData.name +
				" " +
				chalk.yellow(entry.regex) +
				"  : " +
				type
			if (match) {
				if (entry.ignore) {
					if (this.debug) {
						mess += " " + chalk.red("fail") + " : " + fileData.name
						console.log(mess)
					}
					pass = false
					return true
				} else {
					if (this.debug) {
						mess += " " + chalk.green("pass")
						console.log(mess)
					}
					pass = true
					// return true;
				}
			} else {
				// FILTER
				if (!entry.ignore) {
					if (this.debug) {
						mess += " " + chalk.red("fail") + " : " + fileData.name
						console.log(mess)
					}
					pass = false
					return true
				} else {
					if (this.debug) {
						mess += " " + chalk.green("pass")
						console.log(mess)
					}
					pass = true
					return true
				}
			}
		})
		// console.log('#############');
		// console.log(chalk.yellow('---------- > checkFilterFileType : ') + fileData.path+ ' pass :  '+pass);
		return pass
	}

	checkFilterFolder(fileData) {
		// console.log(chalk.yellow('checkFilterFolder : ') + fileData.path);

		let pass = false
		this.testFolder.some(entry => {
			// console.log(entry);
			let match = this.testRegex(entry, fileData.path)
			if (match) {
				if (entry.ignore) {
					// console.log('match ignore');
					// console.log(chalk.red('fail') + ' : ' + fileData.name);
					pass = false
					return true
				}
			} else {
				// console.log('match not');
				if (entry.ignore) {
					// console.log(chalk.green('pass') + ' : ' + fileData.name);
					pass = true
				}
			}

			// if (this.testRegex(entry, fileData.path)) {
			//
			//     console.log('match  ' + entry.type + ' ignore : ' + entry.ignore);
			//
			//     // if ( this.debug ) {
			//     //     console.log( 'checkFilterFileType ' + chalk.green( 'pass' ) + ' : ' + fileData.name );
			//     // }
			//     if (entry.ignore) {
			//         console.log('checkFilterFileType ' + chalk.red('fail') + ' : ' + fileData.name);
			//         pass = false;
			//         return true;
			//     } else {
			//         console.log('checkFilterFileType ' + chalk.green('pass') + ' : ' + fileData.name);
			//         pass = true;
			//     }
			// }
			// } else {

			// }
		})
		// console.log(chalk.yellow('---------- > checkFilterFileType : ') + fileData.path+ ' pass :  '+pass);
		return pass
	}

	filterIgnoredFolder(fileData) {
		// console.log(chalk.yellow('filterIgnoredFolder : ') + fileData.path);

		let pass = true

		this.ignoredFolder.some(entry => {
			if (entry === fileData.name) {
				// if ( this.debug ) {
				//     console.log( chalk.red( 'ignore' ) + ' : ' + fileData.name );
				// }
				pass = false
				return true
			}
		})
		return pass
	}

	activateFilter() {
		if (!this.invisible) {
			this.filterFiles.push(this.filterInvisible.bind(this))
			this.filterFolder.push(this.filterInvisible.bind(this))
		}

		if (this.testFile.length > 0) {
			this.filterFiles.push(this.checkFilterFileType.bind(this))
		}

		if (this.testFolder.length > 0) {
			this.filterFolder.push(this.checkFilterFolder.bind(this))
		}
	}

	setFilter() {}

	// ------------------------------------------------------ HELPER
	filterInvisible(fileData) {
		return fileData.name[0] !== "."
	}

	setFileTypeRegex(type, ignore = true) {
		type = type.replace(/\*/, "")
		type = type.toLowerCase()
		return {
			regex: new RegExp("\\" + type + "$", "i"),
			type: type,
			ignore: ignore
		}
	}

	setPathRegex(type, ignore = true, file = true) {
		type = type.replace(/\*./, ".*\\.")
		type = type.toLowerCase()
		if (!file) {
			type = "/" + type
		}
		let pattern = "" + type + "$"
		return { regex: new RegExp(pattern, "i"), type: type, ignore: ignore }
	}

	testRegex(entry, filePath) {
		return entry.regex.test(filePath)
	}

	getBasePathRead(filePath) {
		let patData = path.parse(filePath)
		// console.log( 'getBasePathRead' );
		// console.log( patData );
		// console.log( '########' );
		if (patData.ext === "") {
			return filePath
		} else {
			this.fileType = patData.ext
			// console.log( filePath );
			// this.fileType = patData.ext.replace(/\*./, '.*\\.');
			this.fileType = "*" + patData.ext
			return filePath.substr(0, filePath.lastIndexOf(path.sep) + 1)
		}
	}

	collectFileData(fileData) {
		if (fileData.isFile) {
			let type = fileData.fileType.substr(1)

			if (type === "") {
				type = "undefined"
			}
			if (this.fileTypes[type] === undefined) {
				this.fileTypes[type] = []
			}
			this.fileTypes[type].push(fileData)
		}

		let regexCwd = new RegExp(path.resolve())
		let pathRel = fileData.path.replace(regexCwd, "")
		this.paths.rel.push(pathRel)
		this.paths.abs.push(fileData.pathAbs)
		// console.log( fileData.path );
		// console.log( 'pathRel : '+pathRel );
		this.collection.rel[pathRel] = fileData
		this.collection.abs[fileData.pathAbs] = fileData
	}
}

module.exports = FileTree
