const path = require("path")
const _log = require("../utils/logger").log("fileData", false)
const _info = require("../utils/logger").info()

class FileData {
	constructor(data) {
		if (data) {
			this.initData(data)
		}
	}

	initData(data) {
		this.setDefaults(data)
	}

	setDefaults(data) {
		this.active = data.active || true
		this.debug = data.debug || false
		if (data.path === undefined) {
			data.path = ""
		}

		this.path = path.normalize(data.path)
		this.pathData = path.parse(data.path)
		this.from = data.from || ""
		this.to = data.to || ""
		this.name = data.name || ""
		this.fileType = data.fileType || ""
		this.fileTypeDest = data.fileTypeDest || ""
		this.fileTypes = data.fileTypes || []
		this.fileTypesRegex = this.fileTypes.map(type => new RegExp(type + "$"))

		this.replace = data.replace || []
		this.compileDir = data.compileDir || "_compiled"
		this.ignore = data.ignore || []
		this.concat = data.concat || false
		this.vendor = data.vendor || false
		this.minify = data.minify || false
		this.dest = data.dest || ""
		this.pathDataWrite = path.parse(this.to)

		if (data.rollup !== undefined) {
			this.rollup = data.rollup
		}
		if (data.es5 !== undefined) {
			this.es5 = data.es5
		}

		this.handler = data.handler
		this.ignoreTree = data.ignoreTree
		this.observe = data.observe
		this.timestamp = false
		this.watch = true

		if (data.timestamp !== undefined) {
			this.timestamp = data.timestamp
		}
		if (data.babel !== undefined) {
			this.babel = data.babel
		}
		if (data.ejs !== undefined) {
			this.ejs = data.ejs
		}
		if (data.typescript !== undefined) {
			this.typescript = data.typescript
		}
		if (this.rollup !== undefined) {
			this.bundle = true
		}
		if (data.watch !== undefined) {
			this.watch = data.watch
		}

		this.flatPaths = false

		if (data.flatPaths !== undefined) {
			this.flatPaths = data.flatPaths
		}

		this.state = {
			isFile: this.pathData.ext !== "",
			pathChecked: false,
			compiled: false,
			init: false,
			tree: false,
			bundle: false,
			cloned: false
		}
		if (this.name === "") {
			this.rename = false
			this.name = this.pathData.name
		} else {
			this.rename = true
		}
		if (this.state.isFile && this.fileTypeDest === "") {
			this.fileTypeDest = this.pathData.ext
		}
		if (!this.state.isFile) {
			this.state.tree = true
		}
		this.stats = {
			timeStart: 0,
			timeEnd: 0,
			duration: 0
		}

		this.setPaths(data)
	}

	setPaths(data) {
		this.paths = {
			read: path.normalize(path.sep + this.from + path.sep + this.path),
			readRelative: path.normalize(path.sep + this.path),
			base:
				path.dirname(path.normalize(this.from + path.sep + this.path)) +
				path.sep,
			write: "",
			watch: "",
			proj: data.pathProj,
			watchRelative: ""
		}

		if (this.state.isFile) {
			this.paths.baseRelative =
				path.normalize(
					this.paths.proj +
						path.sep +
						"src" +
						path.sep +
						this.path.substr(0, this.path.lastIndexOf(path.sep))
				) + path.sep
		} else {
			this.paths.baseRelative =
				path.normalize(
					this.paths.proj + path.sep + "src" + path.sep + this.path
				) + path.sep
		}

		this.paths.compiled = path.normalize(
			this.paths.baseRelative + path.sep + this.compileDir
		)
		this.paths.compiledAbs = path.normalize(
			path.sep +
				this.from +
				path.sep +
				this.paths.baseRelative +
				path.sep +
				this.compileDir
		)

		// console.log('codebaby.paths.pathWatch : ' + codebaby.paths.pathWatch);

		let seperatorWrite = path.sep
		if (this.name === "") {
			seperatorWrite = ""
		}

		let pathWrite = path.sep + this.dest + path.sep + this.to //+ seperatorWrite + this.name;

		if (this.rename || this.state.isFile || this.concat || this.bundle) {
			pathWrite += seperatorWrite + this.name
		}

		if (this.state.isFile || this.concat || this.bundle) {
			pathWrite += this.fileTypeDest
		}

		// if(this.path==='js/test/header.js'){
		//     console.log( '#####' );
		//     console.log( this.fileTypeDest);
		//     console.log( pathWrite );
		// }
		this.pathDataWrite = path.parse(path.normalize(pathWrite))

		// if ( this.debug ) {
		//     console.log( this.pathData );
		//     console.log( this.name );
		//     console.log( this.pathDataWrite );
		// }

		let watchType = ""
		let pathEnd = ""
		if (this.name !== "") {
			this.pathDataWrite.name = this.name
		}
		if (this.name === "") {
			this.name = this.pathData.name
		}
		if (this.concat || this.state.isFile || this.state.bundle) {
			pathEnd = ""
		}
		if (this.fileTypeDest !== "") {
			this.pathDataWrite.ext = this.fileTypeDest
		}

		if (!this.state.isFile) {
			watchType = "/**/*"
		}
		if (this.timestamp) {
			this.pathDataWrite.name =
				this.pathDataWrite.name + "." + FileData.preprocessEnv.TIMESTAMP
			this.pathDataWrite.base = this.pathDataWrite.name + this.pathDataWrite.ext
		}

		this.paths.write = path.normalize(path.format(this.pathDataWrite))
		this.paths.baseWrite = path.relative(process.cwd(), this.paths.write)
		this.paths.writeRelative = path.relative(
			this.dest,
			path.normalize(path.format(this.pathDataWrite) + pathEnd)
		)
		// this.paths.write = path.normalize( path.sep + this.dest + path.sep + this.paths.writeRelative );
		this.paths.watchRelative = path.normalize(this.path + watchType)
		// console.log('### ' + this.observe);
		if (this.observe) {
			this.paths.watch = path.normalize(
				this.paths.baseRelative + path.sep + this.observe
			)
		} else {
			this.paths.watch = path.normalize(
				this.from + path.sep + this.path + watchType
			)
		}

		if (this.state.isFile) {
			this.state.tree = false
			this.fileTree = {
				files: [
					{
						from: this.paths.read,
						to: this.paths.write
					}
				]
			}
		}
	}
}

function setDefaults(data) {
	_log("setDefaults")

	data.active = data.active || true
	data.debug = data.debug || false
	data.path = path.normalize(data.path || "")
	data.from = data.from || ""
	data.to = data.to || ""
	data.name = data.name || ""
	data.fileType = data.fileType || ""
	data.fileTypeDest = data.fileTypeDest || ""
	data.fileTypes = data.fileTypes || []
	data.replace = data.replace || []
	data.ignore = data.ignore || []
	data.concat = data.concat || false
	data.vendor = data.vendor || false
	data.dest = data.dest || ""
	data.fileTree = undefined
	data.pathData = path.parse(data.path)
	data.pathDataWrite = path.parse(data.path)
}

function setStates(data) {
	_log("setStates")

	data.state = {
		isFile: path.extname(data.path) !== "",
		pathChecked: false,
		compiled: false,
		init: false,
		tree: false,
		bundle: false,
		cloned: false
		// name: path.basename( data.path )
	}
	data.stats = {
		timeStart: 0,
		timeEnd: 0,
		duration: 0
	}
}

function setPaths(data) {
	_log("setPaths")

	data.paths = {
		read: path.normalize(path.sep + data.from + path.sep + data.path),
		readRelative: path.normalize(path.sep + data.path),
		base:
			path.dirname(path.normalize(data.from + path.sep + data.path)) + path.sep,
		baseRelative: path.dirname(path.normalize(path.sep + data.path)) + path.sep,
		write: "",
		watch: "",
		watchRelative: ""
	}

	data.evt = {
		path: "",
		type: "",
		pathData: undefined,
		data: undefined
	}

	data.pathDataWrite.base = undefined
	data.pathDataWrite.dir = data.to

	let watchType = ""
	let pathEnd = path.sep
	if (data.name !== "" && data.rename) {
		data.pathDataWrite.name = data.name
	}

	if (data.name === "") {
		// console.log( data.pathData );
		data.name = data.pathData.name
		// console.log( data.name );
	}
	if (data.concat || data.state.isFile || data.state.bundle) {
		pathEnd = ""
	}
	if (data.fileTypeDest !== "") {
		data.pathDataWrite.ext = data.fileTypeDest
	}

	if (!data.state.isFile) {
		watchType = "/**/*"

		// if ( !data.concat && !data.state.bundle ) {
		//     data.pathDataWrite.name = '';
		// }
		if (!data.concat) {
			data.pathDataWrite.name = ""
		}
	}

	data.paths.writeRelative = path.normalize(
		path.format(data.pathDataWrite) + pathEnd
	)
	data.paths.write = path.normalize(
		path.sep + data.dest + path.sep + data.paths.writeRelative
	)
	data.paths.watchRelative = path.normalize(data.path + watchType)
	data.paths.watch = path.normalize(
		data.from + path.sep + data.path + watchType
	)

	if (data.state.isFile) {
		data.state.tree = false
		data.fileTree = {
			files: [
				{
					from: data.paths.read,
					to: data.paths.write
				}
			]
		}
	}
}

module.exports = FileData
