"use strict"
const path = require("path")
const chalk = require("chalk")
const codebaby = require("../../core/global")
const browserSync = require("browser-sync")
const _log = require("../utils/logger").log("browser", false)
const _info = require("../utils/logger").info()
//https://thomastuts.com/blog/browsersync-spa-routing-pretty-urls.html
// const connectHistory = require('connect-history-api-fallback');

let data = {
	ip: undefined,
	port: undefined,
	external: undefined,
	local: undefined
}

module.exports = {
	init: _init,
	notify: _notify,
	cleanNote: _cleanNote,
	stream: browserSync.stream,
	reload: function(file) {
		if (file) {
			browserSync.reload(file)
		} else {
			browserSync.reload()
		}
	}
}

process.on("browser.reload", file => {
	_log(" reload : " + file)

	if (file) {
		browserSync.reload(file)
	} else {
		browserSync.reload()
	}
})
process.on("browser.error", mess => {
	_notify(mess, true)
})
process.on("codebaby.error", mess => {
	_notify(mess, true)
})

function _cleanNote() {
	browserSync.notify("", 1000)
}

function _notify(mess, error) {
	if (error) {
		browserSync.notify(
			`<span style="color: #ff0000!important;"> ${mess} </span>`,
			6000
		)
	} else {
		browserSync.notify(mess, 6000)
	}
}

function _init(config, cb) {
	if (config === undefined) {
		config = {
			ui: false,
			ghostMode: false,
			codeSync: true,
			proxy: undefined,
			startPath: "index.html",
			browser: "google chrome"
		}
	} else {
		// if (config.proxy !== undefined) {
		//     codebaby.state.proxy = true;
		// }
	}
	if (config.middleware === undefined) {
		config.middleware = []
	}

	if (config.spaRouting !== undefined) {
		if (config.spaRouting) {
			let routePath = "/index.html"
			if (typeof config.spaRouting === "string") {
				routePath = config.spaRouting
			}

			// config.middleware.push(connectHistory());
			config.middleware.push(function(req, res, next) {
				if (codebaby.state.buildComplete) {
					let pathData = path.parse(req.url)
					if (
						pathData.ext === "" ||
						pathData.ext === ".html" ||
						pathData.ext === ".htm"
					) {
						console.log(chalk.magenta("url rewrite : ") + routePath)
						req.url = routePath
					}
				}
				return next()
			})
		}

		delete config.spaRouting
	}

	// console.log( config );

	// config.middleware = [
	//     {
	//         route: "/api",
	//         handle: function( req, res, next ) {
	//
	//             app.use( bodyParser.urlencoded( { extended: true } ) )
	//             // handle any requests at /api
	//             // console.log( res );
	//             // console.log( req );
	//
	//             res.end( JSON.stringify( { 'logged': true } ) );
	//             // res.send( JSON.stringify( { 'logged': true } ) )
	//         }
	//     }
	// ];

	if (codebaby.state.remote) {
		config.open = "external"
	}

	let pathPreloadIndex =
		codebaby.paths.pathWs + codebaby.build.project.buildFolder

	if (config.proxy === undefined) {
		config.server = {}
		codebaby.state.proxy = false
		//codebaby.build.browserSync.server = { baseDir: pathPreloadIndex };
		// codebaby.build.browserSync.server = [pathPreloadIndex, codebaby.paths.pathWs];
		config.server.baseDir = [pathPreloadIndex, codebaby.paths.pathWs]
	} else {
		codebaby.state.proxy = true
	}

	// let name = config.name || 'dev-server';
	// browserSync.create( name );

	// console.log(config);

	// process.exit();

	browserSync.init(config, function(err, bs) {
		if (err !== null) {
			console.log(chalk.red("error : browsersync"))
			console.log(`${err}`)
			process.exit()
		}
		// console.log( '#####' );
		// console.log( this.options );
		if (this.options) {
			data.port = this.options.get("port")
			data.ip = this.utils.devIp()[0]
		}

		// console.log( this.utils );
		// console.log( data );
		// process.exit();

		if (data.ip !== undefined && data.port !== undefined) {
			let http = "http"
			if (config.https) {
				http = "https"
			}

			data.local = `${http}://localhost:` + data.port + "/" || ""
			data.external = `${http}://` + data.ip + ":" + data.port + "/"
			data.proxy = config.proxy

			if (config.open === "external") {
				data.local = data.external
			}
		}

		if (cb) {
			cb(data)
		}

		setTimeout(() => {
			process.emit("codebaby.browser.ready", data)
		}, 2000)
	})
}
