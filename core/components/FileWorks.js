const fs = require("fs")
const path = require("path")
const chalk = require("chalk")
const FileTree = require("./FileTree")
const { promList, promListSync } = require("../utils/promise")

class FileWorks {
	constructor(maxFile = 100) {
		this.ft = new FileTree()
		this.cwd = process.cwd()
		this.maxFile = maxFile
	}

	init(config) {
		config.filter = config.filter || []

		this.dest = config.dest
		this.path = config.path
		this.emptyFolder = config.filter.length > 0

		if (!config.invisible) {
			this.emptyFolder = false
		}

		delete config.dest
		this.ft.init(config)
		this.ft.collect = false
	}

	copy(config) {
		this.copy = true

		if (config) {
			this.init(config)
		}

		// console.log( chalk.yellow( 'copy ' ) + this.dest + ' ... ' );

		return new Promise((resolve, reject) => {
			this.resolve = resolve
			this.reject = reject

			this.ft.read().then(result => {
				// console.log( result.files );
				// console.log(chalk.magenta('files') + ' : ' + result.files.length);
				// console.log(chalk.magenta('folder') + ' : ' + result.folder.length);
				// console.log(this.dest);
				this.createFolder(this.dest).then(() => {
					this.createFolderStacks(result.folder)
					this.runFolderStackCreate().then(() =>
						this.runCopyFiles(result.files).then(() => this.resolve())
					)
					// this.runFolderStackCreate().then( () => console.log( 8888 ) )
				})
			})
		})
	}

	remove(config) {
		this.copy = false

		if (config) {
			this.init(config)
		}
		this.dest = config.path
		// console.log( chalk.red( 'delete ' ) + this.dest + ' ... ' );

		return new Promise((resolve, reject) => {
			this.resolve = resolve
			this.reject = reject

			this.ft.read().then(result => {
				// console.log(chalk.magenta('files') + ' : ' + result.files.length);
				// console.log(chalk.magenta('folder') + ' : ' + result.folder.length);

				this.deleteFiles(result.files).then(() => {
					this.createFolderStacks(result.folder, true)
					// console.log(  this.folderStack );
					// console.log( '###' );
					// this.folderStack.forEach( entry => {
					//     console.log( entry[ 0 ].id.length + ' ' + entry.length );
					// } );
					this.runFolderStackRemove().then(() => this.resolve())
				})
			})
		})
	}

	deleteFiles(list) {
		return new Promise(resolveDelete => {
			let funcList = list.map(entry => {
				return resolve => {
					return () => {
						fs.unlink(entry.path, err => {
							if (err) {
								console.log(err)
							}
							resolve()
						})
					}
				}
			})

			promList(funcList).then(() => resolveDelete())
		})
	}

	createFolderStacks(folder, reverse) {
		let collection = {}
		this.folderStack = []

		folder.forEach(entry => {
			let id = "" + entry.id
			let depth = id.length
			if (collection[depth] === undefined) {
				collection[depth] = []
			}
			collection[depth].push(entry)
		})

		Object.keys(collection)
			.map(id => parseInt(id))
			.sort((a, b) => {
				if (reverse) {
					return b - a
				} else {
					return a - b
				}
			})
			.map(id => {
				let depth = "" + id
				this.folderStack.push(collection[depth])
			})
	}

	runCopyFiles(files) {
		return new Promise(resolve => {
			let funcList = files.map(file => {
				return resolve => {
					return () => {
						let pathTo = path.normalize(
							this.cwd + path.sep + this.dest + path.sep + file.pathRel
						)
						this.copyFileTo(file.pathAbs, pathTo).then(() => resolve())
					}
				}
			})

			promList(funcList, this.maxFile).then(() => {
				resolve()
			})
		})
	}

	runFolderStackRemove() {
		// console.log('runFolderStackRemove');
		// console.log(this);
		// process.exit();

		return new Promise(resolve => {
			let funclist = this.folderStack.map(folder => {
				return resolve => {
					return () => this.runRemoveFolder(folder).then(() => resolve())
				}
			})

			promListSync(funclist).then(() => {
				resolve()
			})
		})
	}

	runFolderStackCreate() {
		return new Promise(resolve => {
			let funclist = this.folderStack.map(folder => {
				return resolve => {
					return () => this.runCreateFolder(folder).then(() => resolve())
				}
			})

			promListSync(funclist).then(() => {
				resolve()
			})
		})
	}

	runCreateFolder(list) {
		return new Promise(resolve => {
			let funcList = list.map(folder => {
				return resolve => {
					return () => {
						let pathTo = path.normalize(
							this.cwd + path.sep + this.dest + path.sep + folder.pathRel
						)
						this.createFolder(pathTo).then(() => resolve())
					}
				}
			})

			promList(funcList).then(() => {
				resolve()
			})
		})
	}

	runRemoveFolder(list) {
		return new Promise(resolve => {
			let funcList = list.map(folder => {
				return resolve => {
					return () => {
						// console.log( '#### REMOVE FOLDER' );
						// console.log( this.cwd  );
						// console.log( this.path  );
						let pathTo
						if (this.path.indexOf(this.cwd) === -1) {
							pathTo = path.normalize(
								this.cwd + path.sep + this.path + path.sep + folder.pathRel
							)
						} else {
							pathTo = path.normalize(this.path + path.sep + folder.pathRel)
						}

						// console.log( '### '+pathTo );
						// console.log( '### '+pathTo );
						let folderChilds = fs.readdirSync(pathTo)
						// console.log( folderChilds );
						if (folderChilds.length === 0) {
							fs.rmdir(pathTo, err => {
								if (err && this.emptyFolder) {
									console.log(chalk.red("rmdir : " + pathTo))
									console.log(err)
								}
								resolve()
							})
						} else {
							resolve()
						}
					}
				}
			})

			promList(funcList).then(() => {
				resolve()
			})
		})
	}

	createFolder(path) {
		return new Promise(resolve => {
			fs.exists(path, exist => {
				if (exist) {
					resolve()
				} else {
					fs.mkdir(path, err => resolve())
				}
			})
		})
	}

	copyFileTo(from, to) {
		return new Promise(resolve => {
			let streamWrite = fs.createWriteStream(to)
			streamWrite.on("close", function() {
				resolve()
			})
			streamWrite.on("error", err => {
				console.log(err)
			})
			fs.createReadStream(from).pipe(streamWrite)
		})
	}
}

module.exports = FileWorks
