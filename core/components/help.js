const browserSync = require("browser-sync")

module.exports = function() {}
// console.log( 'help' );
// console.log( __dirname );
// console.log( process.cwd() );

let config = {
	ui: false,
	ghostMode: false,
	codeSync: false,
	startPath: "index.html",
	server: "./codebaby/core/process/assets/help/",
	browser: "google chrome"
}

browserSync.create("codebaby-help")
browserSync.init(config)
