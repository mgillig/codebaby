const path = require("path")
const chalk = require("chalk")
const FileTree = require("../components/FileTree")
// let { cloneDeep } = require( '../utils/object' );
const _log = require("../utils/logger").log("fileDataAdapter", false)
const _info = require("../utils/logger").info()
let cache = {}

class HandlerInject {
	constructor(config, compile) {
		this.type = config.type
		this.fileType = config.fileType
		this.fileTypes = config.fileTypes.map(entry => "*" + entry)
		this.filter = this.fileTypes.concat(
			config.ignoreTree.map(entry => "!" + entry)
		)
		this.observe = config.observe
		this.babelTypes = config.babelTypes
		this.invisibleTreeFiles = config.invisibleTreeFiles
		this.notWatch = config.notWatch
		this.handlerCompileFunc = compile
		this.files = undefined
	}

	compile(fileData, evtData, cb) {
		let idCache = fileData.path + "_" + fileData.fileType
		let handlerData = {
			fileData: fileData,
			evt: evtData,
			cb: cb,
			error: false,
			compileList: undefined,
			fileTree: undefined
		}

		if (cache[idCache] !== undefined) {
			let handlerCache = cache[idCache]
			handlerData.compileList = handlerCache.compileList
			handlerData.fileTree = handlerCache.fileTree
		}

		if (fileData.debug) {
			console.log(fileData)
		}

		if (evtData === undefined) {
			evtData = { path: "", type: "", compileList: [] }
		}

		handlerData.evt = evtData

		let getTree = fileData.state.tree
		let tree = handlerData.fileTree

		if (fileData.state.tree) {
			if (tree) {
				getTree = false
			}
			if (evtData.type === "added" || evtData.type === "unlink") {
				getTree = true
			}
		}

		if (getTree) {
			this.getTree(handlerData)
		} else {
			if (fileData.state.isFile) {
				handlerData.compileList = [
					{
						path: fileData.path,
						pathAbs: fileData.paths.read,
						fromAbs: fileData.paths.read,
						toAbs: fileData.paths.write,
						to: fileData.paths.baseWrite,
						from: fileData.paths.baseWrite,
						name: fileData.pathData.name
					}
				]
			}
			this.callHandler(handlerData)
		}
	}

	setEvtCompileList(handlerData) {
		if (handlerData.evt.type !== "" && handlerData.evt.type !== "unlink") {
			let data = this.createCompileListEntry(
				undefined,
				handlerData.fileData,
				handlerData.evt
			)
			handlerData.evt.compileList = [data]
		}
	}

	createCompileListEntry(entry, fileData, evt) {
		let fromAbs
		let pathRel
		let name
		let fileType
		let cwd = path.resolve()
		let baseTo = fileData.paths.baseWrite
		let to
		let toAbs

		if (evt) {
			fromAbs = evt.path
			fileType = evt.pathData.ext
			pathRel = evt.pathData.base
			name = evt.pathData.name + fileType
		} else {
			fromAbs = entry.path
			fileType = entry.fileType
			pathRel = entry.pathRel
			name = entry.name
		}

		let from = path.relative(cwd, fromAbs)
		if (fileData.flatPaths) {
			pathRel = pathRel.substr(pathRel.lastIndexOf("/") + 1)
		}

		if (fileData.babel) {
			let pathBabel = path.normalize(
				fileData.paths.compiled +
					path.sep +
					from.substr(fileData.paths.baseRelative.length)
			)
			//TODO MORE TYPE CONVERSION
			to = pathBabel.replace(/\.jsx$/, ".js")
		} else {
			to = path.normalize(baseTo + path.sep + pathRel)
			toAbs = path.normalize(cwd + path.sep + baseTo + path.sep + pathRel)
		}

		return {
			path: pathRel,
			to: to,
			from: from,
			toAbs: toAbs,
			fromAbs: fromAbs,
			name: name,
			fileType: fileType
		}
	}

	getTree(handlerData) {
		let idCache =
			handlerData.fileData.path + "_" + handlerData.fileData.fileType
		let ft = new FileTree()
		let config = {
			path: handlerData.fileData.paths.read,
			filter: this.filter,
			invisible: this.invisibleTreeFiles,
			cwd: ""
		}
		ft.read(config).then(result => {
			let compileList = result.files.map(entry => {
				return this.createCompileListEntry(entry, handlerData.fileData)
			})

			handlerData.fileTree = result
			handlerData.compileList = compileList
			cache[idCache] = { fileTree: result, compileList: compileList }

			this.callHandler(handlerData)
		})
	}

	callHandler(handlerData) {
		this.setEvtCompileList(handlerData)
		this.handlerCompileFunc(handlerData, handlerData =>
			process.emit("codebaby.filehandler.ready", handlerData)
		)
	}
}

module.exports = HandlerInject
