const fs = require("fs")
const path = require("path")
const chalk = require("chalk")
// const spawn = require( 'child_process' ).spawn;
// const fork = require( 'child_process' ).fork;
const readline = require("readline")
const argsParser = require("../utils/args")
const _log = require("../utils/logger").log("readline", false)
const _info = require("../utils/logger").info()

let rl
let commands = {}
let commandsN = 0
let context = ""
let contextAsk = ""
let listen = false

module.exports = {
	init: _init,
	unregister: _unregister,
	register: _register,
	setContext: (cmd, ask) => {
		_log("setContext : " + cmd, 1)
		context = cmd
		contextAsk = ask
	},
	ask: _ask,
	reset: _reset
}

function _init() {
	_log("init", 0)

	if (listen) {
		return
	}
	listen = true

	if (rl === undefined) {
		rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		})
		rl.on("line", input => {
			let argsData
			let args

			if (input.indexOf("npm run") > -1) {
				return
			}
			if (input !== "") {
				// let inputArr = input.split(' ').filter(entry => entry !== '');
				let command
				let cmd

				// console.log('cmd : ' + cmd);

				if (context === "") {
					argsData = parseArgsString(input)
					cmd = argsData.cmd
					args = argsParser.parse(argsData.args.join(" "))
					// console.log('input : '+input);
					// console.log('argsData : ');
					// console.log(argsData);
					// console.log('args : ');
					// console.log(args);
					// let cmd = inputArr[0];
					command = commands[cmd]

					if (command === undefined) {
						_printCommands(cmd)
					} else {
						_exec(command, cmd, args)
					}
				} else {
					argsData = parseArgsString(context + " " + input)
					args = argsParser.parse(argsData.args.join(" "))

					command = commands[context]
					_exec(command, args)
				}
			}
		})
	}
}

function _ask(mess) {
	console.log("-> " + chalk.green(mess))
}

function _printCommands(cmd) {
	console.log(
		chalk.red("\n??? ") + chalk.yellow("unknown command") + " : " + cmd
	)

	if (commandsN > 0) {
		console.log("-> " + chalk.yellow("known commands are ") + " : ")
		for (let prop in commands) {
			if (commands.hasOwnProperty(prop)) {
				console.log("- " + prop)
			}
		}
	}
}

function _exec(command, cmdStr, args) {
	// console.log( command );
	// console.log( args );

	if (args === undefined) {
		args = {}
	}

	if (args.hasOwnProperty("help")) {
		console.log(command.help)
	} else {
		// console.log( command );

		if (command.cb) {
			command.cb(cmdStr, args)
		} else {
			console.log("codebaby.command." + command.cmd)
			process.emit("codebaby.command." + command.cmd, args)
		}
	}

	context = ""
}

function _reset() {
	// console.log( '------------------- reset ' );
	if (context !== "") {
		console.log("2. reset ")
		let comand = commands[context]
		if (comand.reset) {
			comand.reset()
		}
	}
	context = ""
}

function _register(cmd, cb, help = "! sorry : no help defined", reset) {
	if (commands[cmd] === undefined) {
		commands[cmd] = { cb: cb, help: help, reset: reset, cmd: cmd }
		commandsN++
	}
}

function _unregister(cmd) {
	if (commands[cmd]) {
		delete commands[cmd]
		commandsN--
	}
}

function parseArgsString(str) {
	let command = undefined
	let args = str
		.split(" ")
		.filter(entry => entry !== "")
		.filter(entry => {
			if (entry.indexOf("=") > -1) {
				return true
			} else {
				if (command === undefined) {
					command = entry
				} else {
					return true
				}
			}
		})
	return { cmd: command, args: args }
}
