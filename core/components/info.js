const chalk = require("chalk")
let packageJson

if (__dirname.indexOf("node_modules") > -1) {
	packageJson = require("../../package.json")
} else {
	packageJson = require("../../../node_modules/codebaby/package.json")
}

module.exports = {
	info: () => {
		show(infoData)
	}
}

let logCollors = {
	yellow: mess => chalk.yellow(mess),
	magenta: mess => chalk.magenta(mess),
	blue: mess => chalk.blue(mess),
	inverse: mess => chalk.inverse(mess),
	cyan: mess => chalk.cyan(mess),
	underline: mess => chalk.underline(mess),
	bgYellow: mess => chalk.bgYellow(mess),
	green: mess => chalk.green(mess),
	example: mess => chalk.green(mess)
}

let line = "----------------------------"
let arrow = { yellow: "->" }
let p = ""
let pp = "\n"
let chapter = "\n\n\n\n\n"

function show(data) {
	data.forEach(log => {
		let output = log.map(coloredOutput => {
			if (typeof coloredOutput === "object") {
				for (let prop in coloredOutput) {
					if (coloredOutput.hasOwnProperty(prop)) {
						let colorLog = logCollors[prop]
						if (colorLog) {
							return colorLog(coloredOutput[prop])
						} else {
							return coloredOutput[prop]
						}
					}
				}
			} else {
				return coloredOutput
			}
		})
		// console.log( output );
		console.log(output.join(""))
	})
}

let infoData = [
	[line],
	[p],
	[{ yellow: "codebaby" }, " version :  " + packageJson.version],
	[p],
	[line],
	[p],
	// [ { magenta: '< QUICK START >  ' } ],
	[p],
	["[ 1.0 ] ", { magenta: "< QUICK START >  " }],
	[p],
	[{ yellow: "- create a project " }, ': with name "foo"'],
	[p],
	[
		'All projects are defined in your codebaby.config.js \n\nTo create a project with name "foo" your codebaby.config.js\nshould look like this :'
	],
	[p],
	[{ example: '"projects" : {\n "foo":""\n}' }],
	// [ '[ 1.1 ] ' ],
	[p],
	[{ yellow: "- setup a project " }],
	[p],
	['run npm task "cb setup" '],
	["All projects defined in your codebaby.config.js will be set up.\n"],
	[arrow, " under /projects/foo is your new project"],
	[
		arrow,
		" new npm tasks for project were created in your codebaby.config.js\n( refresh codebaby.config.js )"
	],

	[pp],
	// [ '[ 1.2 ] ' ],
	[p],
	[{ yellow: "- build a project " }],
	[p],
	['run npm task "foo:dev" '],
	['Project "foo" will be started in dev mode.\n'],
	[arrow, " browserSync starts your project in google chrome browser"],
	[arrow, " edit file under projects/foo/src/html/index.html."],
	[arrow, " On saving your changes - the browser will refresh."],
	[chapter],
	// [ { magenta: '< PROJECTS & TEMPLATES > ' } ],
	// [ pp ],
	["[ 2.0 ] ", { magenta: "< PROJECTS & TEMPLATES > " }],
	[p],
	[{ yellow: "- projects " }],
	[p],
	["You can have multiple projects inside your workspace."],
	[p],
	[arrow, " All created projects are under: /projects"],
	[p],
	["for example:"],
	["/projects/myReactApp"],
	["/projects/myAngularApp"],
	["/projects/myJqueryWebsite"],
	// [ pp ],
	// [ '[ 2.1 ] ' ],
	[p],
	[{ yellow: "- project templates " }],
	[p],
	["Each project is created from a project template."],
	[p],
	[arrow, " All templates are under: /codebaby/tpl/projects"],
	[p],
	[
		"You can define new templates, or edit the existing templates for your own purpose."
	],
	// [ pp ],
	// [ '[ 2.2 ] ' ],
	[p],
	[{ yellow: "- assign a project template " }],
	[p],
	[
		'Each project is created from a project template. Assign a template\nto a project happens inside "projects in your codebaby.config.js'
	],
	[p],
	[{ example: '"projects" : {\n "foo":""\n}' }],
	[p],
	[""]
]
