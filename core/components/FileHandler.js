const fs = require("fs")
const chalk = require("chalk")
const { promListSync, promList } = require("../utils/promise")
const fileUtil = require("../utils/file")

const _log = require("../utils/logger").log("fileHandler", false)
const _info = require("../utils/logger").info()

class FileHandler {
	constructor(config, resolve) {
		this.resolve = resolve
		this.stack = config.stack.concat()
		this.targets = config.targets.concat()
		this.shared = config.shared
		this.shared.fileData.stats.timeStart = new Date().getTime()
		this.concat = false
		this.error = false
		this.read = true
		this.write = true
		this.async = true

		if (config.concat !== undefined) {
			this.concat = config.concat
		}
		if (config.read !== undefined) {
			this.read = config.read
		}
		if (config.write !== undefined) {
			this.write = config.write
		}
		if (config.async !== undefined) {
			this.async = config.async
		}
		if (this.read) {
			this.stack.splice(0, 0, readFile)
		}
		if (this.write && !this.concat) {
			this.stack.push(writeFile)
		}

		// console.log('');
		// console.log(chalk.magenta('read ') + ' : ' + this.read);
		// console.log(chalk.magenta('write ') + ': ' + this.write);
		// console.log(chalk.magenta('async ') + ': ' + this.async);
		// console.log(chalk.magenta('concat ') + ': ' + this.concat);

		if (this.async) {
			this.promList = promListSync
		} else {
			this.promList = promList
		}
		this.promListStack = promListSync

		this.runTargets()
	}

	runTargets() {
		let funcListTargets = this.targets.map(entry => {
			return next => {
				return () => {
					this.runStack(entry, () => next())
				}
			}
		})

		this.promList(funcListTargets).then(() => this.ready())
	}

	runStack(entry, cb) {
		let funcList = this.stack.concat().map(func => {
			return next => {
				return () => {
					// console.log(func);
					// console.log(chalk.yellow('target : ' + entry.path+'\n'));
					// console.log(chalk.yellow('error : ' + this.shared.error));
					func(entry, this.shared, () => next())
				}
			}
		})

		this.promListStack(funcList).then(() => cb())
	}

	concatFiles(cb) {
		let data = this.targets.reduce((acc, entry) => acc + entry.content, "")
		// console.log(data)
		// console.log(this.shared.fileData)
		fileUtil
			.writeFile(this.shared.fileData.paths.baseWrite, data)
			.then(error => {
				if (error) {
					console.log("\nerror write " + data.to)
					cb(error)
				} else {
					cb()
				}
			})
	}

	ready() {
		if (this.concat) {
			this.concatFiles(() => this.complete())
		} else {
			this.complete()
		}
	}

	complete() {
		let timeEnd = new Date().getTime()
		let duration = timeEnd - this.shared.fileData.stats.timeStart

		this.shared.fileData.stats.timeEnd = timeEnd
		this.shared.fileData.stats.duration = duration
		this.destroy()
		this.resolve()
		this.resolve = undefined
	}

	destroy() {
		this.shared = undefined
		this.targets = undefined
		this.promList = undefined
		this.promListStack = undefined
		this.stack = undefined
	}
}

function readFile(data, shared, next) {
	let from = data.from || data.pathAbs
	_log("read : " + from)
	data.src = ""
	data.content = ""

	fs.readFile(from, "utf-8", (err, result) => {
		if (err) {
			console.log(err)
			next()
		} else {
			data.src = result
			data.content = result
			next()
		}
	})
}

function writeFile(data, shared, next) {
	_log("write : " + data.to)

	// if (shared.fileData.to === "ng-templates") {
	// 	console.log(data)
	// 	console.log(shared.fileData)
	// }
	if (!data.error) {
		if (data.content === undefined) {
			data.content = data.src
		}

		fileUtil.writeFile(data.to, data.content).then(error => {
			if (error) {
				console.log(chalk.red("!!! ") + chalk.yellow("Error write file : "))
				console.log(data.to)
				// console.log(shared.fileData);
				console.log(error)
				next(error)
			} else {
				next()
			}
		})
	} else {
		next()
	}
}

module.exports = function(config) {
	return new Promise(resolve => {
		new FileHandler(config, resolve)
	})
}
