const path = require("path")
const fs = require("fs")
const chalk = require("chalk")
const handler = require("../../handler/copy/handler")
const FileData = require("../components/FileData")
const FileTree = require("../components/FileTree")
const FileWorks = require("../components/FileWorks")
const fileUtil = require("../utils/file")
const argsParser = require("../utils/args")
const _log = require("../utils/logger").log("project", false)
const _info = require("../utils/logger").info()

let pathFromTpl = "codebaby/tpl/projects/"
let pathFromModules = "codebaby/modules/"
let to = "projects/"
let pathWs = path.resolve()
let createdProjects = []
let existingProjects = []
let createdModules = []
let existingModules = []
let convertedFiles = [[/\/babel-config.json/, "/.babelrc"]]
let npmTaskMode
module.exports = {
	createProj: _createProj,
	deleteProj: _delete,
	setupProj: _setup,
	initUi: settings => {
		npmTaskMode = settings
	}
}

function _create(name, tpl, modules) {
	_log("_create", 0)
	if (modules === undefined) {
		modules = []
	}

	return new Promise((resolve, reject) => {
		buildProject(name, tpl, err => {
			if (!err || err.type === "project exist") {
				handleModules(name, modules, () => {
					resolve()
				})
			} else {
				reject(err)
			}
		})
	})
}

function _delete(name) {
	let pathProj = to + name
	let error = false

	if (checkExist(pathProj, "there is no project to delete : " + name, false)) {
		error = true
	}
	if (!error) {
		let fw = new FileWorks()
		fw.remove({ path: pathProj }).then(() => {
			updatePackage(name, "remove", err => {
				if (!err) {
					console.log(chalk.green("project " + name + " deleted!"))
				}
			})
		})
	}
}

function _setup(proj) {
	_log("_setup", 0)

	if (
		!checkExist("codebaby.config.js", "codebaby.config.js not found !", false)
	) {
		let config = require("../../../codebaby.config")

		let projects = getProjects(config)
		let projList = []

		for (let property in projects) {
			if (projects.hasOwnProperty(property)) {
				if (proj === "all" || property === proj) {
					let args = argsParser.parse(projects[property])
					if (args === undefined) {
						args = { tpl: "", mod: [] }
					}
					projList.push({
						name: property,
						tpl: args.tpl || "",
						modules: args.mod || []
					})
				}
			}
		}

		if (projList.length === 0) {
			console.log(chalk.red("No projects defined!"))
			return
		}

		// let projList = config.projects;
		function createNext() {
			let func = stack.shift()
			if (func) {
				func()
			}
		}

		let stack = []
		let promises = projList.map(entry => {
			return new Promise((resolve, reject) => {
				stack.push(() => {
					_create(entry.name, entry.tpl, entry.modules)
						.then(() => {
							resolve()
							createNext()
						})
						.catch(err => {
							console.log(chalk.red(err))
							reject()
							createNext()
						})
				})
			})
		})

		createNext()

		Promise.all(promises)
			.then(() => {

				if (createdProjects.length > 0) {
					console.log("-> " + chalk.green("created :"))
					createdProjects.forEach(path => {
						console.log("- " + path)
					})

					console.log("\n! " + chalk.yellow("npm scripts updated "))
				}
				if (createdModules.length > 0) {
					console.log("\n-> " + chalk.green("added module(s)  :\n"))
					createdModules.forEach(path => {
						console.log("- " + path)
					})
					console.log(
						"\n! " + chalk.yellow("check your modules folder in project ")
					)
				}
				if (createdProjects.length === 0 && createdModules.length === 0) {
					console.log("\nsetup ready: " + chalk.yellow("no changes detected!"))
				}
			})
			.catch(e => {
				console.log("Error")
				console.log(e)
			})
	}
}

function _createProj(name, tpl, modules) {
	_log("_createProj", 0)

	_create(name, tpl, modules).then(() => {
		if (createdProjects.length > 0) {
			console.log("-> " + chalk.green("created  :"))
			createdProjects.forEach(path => {
				console.log("- " + path)
			})
		}
		if (createdModules.length > 0) {
			console.log("\n-> " + chalk.green("added module(s) :"))
			createdModules.forEach(path => {
				console.log("- " + path)
			})
		}
	})
}

function buildProject(name, tpl, cb) {
	_log("buildProject : " + name)

	if (tpl === "" || tpl === undefined) {
		tpl = "default"
	}

	let pathProj = to + name
	let pathTpl = pathFromTpl + tpl
	let error = false
	let errorType = ""

	if (checkExist(pathProj, "", true)) {
		error = true
		errorType = "project exist"
		existingProjects.push(pathProj)
	}

	if (checkExist(pathTpl, "tpl not found : " + tpl, false)) {
		error = true
		errorType = "tpl not found"
	}

	if (!error) {
		createdProjects.push(pathProj)
		fileUtil.mkdir(pathProj).then(() => {
			let ft = new FileTree()
			ft.read({
				path: pathTpl,
				transform: path.resolve() + "/projects/" + name + "/"
			}).then(result => {
				let pathProj = "projects/" + name
				let data = {
					path: pathProj,
					pathProj: pathProj,
					to: pathProj,
					replace: [[/{#PROJ#}/gi, name, "{#PROJ#}"]]
				}

				// console.log( result.transform );

				result.transform.forEach(entry => {
					// console.log( entry );

					convertedFiles.forEach(entryConv => {
						let regEx = entryConv[0]
						let val = entryConv[1]
						if (entry.path.match(regEx)) {
							entry.to = entry.to.replace(regEx, val)
						}
					})
				})

				let fd = new FileData(data)
				// console.log(fd);

				let handlerData = {
					fileData: fd,
					compileList: result.transform,
					evt: { type: "" }
				}
				handler(handlerData, () => {
					cb(null)
				})
			})
		})
	} else {
		cb({ type: errorType })
	}
}

function handleModules(name, modules, cb) {
	if (typeof modules === "string") {
		modules = [modules]
	}
	_log("handleModules : " + name)

	buildModules(name, modules, () => {
		_log("buildModules : ready")
		updatePackage(name, "add", err => {
			if (cb) {
				cb()
			}
		})
	})
}

function buildModules(name, list, cb) {
	_log("buildModules : " + list.length)
	if (list.length > 0) {
		let promises = list.map(module => {
			let pathProj = to + name + "/modules/" + module + "/"
			let pathModule = pathFromModules + module
			let error = false

			if (checkExist(pathProj, "", true)) {
				error = true
				existingModules.push(pathProj)
			}

			if (checkExist(pathModule, "module not found : " + module, false)) {
				error = true
			}

			if (!error) {
				let mod = require(pathWs + "/" + pathModule + "/index")
				if (mod.setup) {
					createdModules.push(pathProj)
					return mod.setup(name)
				} else {
					console.log(
						chalk.red("Every module should have a setup(projName) function !")
					)
				}
			}
		})

		Promise.all(promises).then(() => {
			cb()
		})
	} else {
		cb()
	}
}

function getProjects(data, init, cb) {
	if (typeof init === "function") {
		init = false
	}
	let defaultData = {}
	if (init) {
		defaultData = { foo: "" }
	}
	if (data["projects"] === undefined) {
		return defaultData
	} else {
		return data["projects"]
	}
}

function updatePackage(name, action, cb) {
	getPackageJson((err, json) => {
		if (!err) {
			let cmds = {}
			cmds.cmd_d = [name + ":dev", "node codebaby/core/index.js build=" + name]
			cmds.cmd_p = [
				name + ":production",
				"node codebaby/core/index.js build=" + name + " production"
			]
			cmds.cmd_r = [
				name + ":remote",
				"node codebaby/core/index.js build=" + name + " remote"
			]
			cmds.cmd_t = [name + ":test", "node codebaby/core/index.js test=" + name]

			if (json.browserslist === undefined) {
				json.browserslist = ["> 1%", "last 2 versions"]
			}

			if (json.scripts === undefined) {
				json.scripts = {}
			}

			if (action === "add") {
				let ui = npmTaskMode.split(":")
				if (ui.length === 0) {
					ui = ["d", "p", "r", "t"]
					console.log(
						chalk.red("!!! ") +
							chalk.yellow(
								"current npmTaskMode is empty and will be set to default."
							)
					)
				}
				ui.forEach(entry => {
					let cmd = cmds["cmd_" + entry]
					json.scripts[cmd[0]] = cmd[1]
				})


			} else {
				Object.keys(cmds).forEach(key => {
					let cmd = cmds[key][0]
					if (json.scripts[cmd] !== undefined) {
						delete json.scripts[cmd]
					}
				})

			}

			updatePackageJson(json, err => {
				if (cb) {
					cb(err)
				}
			})
		} else {
			console.log(chalk.red("Error : updatePackage :  " + name))
			console.log(chalk.red(err))
		}
	})
}

// ----------------------------------------------------- HELPER
function checkExist(path, mess, condition) {
	let error = false
	if (fs.existsSync(path) === condition) {
		if (mess !== "") {
			console.log(chalk.red(mess))
		}
		error = true
	}

	return error
}

function getPackageJson(cb) {
	fs.readFile("package.json", "utf-8", (err, data) => {
		if (!err) {
			try {
				let json = JSON.parse(data)
				cb(null, json)
			} catch (err) {
				cb(err)
			}
		} else {
			cb(err)
		}
	})
}

function updatePackageJson(data, cb) {
	fs.writeFile("package.json", JSON.stringify(data, null, 1), err => {
		if (err) {
			console.log(chalk.red("error writing package.json"))
		} else {
			cb(null)
		}
	})
}
