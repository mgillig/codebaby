/*
 create a project from default template -> "foo": ""
 create a project from template ( eg. typescript)  -> "foo" : "tpl=typescript"
 project using a  module  ( eg. cordova ) -> "foo" : "mod=cordova"
 combine template &  module -> "foo" : "tpl=typescript mod=cordova"

 -> all projects under : codebaby/tpl/projects
 -> all modules under : codebaby/modules


 It is possible to create a different versions direct from the npm task via command line arguments
 ( for example in package.json )
  "foo : dev": "node codebaby/core/index.js build=foo"

  copy this task with a additional custom option
  "foo : dev": "node codebaby/core/index.js build=foo" NODE_ENV_LANG=en

  NODE_ENV_LANG  will be accessable via preprocess, so it
  is possible to create cloned version just through different tasks


 */

module.exports = {
    "projects": {
        "foo-es5": "", // es5
        //"foo-babel-env": "tpl=babel-env", // es6++
        // "foo-vue": "tpl=vue",
        //"foo-react": "tpl=react",
        //"foo-react-typescript": "tpl=react-typescript",
        //"foo-angular-js": "tpl=angular-js",
        //"foo-angular-js-typescript": "tpl=angular-js-typescript",
        //"foo-typescript": "tpl=typescript"
    }
};
