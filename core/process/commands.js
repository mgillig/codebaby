const chalk = require('chalk');
const path = require('path');
const fs = require('fs');
const pathUtil = require('../utils/path');
const FileTree = require('../components/FileTree');
const FileData = require('../components/FileData');
const cbInterface = require('../components/readline');
const copy = require('../../handler/copy/handler');

const _log = require('../utils/logger').log('commands', false);
const _info = require('../utils/logger').info();

let cache = {path: undefined};
let commands;
let paths;
module.exports = {

    initCommands: _init,
    copyTemplate: _copyTpl,
    initConfig: _initConfig
};

function _init(list) {
    commands = list;
    cbInterface.init();
    startInterface(list);
}

function _initConfig(dirname) {
    paths = pathUtil.getProjectPaths(dirname);
    return cache;
}

function startInterface(commands) {

    if (commands.length > 0) {
        _info('-> ' + chalk.yellow('commands ') + ':\n', true);

    }
    commands.forEach(entry => {

        if (entry.cb) {
            cbInterface.register(entry.cmd, entry.cb);
        }
        _info('$ ' + entry.syntax, true);
        _info('( ' + chalk.yellow(entry.info) + ' )\n', true);

        if (entry.description) {
            _info(entry.description, true);
        }
    });
}

//-------------------------------
function _copyTpl(cmd, args) {

    // console.log('--->  Copy tpl : ' + cmd);
    // console.log(args);
    // console.log( cache );


    if (args.hasOwnProperty('path')) {
        cache.path = args.path || '';
        console.log('-> ' + chalk.yellow('cache path ') + ': ' + cache.path);
    }

    if (cache.path === '' || cache.path === undefined) {
        console.log(chalk.red('!!!') + 'error - no path defined ');
        console.log('-> ' + chalk.yellow('provide a path ') + ': eg. "js/app" ');
        return;
    }

    let configCmd = undefined;
    commands.some(entry => {
        if (entry.cmd === cmd) {
            if (Object.keys(args).indexOf(entry.arg) > -1) {
                configCmd = entry;
                return true;
            }
        }
    });

    if (configCmd === undefined) {
        console.log(chalk.red('\n!!! ') + 'unknow , empty or wrong arg for command "' + cmd + '"  ');
        console.log('-> ' + chalk.yellow('known arg(s) are :  '));
        commands.forEach(entry => {
            if (entry.cmd === cmd) {
                console.log('- ' + entry.arg);
            }
        });
    } else {

        let name = args[configCmd['arg']];

        if (!configCmd.hasOwnProperty('from')) {
            console.log(chalk.red('\n!!! ') + 'missing arg : from ');
            return;
        }
        if (!configCmd.hasOwnProperty('to')) {
            console.log(chalk.red('\n!!! ') + 'missing arg : to ');
            return;
        }

        // console.log('command found');
        // console.log(configCmd);
        // console.log( '-----------' );
        let pathCompFiles = path.normalize(paths.pathProj + '/config/cmd/' + configCmd.from + '/');
        let pathCompTo = path.normalize(paths.pathWatch + '/src/' + cache.path + '/' + configCmd.to + '/');
        let pathExist = path.normalize(paths.pathWatch + '/src/' + cache.path + '/' + configCmd.to + '/' + name + configCmd.fileType);


        // console.log( 'template : '+template );
        // console.log( 'templateName : '+templateName );
        // console.log( 'pathCompTo : '+pathCompTo );
        // console.log( 'pathExist : '+pathExist );
        fs.exists(pathExist, exist => {


            if (!exist) {
                fs.exists(pathCompFiles, exist => {

                    if (!exist) {
                        console.log(chalk.red('error - no template found !'));
                        console.log(chalk.red('-> ' + pathExist));

                    } else {

                        let ft = new FileTree();
                        ft.read({
                            path: pathCompFiles,
                            transform: pathCompTo,
                            cwd: ''
                        }).then(result => {

                            let data = {
                                path: pathCompFiles,
                                to: pathCompTo
                            };
                            if(configCmd.replace !==undefined && configCmd.replace !==''){
                                data.replace= [[new RegExp(configCmd.replace,'g'), name, configCmd.replace]]
                            }

                            // console.log('----------');
                            // console.log(data);
                            copy({fileData: new FileData(data), compileList: result.transform}, () => {
                                console.log(chalk.green(configCmd.from + ' : ' + name + ' created !'));
                            })
                        });
                    }
                });
            } else {
                console.log(chalk.red('error - ' + pathExist + ' allready exist !'));
            }
        });
    }
}