const chalk = require('chalk');
const fs = require('fs');
const FileTree = require('../components/FileTree');
const {promListSync} = require('../utils/promise');
const handlerTest = require('../../handler/test/handler');

module.exports = function (proj, config) {

    // console.log( config );

    let testType = config.type;
    let pathTest = 'projects/' + proj + '/test/' + testType + '/specs';
    let hasTests = fs.existsSync(pathTest);
    let infoActivate = chalk.yellow('enable test settings in') + ' : projects/' + proj + '/config/build.js';
    let infoBuild = chalk.yellow('build your project');
    // console.log( build );

    if (!hasTests) {
        console.log(chalk.red('!!! ') + chalk.yellow('no tests found\n'));
        console.log('-> ' + infoActivate);
        console.log('-> ' + infoBuild);
    } else {
        if (!config.active) {
            console.log('! ' + chalk.yellow('tests not active'));
        }
        if (!testType) {
            console.log('! ' + chalk.yellow('#### no tests defined'));
        }

    }

    if (hasTests) {

        let ft = new FileTree();
        ft.read({path: pathTest,filter:['*.spec.js'], cwd: ''}).then(result => {

            let funcList = result.files.map(entry => {

                return (resolve) => {
                    return () => {
                        console.log('test : ' + chalk.blue(entry.path));
                        handlerTest(entry.path, testType, () => resolve());
                    }
                }
            });

            promListSync(funcList).then(() => {
                console.log(chalk.green('tests complete'));
            });
        });

    }

};