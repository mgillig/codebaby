const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const FileWorks = require('../components/FileWorks');

let cwd;
let pathFrom;
let pathTo;
let pathDest = '../../';
let folderDest = 'codebaby';

let pathRoot;


module.exports = function (force) {


    // console.log('__dirname : ' + __dirname);
    // console.log('__dirname ../../: ' + path.resolve(__dirname, '../../'));
    // console.log('__dirname ../../../../: ' + path.resolve(__dirname, '../../../../'));
    pathRoot = path.resolve(__dirname, '../../../../');


    if (__dirname.indexOf(path.sep + 'node_modules' + path.sep) > -1) {

        if (__dirname.indexOf(path.sep + 'codebaby-next' + path.sep) > -1) {
            folderDest = 'codebaby_install';
        }

        pathFrom = path.resolve(__dirname, '../../');
        pathTo = pathRoot + path.sep + folderDest;

        if (process.cwd() !== pathFrom) {

            process.chdir(pathFrom)
        }

    }

    // during npm installation cwd is /node_modules/codebaby
    cwd = process.cwd();



    if (fs.existsSync(pathTo)) {

        if (force) {
            console.log(chalk.yellow('- overriding your local codebaby! \n'));
            cleanCore();
        } else {

            let packageJson = require('../../package');
            let codebaby = require(pathRoot + '/codebaby/core/global');

            console.log(chalk.red('!!! ') + chalk.yellow('A local codebaby version allready exist! \n'));
            console.log('codebaby version (node_modules): ' + chalk.yellow(packageJson.version));
            console.log('codebaby version (workspace): ' + chalk.yellow(codebaby.version + ' \n'));


            console.log('You have 2 options :\n');
            console.log('1. '+chalk.yellow('[ clean install ] \n'));
            console.log('( All changes inside of your local version will be lost ! )');
            console.log('->  Delete your local codebaby folder. And run this command again : ' + chalk.yellow('npm run cb install\n'));
            // console.log( '( all custom changes inside of codebaby get lost ! )\n' );
            console.log('2. '+chalk.yellow('[ keep local changes in handler & tpl  ] \n'));
            console.log('Run command : ' + chalk.yellow('npm run cb install=force'));
            console.log('( only codebaby core files will be updated, custom added project templates, handler or modules will be untouched ! )');

        }

    } else {
        copyFiles(false);
    }
};

function cleanCore() {

    let fw = new FileWorks();
    let config = {
        path: pathDest + folderDest + '/core',
        cwd: '',
    };
    fw.remove(config).then(() => {
        copyFiles(true);
    })
}

function cleanUpFolder(pathDest, pathFrom) {


    return new Promise((resolve, reject) => {

        let folderFrom;
        let folderDest;
        const promises = [];

        promises.push(readDir(pathFrom).then((result => folderFrom = result)));
        promises.push(readDir(pathDest).then((result => folderDest = result)));

        Promise.all(promises).then(() => {

            // DELETE IN FOLDER

            const ignoredFolder = folderDest.filter(entry => folderFrom.indexOf(entry) === -1);
            const filter = ignoredFolder.map(entry => '!' + entry);
            let fw = new FileWorks();
            let config = {
                path: pathDest,
                filter: filter,
                cwd: '',
            };

            fw.remove(config).then(() => {
                copyFolder(pathDest, pathFrom).then(() => {
                    resolve();
                })
            })


        }).catch(e => {
            console.log(e);
            reject();
        })
    });
}

function copyFolder(pathDest, pathFrom) {

    return new Promise((resolve, reject) => {

        let fw = new FileWorks();
        let config = {
            path: pathFrom,
            dest: pathDest,
            cwd: '',
        };
        fw.copy(config).then(() => {
            resolve();
        })
    })
}


function copyFiles(force) {


    let ignore = ['!node_modules', '!bin', '!package.json', '!README.md', '!package-lock.json', '!.gitignore'];

    if (force) {
        ignore.push('!handler');
        ignore.push('!modules');
        ignore.push('!tpl');
    }

    let fw = new FileWorks();
    let config = {
        path: pathFrom,
        dest: pathDest + folderDest,
        cwd: '',
        filter: ignore
    };
    fw.copy(config).then(() => {

        let configFrom = pathFrom + '/core/process/assets/codebaby.config.js';
        let configTo = pathRoot + path.sep + 'codebaby.config.js';
        // console.log('configFrom : ' + configFrom);
        // console.log('configTo : ' + configTo);
        if (!fs.existsSync(configTo)) {
            // console.log('######### CREATE CONFIG');
            fs.readFile(configFrom, 'utf-8', (err, result) => {

                if (err) {
                    console.log(err);
                } else {
                    fs.writeFile(configTo, result, (err => {
                        ready();
                    }))
                }
            });
            // fs.createReadStream( from ).pipe( fs.createWriteStream( codebabyConfig ) );
        } else {

            if (force) {

                const promises = [];
                promises.push(cleanUpFolder(pathDest + folderDest + '/modules', pathFrom + '/modules'));
                promises.push(cleanUpFolder(pathDest + folderDest + '/tpl/projects', pathFrom + '/tpl/projects'));
                promises.push(cleanUpFolder(pathDest + folderDest + '/tpl/test', pathFrom + '/tpl/test'));
                promises.push(cleanUpFolder(pathDest + folderDest + '/handler', pathFrom + '/handler'));

                Promise.all(promises).then(() => {
                    ready(true);
                })
            } else {
                ready(false);
            }

        }
    });
}


function ready(force = false) {
    let config;

    function logReady() {

        if(!force){
            console.log(chalk.green('codebaby successfully installed !\n'));
            console.log(chalk.yellow('define your projects in codebaby.config.js !\n'));
            console.log(chalk.yellow('build your projects with npm task "cb setup" in package.json !\n'));
        }else{

            console.log('Your loacal codebaby has been updated, custom templates, mudules and handler are untouched!');
        }

    }

    let pathConfig = pathRoot + '/package.json';

    // console.log('pathConfig : ' + pathConfig);

    if (fs.existsSync(pathConfig)) {

        // console.log( 8888 );

        config = require(pathConfig);
        let write = false;
        let flag = 'scripts';
        if (config[flag] !== undefined) {

            if (config[flag]['cb'] === undefined) {
                config[flag]['cb'] = 'codebaby';
                write = true;
            }
            if (config[flag]['cb:setup'] === undefined) {
                config[flag]['cb:setup'] = 'node codebaby/core/index.js setup';
                write = true;
            }
            if (config[flag]['cb:deploy'] === undefined) {
                config[flag]['cb:deploy'] = 'node codebaby/core/process/deploy.js';
                write = true;
            }

        } else {

            config[flag] = {'cb': 'codebaby', 'cb:setup': 'node codebaby/core/index.js setup','cb:deploy': 'node codebaby/core/process/deploy.js'};
            write = true;
        }

        if (write) {
            fs.writeFile(pathConfig, JSON.stringify(config, null, 1), (err => {

                if (err) {
                    console.log(err);
                }
                logReady()
            }))
        } else {
            logReady()
        }

        // fs.createReadStream( from ).pipe( fs.createWriteStream( codebabyConfig ) );
    } else {

        config = {scripts: {'cb': 'codebaby', 'cb:setup': 'node codebaby/core/index.js setup','cb:deploy': 'node codebaby/core/process/deploy.js'}};
        fs.writeFile(pathConfig, JSON.stringify(config, null, 1), (err => {

            if (err) {
                console.log(err);
            }
            logReady()
        }));
    }
}

// ---------------------------- HELPER

function readDir(path) {

    return new Promise((resolve, reject) => {


        fs.readdir(path, (err, result) => {
            if (err !== null) {

                console.log(chalk.red(err));
                reject([]);

            } else {
                resolve(result);
            }
        })
    })

}
