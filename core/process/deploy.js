const fs = require('fs');
const {exec} = require('child_process');
const path = require('path');
const chalk = require('chalk');
const ignoreDefault = ['.DS_Store'];

let date = new Date();
let year = date.getFullYear();
let month = date.getMonth() + 1;
let day = date.getDate();
// console.log( date );
// console.log( date.getDate() );
// console.log( day );

if (day < 10) {
    day = `0${day}`
}
if (month < 10) {
    month = `0${month}`
}
let deployFolder = `${year}-${month}-${day}`;
let projectExists = fs.existsSync('projects');
if (!projectExists) {
    console.log(chalk.yellow('! no projects found '));
    return;
}

fs.readdirSync('projects').forEach(entry => {

    let pathBuild = `projects/${entry}/config/build.js`;

    if (fs.existsSync(pathBuild)) {

        let build = require(path.resolve(pathBuild));
        if (build.project.deployFolder) {

            let pathFrom = build.project.buildFolder;
            let pathTo = path.normalize(build.project.deployFolder + path.sep);
            let zip = false;
            if (build.project.deployZip !== undefined) {
                zip = build.project.deployZip;
            }


            if (fs.existsSync(pathFrom)) {

                createFolder(pathTo);

                let version = getDeployVersion(pathTo, deployFolder);
                let deployFolderName = `${deployFolder}${version}`;
                let projectIgnores = build.project.deployIgnore || [];

                let ignores = ignoreDefault.concat(projectIgnores);
                ignores = ignores.map(entry => new RegExp(entry));

                console.log(chalk.green('project : ') + build.project.name);
                console.log(chalk.green('deployed to : ') + pathTo + deployFolderName);
                console.log(chalk.green('zip : ') + zip);
                console.log(chalk.green('ignores : '));
                console.log(ignores);

                copyDir(pathFrom, pathTo + deployFolderName, pathFrom, ignores);

                if (zip) {

                    console.log(chalk.green('zip ... \n'));
                    exec(`zip -r ${pathTo}${deployFolderName}.zip ` + pathTo + deployFolderName, (err, stdout, stderr) => {
                        if (err) {
                            console.error(err);
                            return;
                        }
                        // console.log(stdout);
                    });
                }
            } else {
                console.log(chalk.yellow('! no build found to deploy : ') + build.project.name);

            }
        }
    }
});

function getDeployVersion(pathTo, deployFolder) {

    let contents = fs.readdirSync(pathTo).filter(entry => {
        let stats = fs.statSync(pathTo + '/' + entry);
        return !stats.isFile();
    });

    let versions = 1;

    contents.forEach(entry => {

        if (entry.indexOf(deployFolder) > -1) {
            versions++;
        }
    });

    if (versions === 0) {
        versions = '';
    } else {

        let leading = (versions < 10) ? 0 : '';
        let versionStr = `-${leading}${versions}`;
        if (versionStr !== '-00') {
            versions = `-${leading}${versions}`;
        }
    }
    return versions;
}

function createFolder(pathStr) {

    let folders = pathStr.split(path.sep).filter(entry => entry !== '');
    folders.reduce((folder, base) => {

        let pathToCreate = folder + path.sep + base;
        if (!fs.existsSync(pathToCreate)) {
            fs.mkdirSync(pathToCreate)
        }
        return pathToCreate;
    }, process.cwd());
}

function checkIgnore(path, ignores) {
    let pass = true;
    ignores.some(test => {
        if (test.test(path)) {
            pass = false;
            return true;
        }
    });
    return pass;
}


function copyDir(pathDir, pathTo, base, ignores) {

    let contents = fs.readdirSync(pathDir);

    if (!fs.existsSync(pathTo)) {
        fs.mkdirSync(pathTo)
    }

    contents.forEach(entry => {

        let stats = fs.statSync(pathDir + '/' + entry);
        let from = pathDir + '/' + entry;
        let to = pathTo + '/' + entry;
        let fromRel = from.substr(base.length + 1);

        if (checkIgnore(fromRel, ignores)) {

            if (stats.isFile()) {
                fs.copyFileSync(from, to)
            } else {
                copyDir(from, to, base, ignores)
            }
        }
    })
}


