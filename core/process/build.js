const path = require('path');
const fs = require('fs');
const chokidar = require('chokidar');
const chalk = require('chalk');
const codebaby = require('../../core/global');
const browser = require('../components/browser');
const fileUtil = require('../utils/file');
const FileWorks = require('../components/FileWorks');
const {promListSync} = require('../utils/promise');
const {initCommands} = require('./commands');
const FileTree = require('../components/FileTree');
const cbInterface = require('../components/readline');
const HandlerInject = require('../components/HandlerInject');
const FileData = require('../components/FileData');
const handlerCopy = require('../../handler/copy/handler');
const helper = require('../utils/helper');
const _log = require('../utils/logger').log('build', false);
const _info = require('../utils/logger').info();


let pathDist;
let pathProj;
let handler;
let compileStackList;
let collectionHandler = {};
let bindedTests = {};
let watchList = [];
let ignoreList = [];
let commandList = [];
let watchedFiles = {};
let watchedFolders = [];
let watchedModules = [];
let testFolders = [];
let durationBuild = 0;
let callback;
let handlerTest;
let compileLog = [];
let testType;
let testCreated = '';
let projectInfo = '';
let lastScript;
let lastTest;
let startPath = 'index.html';

process.on('codebaby.browser.ready', data => {

    if (codebaby.build.browserSync.logLevel === 'silent') {
        _info(chalk.yellow('local       ') + ' : ' + chalk.magenta(data.local), true);
        _info(chalk.yellow('external    ') + ' : ' + chalk.magenta(data.external), true);
        if(codebaby.state.proxy){
            _info(chalk.yellow('proxy       ') + ' : ' + chalk.magenta(data.proxy), true);
        }
    }

    codebaby.update(data);

    _info('\n\n[ stats ]', true);

    fs.unlink(pathDist + startPath, (err) => {

        // console.log( 'unlink '+pathDist + startPath );
        if (codebaby.state.remoteLog && !codebaby.state.production) {
            remoteLogger.init();
        } else {
            console.log('');
            compile();
        }
    });
});
process.on('codebaby.filehandler.ready', handlerData => {

    // codebaby.messureEnd();
    // console.log( 'codebaby.filehandler.ready' );
    // console.log( handlerData );
    let fd = handlerData.fileData;
    _log('filehandler.ready : ' + fd.path, 2);

    if (codebaby.state.buildComplete && !handlerData.error) {

        if (codebaby.build.browserSync.logLevel === 'silent') {
            _info(chalk.green('reload') + ' ...', true);
        }

        browser.reload(fd.paths.writeRelative);

        if (codebaby.build.test && fd.handler === 'handler-javascript') {

            if (handlerData.evt.type === 'changed') {
                lastScript = handlerData.evt.path;
                let bindedScript = bindedTests[lastScript];

                if (bindedScript !== undefined) {

                    runTest(bindedScript[0]);
                }
            }
        }
        cbInterface.reset();
    }

    if (!codebaby.state.buildComplete) {
        compileLog.forEach(entry => {

            if (entry.path === fd.path) {
                entry.duration = fd.stats.duration;
                entry.ready = true;
            }
        });

        durationBuild += fd.stats.duration;
        _info(' ' + helper.formatTime(fd.stats.duration) + ' ', true);

    }

    if (handlerData.cb) {
        handlerData.cb();
    }

    // DESTROY
    handlerData.fileData = undefined;
    handlerData.evt = undefined;
    handlerData.cb = undefined;
});
process.on('codebaby.error', (type, err) => {

    console.log('\n\n' + chalk.red('Error : ' + type));
    console.log(err);
    browser.notify('<p>error : ' + type + '</p>', true);
});
module.exports = function (pHandler, watchedModulesList, cb) {

    _log('build', 0, true);
    _info('[ info ]\n', true);
    _info(chalk.yellow('project  ') + '    : ' + codebaby.build.project.name, true);


    FileData.preprocessEnv = codebaby.preprocessEnv;


    watchedModulesList.forEach(entry => {
        watchList.push(entry.pathWatch);
    });

    watchedModules = watchedModulesList;
    handler = pHandler;
    callback = cb;

    pathDist = codebaby.paths.pathDist;
    pathProj = codebaby.paths.pathProj;


    compileStackList = handler.concat();

    if (codebaby.state.production) {
        _info(chalk.yellow('production  ') + ' : ' + chalk.green(codebaby.state.production), true);
    } else {
        _info(chalk.yellow('production  ') + ' : ' + chalk.magenta(codebaby.state.production), true);
    }
    _info(chalk.yellow('remote      ') + ' : ' + chalk.magenta(codebaby.state.remote), true);
    _info(chalk.yellow('node version') + ' : ' + codebaby.nodeVersion, true);


    if (validateSrc()) {

        handleTests(() => {

            // CREATE BUILD FOLDER , DELETE COMPILE ARTIFACTS
            let pathDistFolder = codebaby.build.project.buildFolder;


            fileUtil.mkdir(pathDistFolder).then(created => {

                if (!created) {

                    if (codebaby.build.project.cleanFolder) {

                        //TODO CHANGE BUILD.JS
                        // add .ds_store global
                        let ignoreList = codebaby.build.project.ignoreFolder.concat(codebaby.build.project.ignoreFiles);
                        let ignore = ignoreList.map(entry => {
                            if (entry[0] !== '!') {
                                entry = '!' + entry;
                            }
                            return entry;
                        });
                        let config = {
                            path: pathDistFolder,
                            filter: ignore,
                            invisible: true
                        };

                        let fw = new FileWorks();
                        fw.remove(config).then(() => createPreloader())


                    } else {
                        createPreloader();
                    }
                } else {
                    createPreloader();
                }
            });
        })
    }
};

function handleTests(cb) {


    if (codebaby.build.test.active) {

        testType = codebaby.build.test.type;
        handlerTest = require(codebaby.paths.pathCodebaby + '/handler/test/handler');

        registerBindTest();
        registerUnbindTest();

        // setProjectInfo();

        let pathTest = codebaby.paths.pathProj + 'test/';
        watchList.push(codebaby.paths.pathWatch + 'test/' + testType + '/**/*.spec.js');
        // console.log(testType);
        // console.log(pathTest);
        // process.exit();
        let promise = new Promise(resolve => {

            if (fs.existsSync(codebaby.paths.pathWatch + 'test/' + testType)) {
                resolve();
            } else {

                // console.log('CREATE TEST : ' + codebaby.paths.pathWatch + 'test/');
                fileUtil.mkdir(codebaby.paths.pathWatch + 'test/' + testType).then(() => {

                    testFolders.push(pathTest);

                    let promises = [];
                    promises.push(new Promise(resolveSpecs => {
                        let ft = new FileTree();
                        let config = {
                            path: codebaby.paths.pathCodebaby + 'tpl/test/' + testType,
                            transform: pathTest + testType + '/specs/',
                            cwd: ''
                        };
                        ft.read(config).then(result => {

                            let data = {
                                path: pathProj,
                                to: 'projects/' + codebaby.build.project.name,
                                replace: [[/{#PROJ#}/g, codebaby.build.project.name, '{#PROJ#}']]
                            };

                            let handlerData = {fileData: new FileData(data), compileList: result.transform};

                            handlerCopy(handlerData, () => {
                                resolveSpecs();
                            });
                        });
                    }));

                    Promise.all(promises).then(() => {
                        testCreated = '-> ' + chalk.green('default test created') + ' : ' + chalk.blue(codebaby.paths.pathWatch + 'test/' + testType);
                        resolve();
                    })
                });


            }

        });
        promise.then(() => {
            cb()
        })
    } else {
        cb();
    }
}

function validateSrc() {

    _log('validateSrc', 0, true);

    let valide = true;
    let errors = chalk.red('\n!!!') + ' config.js - file(s) not found : \n';
    handler.forEach(entry => {

        entry.files.forEach(file => {

            if (!fs.existsSync(file.paths.read)) {
                let pathError = path.normalize('projects/' + codebaby.build.project.name + '/src/' + file.path);
                errors += '\n-> ' + chalk.yellow(entry.type) + ' : ' + pathError;
                valide = false;
            }
        })
    });

    if (!valide) {
        console.log(errors);
    }
    return valide;


}

function createPreloader() {

    _log('createPreloader', 0, true);

    if (codebaby.build.browserSync.startPath !== undefined && codebaby.build.browserSync.startPath !== '') {
        startPath = codebaby.build.browserSync.startPath;
    }
    // console.log('startPath : ' + startPath);

    fs.readFile(__dirname + '/assets/preloader.html', 'utf8', (err, data) => {

        if (!err) {
            data = data.replace(/{#PROJ#}/, codebaby.build.project.name);
            data = data.replace(/{#SRCFOLDER#}/, codebaby.build.project.srcFolder);
            data = data.replace(/{#BUILDFOLDER#}/, codebaby.build.project.buildFolder);
            data = data.replace(/{#NODE_ENV#}/, process.env['NODE_ENV']);
            // console.log( 'write : ' +codebaby.build.project.buildFolder + path.sep + startPath);
            fileUtil.writeFile(path.normalize(codebaby.build.project.buildFolder + path.sep + startPath), data).then(() => {
                browser.init(codebaby.build.browserSync);
            });
        } else {
            console.log(chalk.red(err));
        }
    });
}

function compile() {

    _log('compile', 0, true);

    createWatchList();
    compileStack();

}

function createWatchList() {

    _log('createWatchList', 0, true);

    handler.forEach(entry => {

        entry.compile = require(codebaby.paths.pathCodebaby + 'handler/' + entry.path + '/handler');
        collectionHandler[entry.type] = new HandlerInject(entry.config, entry.compile);

        entry.files.forEach(fileData => {

            let pathAbs = path.normalize(fileData.from + fileData.path);
            let watchGlobe;

            if (fileData.state.isFile) {
                watchedFiles[pathAbs] = {path: pathAbs, handler: fileData.handler, data: fileData};

                entry.config.fileTypes.forEach(type => {

                    watchGlobe = fileData.paths.baseRelative + '/**/*' + type;
                    if (watchList.indexOf(watchGlobe) === -1 && fileData.watch) {
                        // watchList.push(path.normalize(codebaby.paths.pathWatch + 'src/' + fileData.paths.baseRelative + '/**/*' + type));
                        watchList.push(path.normalize(watchGlobe));
                    }
                });

                addObservedFolder({path: fileData.paths.base, handler: fileData.handler, data: fileData});

            } else {


                watchGlobe = codebaby.paths.pathWatch + 'src/' + fileData.path + '/**/*';
                if (watchList.indexOf(watchGlobe) === -1 && fileData.watch) {
                    watchList.push(watchGlobe);
                }

                addObservedFolder({path: fileData.paths.read, handler: fileData.handler, data: fileData});
            }
            //ignoreWatch
            if (entry.config.ignoreWatch) {
                entry.config.ignoreWatch.forEach(entry => {

                    let pathdata = path.parse(entry);
                    if (pathdata.ext === '') {
                        watchGlobe = '**/' + entry + '/**/*';
                    } else {
                        watchGlobe = '**/' + entry;
                    }
                    if (ignoreList.indexOf(watchGlobe) === -1) {
                        ignoreList.push(watchGlobe);
                    }

                });
            }

        })
    });


    // order folders  on path
    watchedFolders.sort((a, b) => {

        let sortA = a.path;
        let sortB = b.path;
        if (sortA < sortB) {
            return 1;
        } else {
            return -1;
        }
    });

}

function compileSrc(list) {

    return new Promise(resolveCompileSrc => {

        let functionList = list.map(fileData => {

            return (resolve) => {
                return () => {
                    _info('' + fileData.handler + ' ' + chalk.blue(path.normalize('/' + fileData.path)) + ' ...');

                    let handler = collectionHandler[fileData.handler];
                    handler.compile(fileData, undefined, () => {

                        // console.log(chalk.yellow('compileSrc : ready ') + fileData.path);
                        resolve()
                    });
                }
            }
        });

        promListSync(functionList).then(() => resolveCompileSrc())
    })


}

function compileStack() {

    _log('compileStack', 1, true);

    let functionList = compileStackList.map(stack => {

        return (resolve) => {
            return () => {
                compileSrc(stack.files).then(resolve);
            }
        }
    });

    promListSync(functionList).then(() => compileStackReady())
}

function compileStackReady() {
    // console.log('---------------- x stack ready : ' + codebaby.state.buildComplete);

    if (!codebaby.state.buildComplete) {

        _info('\n' + chalk.yellow('compiled in') + ' : ' + helper.formatTime(durationBuild), true);
        startWatch();
    }
}

function compileFileData(fileData, evtData, resolve) {

    _log('compileFileData', 1, true);
    let handler = collectionHandler[fileData.handler];
    handler.compile(fileData, evtData, resolve);
}

function startWatch() {

    _log('startWatch', 0, true);
    // console.log('START WATCH ');
    // console.log(watchList);
    // watchedFolders.forEach( entry => console.log( entry.path ) );
    ignoreList.push('codebaby');
//TODO ADD
    // ___jb_tmp___
    // console.log( watchList );
    // console.log( '' );
    // console.log( watchList );
    // console.log( ignoreList );
    let watcher = chokidar.watch(watchList, {
        // ignored: /(^|[\/\\])\../,
        ignored: ignoreList,
        persistent: true
    });

    let success = function (mess, p) {

        _info('\n' + mess + ' ' + chalk.blue(p), true);

    };
    let warn = function (mess) {

        return console.log(chalk.red(mess));
    };

    watcher
        .on('add', p => {
            // console.log('buildComplete : ' + codebaby.state.buildComplete);
            if (codebaby.state.buildComplete) {
                success('added', p);
                compileFile(codebaby.paths.pathWs + p, 'added');
            }
        })
        .on('change', p => {
            // console.log('buildComplete : ' + codebaby.state.buildComplete);
            success('changed', p);
            compileFile(codebaby.paths.pathWs + p, 'changed');

        })
        .on('unlink', p => {

            success('unlink', p);

            let pathData = path.parse(codebaby.paths.pathWs + p);

            let pathEntry = getFileEntry(codebaby.paths.pathWs + p, pathData);
            if (pathEntry) {

                let filePathRel = path.relative(pathEntry.data.paths.read, p);
                let pathUnlink = pathEntry.data.paths.write + '/' + filePathRel;

                if (fs.existsSync(pathUnlink)) {
                    fs.unlinkSync(pathUnlink);
                }
                compileFileData(pathEntry.data, {path: p, pathData: undefined, type: 'unlink'})

            }
        });


    watcher
        .on('error', error => warn(`chokidar : Watcher error: ${error}`))
        .on('ready', () => {
            // console.log('WATCH READY ');
            getStats();
        })

    // .on( 'addDir', path => info( `Directory ${path} has been added` ) )
    // .on( 'unlinkDir', path => info( `Directory removed : ${path} ` ) )
}

function ready() {

    _log('ready', 0, true);

    // console.log( '\n' );
    if (codebaby.build.project.commands) {
        setProjectInfo();

    }
    console.log(projectInfo + '\n');

    codebaby.state.buildComplete = true;

    if (testCreated !== '') {
        _info(testCreated, true);
    }

    if (testType) {
        _info('-> ' + chalk.green('tests running with') + ' : ' + chalk.magenta(testType) + '\n', true);
    }


    watchedModules.forEach(entry => {
        console.log('');
        // process.emit( 'codebaby.module.' + entry.type, { type: 'init' } );
        entry.cb({type: 'init', data: entry});
    });


    if (codebaby.build.project.commands) {

        let pathCmd = path.normalize(codebaby.paths.pathProj + '/config/cmd/index');

        fs.exists(pathCmd + '.js', exist => {
            if (exist) {

                let commands = require(pathCmd);
                initCommands(commands.list)
            } else {
                // console.log( chalk.red( 'error - missing commands file :' ) );
                // console.log( pathCmd + '.js' );
                // console.log( 'set commands=false in your build.js' );
                // cb();
            }
            complete()
        });
    } else {
        complete()
    }
}

function complete() {

    browser.reload();
    if (codebaby.build.browserSync.logLevel === 'silent') {
        _info(chalk.green('reload') + ' ...\n', true);
    }
    callback();
}

function compileFile(filePath, eventType) {

    // codebaby.messureStart();

    let pathData = path.parse(filePath);
    // console.log( pathData );
    // pathData.ext = pathData.base.substr(pathData.base.indexOf('.'));

    // console.log( ext );
    // console.log( pathData );

    if (pathEntry = getFileEntry(filePath, pathData)) {

        let evtData = {path: filePath, pathData: pathData, type: eventType};
        compileFileData(pathEntry.data, evtData);

    } else {

        if (pathData.ext === '.js' && (filePath.indexOf('.spec.js') > -1 || filePath.indexOf('.test.js')) > -1) {
            runTest(filePath);

        } else {
            watchedModules.some(entry => {
                if (fileUtil.pathContains(entry.pathAbs, pathData.dir)) {
                    entry.cb({type: 'update', data: entry});
                    return true;
                }
            })

        }
    }

}

function runTest(pathTest) {

    _info(chalk.green('test') + ' ...\n', true);
    lastTest = pathTest;
    handlerTest(pathTest, testType, () => {

    });

}

function getFileEntry(filePath, pathData) {
    _log(filePath);
    let pathEntry = watchedFiles[filePath];

    // console.log( filePath );

    if (pathEntry === undefined) {

        watchedFolders.some(entry => {

            if (fileUtil.pathContains(entry.path, pathData.dir)) {
                // console.log( 'CONTAINS' );
                // console.log( entry );
                // console.log( entry.data.fileTypes );

                //TODO REMOVE CONDITION ( PERFORMANCE )
                if (entry.data.fileTypes.length === 0 || entry.data.fileTypes.join('') === '') {
                // if (entry.data.fileTypes.length === 0) {
                    pathEntry = entry;
                } else {
                    return entry.data.fileTypesRegex.some(regEx => {
                        // console.log( type );
                        // let regEx = new RegExp(type+'$');
                        // console.log( pathData.ext );
                        // console.log( regEx.test(filePath) );
                        // if (type === pathData.ext) {
                        if (regEx.test(filePath)) {
                            // console.log( entry );
                            pathEntry = entry;
                            return true;
                        }
                    });
                }
            }
        });
    }


    // if ( pathEntry ) {
    //     pathEntry.data.evt.pathData = pathData;
    //     pathEntry.data.evt.path = filePath;
    //     pathEntry.data.evt.type = eventType;
    //     // pathEntry.data.evt.pathRelative = path.relative( pathEntry.paths.read, filePath );
    // }

    return pathEntry;


}

function getStats() {

    let ft = new FileTree();
    ft.read({path: pathDist, ignore: ['.DS_Store'], stat: true}).then(result => {

        let myResult = [];
        let sizeTotal = 0;
        result.files.forEach(data => {

            let fileName = data.path;
            sizeTotal += data.stat.size;
            myResult.push({size: data.stat.size, info: '- ' + fileName + ' '});
        });

        myResult.sort(function (a, b) {

            if (a.size > b.size) {
                return 1;
            }
            if (a.size < b.size) {
                return -1;
            }
            if (a.size === b.size) {
                return 0;
            }
        });

        myResult.reverse();
        if (codebaby.build.project.trackSize) {
            console.log('\n');
            myResult.forEach(result => {
                _info(chalk.yellow(result.info) + formatSize(result.size), true);
            });
        }

        _info(chalk.yellow('size total ') + ' : ' + formatSize(sizeTotal), true);
        // fileTree.destroy();
        ready();

    });
}

// --------------------------------- INTERFACE ---------------------------------
function registerUnbindTest() {

    cbInterface.register('unbindtest', (input) => {

        let error = false;
        let errorMess = '';
        if (!lastTest) {
            errorMess += '\n-  you must run a test';
            error = true;
        }
        if (!lastScript) {
            errorMess += '\n-  you must compile a js ';
            error = true;
        }
        if (error) {
            console.log(chalk.red('!!! ') + chalk.yellow('Error unbinding test ') + errorMess);
        } else {

            console.log(chalk.magenta('script : ') + chalk.blue(shortPath(lastScript)));
            console.log(chalk.magenta('test   : ') + chalk.blue(shortPath(lastTest)));
            console.log('! ' + chalk.yellow('unbound '));

            let bindedScript = bindedTests[lastScript];
            if (bindedScript !== undefined) {
                delete  bindedTests[lastScript];
            }

            // console.log( bindedScript );
        }
    }, () => {

        // console.log( ' bind-test : reset' );
        // let specsData = runningSpecs[ lastTest ];
        // specsData.count = 0;


    });

    commandList.push({
        cmd: 'unbindtest',
        syntax: 'unbindtest',
        info: 'unbind a test.js from a .js file'
    });


}

function registerBindTest() {

    cbInterface.register('bindtest', (input) => {

        let error = false;
        let errorMess = '';
        if (!lastTest) {
            errorMess += '\n-  you must run a test';
            error = true;


        }
        if (!lastScript) {
            errorMess += '\n-  you must compile a js ';
            error = true;
        }
        if (error) {

            console.log(chalk.red('!!! ') + chalk.yellow('Error binding test ') + errorMess);
        } else {

            console.log(chalk.magenta('script : ') + chalk.blue(shortPath(lastScript)));
            console.log(chalk.magenta('test   : ') + chalk.blue(shortPath(lastTest)));
            console.log('! ' + chalk.yellow('bound '));


            let bindedScript = bindedTests[lastScript];
            if (bindedScript === undefined) {
                bindedScript = [];
                bindedTests[lastScript] = bindedScript;
            }
            if (bindedScript.indexOf(lastTest) === -1) {
                bindedScript.push(lastTest);
            }
            // console.log( bindedScript );
        }
    }, () => {

        // console.log( ' bind-test : reset' );
        // let specsData = runningSpecs[ lastTest ];
        // specsData.count = 0;


    });
    commandList.push({
        cmd: 'bindtest',
        syntax: 'bindtest',
        info: 'bind a test.js to a .js file'
    });

}


// --------------------------------- HELPER ---------------------------------

function shortPath(path) {
    return path.substr(codebaby.paths.pathWs.length, path.length)
}

function addObservedFolder(data) {


    //TODO ????? DUPLICATES ????
    let exist = false;
    watchedFolders.some(entry => {


        // let test = false;
        // if ( entry.path === data.paths.read && data.handler === handler ) {
        //     test = true;
        //     // console.log( 'exits ' +path + ' '+handler);
        // }
        // // console.log( 'check : ' +entry.path);
        // // console.log( 'path  : ' +path);
        // // console.log( '--------------- '+(entry.path == path) );
        // return test;
    });

    if (!exist) {

        // console.log( 'addObservedFolder ' + data.paths.read + ' ' + data.handler );
        watchedFolders.push(data);
    }

}

function setProjectInfo() {

    // projectInfo = chalk.magenta( 'project info' );
    _info('\n\n[ options ]');
}

function formatSize(size) {
    let i = Math.floor(Math.log(size) / Math.log(1024));
    let unit = ['B', 'KB', 'MB', 'GB', 'TB'][i];
    let mySize = size;
    if (i === 1) {
        mySize = Math.ceil((size / 1000));
    }
    if (i === 2) {
        mySize = (size / 1000000).toFixed(2);
    }

    if (unit === undefined) {
        unit = 'B';
    }

    return mySize + ' ' + unit;

}