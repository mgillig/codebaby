"use strict";
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const fileUtil = require('../utils/file');
const FileData = require('../components/FileData');
const _log = require('../utils/logger').log('configuration', false);
const _info = require('../utils/logger').info();

let src;
let package_json;
let dependencies = [];
let error = false;
let activeHandler = [];
let handler = [];
let installedModules = [];
let install = [];
let collectionHandler = {};
let stack = [];
let pathWs;
let pathProj;
let production;
let build;


module.exports = function (paths, projDependencies, tests, prod, projBuild, cb) {

    _log('configuration', 0, true);

    production = prod;
    pathProj = paths.pathWatch;
    build = projBuild;
    // READ SRC, HANDLER, PACKAGE.JSON FILES
    // CREATE HANDLER STACK
    pathWs = paths.pathWs;

    let promises = [];
    let testDependencies = [];

    if (tests === undefined) {

        let pathBuild = paths.pathWatch + 'config/build.js';
        console.log(chalk.yellow('-> ') + 'deprecated "tests"');
        console.log('please remove in : ' + chalk.blue(pathBuild));
        console.log('the entry "tests" , ' + chalk.yellow('which is now now deprecated'));
        console.log('and replace it with the following block:\n');
        console.log('"test":{ active:false, ');
        console.log('"type":"jasmine"  // Can be either "mocha", "jasmine" or "tape"');
        console.log('},');
        process.exit();

    }

    if (tests.active) {

        let testHandlerConfig = require('../../handler/test/handler-config');
        testDependencies = testHandlerConfig.dependencies.filter(entry => {
            // console.log( entry );
            if (entry.name === tests.type || (entry.depends !== undefined && entry.depends.indexOf(tests.type) > -1)) {
                return entry;
            }
        });
        // console.log( testHandlerConfig );
        // console.log( testDependencies );

    }

    // check module dependencies
    let pathModules = paths.pathProj + 'modules';
    let moduleDependencies = [];


    // console.log('pathModules : ' + pathModules);
    if (fs.existsSync(pathModules)) {


        let dir = fs.readdirSync(pathModules);
        let folders = dir.filter(entry => {
            let stat = fs.statSync(path.normalize(pathModules + path.sep + entry));
            return stat.isDirectory();
        });
        folders.forEach(module => {


            let pathModuleconfig = pathModules + path.sep + module + '/module.config.js';
            //console.log('pathModuleconfig : ' + pathModuleconfig);

            if (fs.existsSync(pathModuleconfig)) {

                //console.log('pathModuleconfig : ' + pathModuleconfig);
                let moduleConfig = require(pathModuleconfig);
                if (moduleConfig.dependencies) {
                    moduleDependencies = moduleDependencies.concat(moduleConfig.dependencies)
                }

            }
        })
    }


    projDependencies = [].concat(projDependencies, moduleDependencies, testDependencies);
    projDependencies.forEach(checkDependencies);

    promises.push(getFile(paths.pathProj + '/config/src.js').then((result) => {
        src = result;
    }));


    _log(paths.pathWs + 'package.json');
    promises.push(readFile(paths.pathWs + 'package.json').then((result) => {

        try {
            package_json = JSON.parse(result);
        } catch (error) {
            console.log('configuration : error');
            console.log(error);
        }

    }).catch(error => {
        console.log('configuration : error');
        console.log(error);

    }));

    Promise.all(promises).then(() => {

        if (error) {
            console.log('configuration : error');
            process.exit();
        } else {

            getHandler(paths.pathCodebaby, src, paths.pathSrc, paths.pathDist);
            checkInstalledModules();


            _log('ready', 0, true);
            cb({handler: stack, install: install, activeHandler: handler});
        }
    })
}


function getHandler(pathCb, source, pathSrc, pathDist) {
    _log('getHandler', 0, true);
    // CHECK IF A HANDLER HAS FILES TO HANDLE

    for (let handlerName in source) {

        if (source.hasOwnProperty(handlerName)) {

            if (handlerName === 'handler-html-ejs') {

                _info(chalk.yellow(' -> ') + 'deprecated handler name : ' + chalk.yellow('"' + handlerName + '"'), true);
                _info(chalk.yellow(' -> ') + 'please rename handler in ' + chalk.blue('config/src.js') + ' to : ' + chalk.yellow('"handler-html"'), true);
                handlerName = 'handler-html';
                console.log('');
                process.exit();
            }

            let files = source[handlerName];
            if (files) {
                let activeFiles = files.filter(file => {

                    if (file.active === undefined || file.active) {
                        return file;
                    }
                });
                if (activeFiles.length > 0) {
                    // CREATE THE HANDLER STACK ENTRY FOR BUILD
                    let handlerPath = handlerName.replace(/handler-/, '');
                    let pathRequire = path.normalize(pathCb + '/handler/' + handlerPath);
                    let config = require(pathRequire + '/handler-config.js');
                    let handlerEntry = {path: handlerPath, name: handlerName, data: activeFiles};
                    let handlerFileData;
                    if (fs.existsSync(pathRequire + '/FileData.js')) {

                        handlerFileData = require(pathRequire + '/FileData.js');
                    }
                    // console.log( config );
                    activeFiles = activeFiles.map(entry => {


                            let fileTypeDest = config.fileTypeDest;


                            if (entry.fileTypeDest !== undefined) {
                                if (typeof entry.fileTypeDest === 'object' && entry.fileTypeDest.length === 2) {

                                    if (production) {
                                        fileTypeDest = entry.fileTypeDest[1];
                                    } else {
                                        fileTypeDest = entry.fileTypeDest[0];
                                    }
                                } else if (typeof entry.fileTypeDest === 'string') {
                                    this.fileTypeDest = entry.fileTypeDest;
                                }
                            }

                            if (config.optionalDependencies) {
                                checkOptionalDependencies(entry, config.optionalDependencies);
                            }


                            // if (handlerName === 'handler-javascript') {
                            //     if (entry.es5 === undefined) {
                            //         entry.es5 = true;
                            //     }
                            //     console.log(entry);
                            // }


                            entry.fileType = config.fileType;
                            entry.fileTypes = config.fileTypes;
                            entry.fileTypeDest = fileTypeDest;
                            entry.observe = config.observe;
                            // entry.ignore = config.ignore;
                            entry.ignoreTree = config.ignoreTree || [];
                            // entry.ignoreTreeFiles = config.ignoreTreeFiles || [];
                            entry.notWatch = config.notWatch;
                            entry.from = pathSrc;
                            entry.dest = pathDist;
                            entry.handler = config.type;
                            entry.pathProj = pathProj;

                            let fd = new FileData();
                            // if (handlerName === 'handler-javascript') {
                            //     if (entry.es5 === undefined) {
                            //         entry.es5 = true;
                            //     }
                            //     console.log(entry);
                            // }


                            fd.initData(entry);
                            if (handlerFileData) {
                                handlerFileData(fd, config, build);
                            }

                            return fd;
                        }
                    );

                    activeHandler.push(handlerEntry);

                    collectionHandler[handlerName] = config;
                    config.dependencies.forEach(entry => checkDependencies(entry));

                    stack.push({
                        type: handlerName,
                        path: handlerPath,
                        templates: config.templates,
                        config: config,
                        stackPos: config.config || 0,
                        files: activeFiles
                    });
                }
            }
        }
    }

    _log('getHandler end', 0, true);
}

function checkOptionalDependencies(entry, list) {

    list.forEach(listEntry => {

        if (entry.hasOwnProperty(listEntry.flag)) {
            if (entry[listEntry.flag]) {
                // console.log( '-----> ' + listEntry.flag );
                _log('add optional depencie : ' + listEntry.flag);
                dependencies.push(listEntry.dependencie);

            }
        }
    });

}

function checkInstalledModules() {

    // console.log( '####### 999' );
    // dependencies.push( { name: "typescript" } );
    // dependencies.push( { name: "typescript" } );

    // filter double entrys
    let entrys = [];
    let cleanedDepencies = dependencies.filter(entry => {
        if (entrys.indexOf(entry.name) === -1) {
            entrys.push(entry.name);
            return true;
        }
    });
    entrys = undefined;

    // console.log( dependencies );
    // console.log('----------------------' );
    // console.log( cleanedDepencies );
    // process.exit();

    _log('checkInstalledModules', 0, true);
    if (package_json.devDependencies) {
        collect(package_json.devDependencies);
    }
    if (package_json.dependencies) {
        collect(package_json.dependencies);
    }
    if (package_json.optionalDependencies) {
        collect(package_json.optionalDependencies);
    }

    _log('getHandler end', 0, true);
    install = cleanedDepencies.filter(entry => {

        let existInNode = fs.existsSync(pathWs + '/node_modules/' + entry.name);
        let toInstall = false;
        let isInConfig = true;
        if (installedModules.indexOf(entry.name) === -1) {
            isInConfig = false;
            toInstall = true
        }
        // console.log( '--> ' +entry.name+' : '+existInNode );
        if (!existInNode) {

            _log('existInNode : ' + existInNode);
            if (isInConfig) {
                console.log(chalk.red('!!! ') + chalk.yellow('dependencies error ') + '\n\n"' + entry.name + '" not found in node_modules, but is listed in your package.json!');
                console.log('-> ' + chalk.yellow('delete ' + entry.name + ' in your package.json'));
                console.log('-> ' + chalk.yellow('or run $ npm i '));
                console.log('-> ' + chalk.yellow('then run this task again.'));
                process.exit();
            }

            toInstall = true
        }
        return toInstall
    });

}

// --------------------------------- HELPER ---------------------------------
function checkDependencies(entry) {

    _log('checkDependencies ', 0, true);
    _log(entry);

    // console.log( entry );
    let registered = false;

    dependencies.some(dep => {
        registered = (dep.name === entry.name);
        return registered;
    });

    // if ( registered ) {
    //     let existInNode = fs.existsSync( pathWs + '/node_modules/' + entry.name );
    //     if ( !existInNode ) {
    //         console.log( chalk.red( 'dependencies error :\n' + entry.name + ' not found in node_modules !' ) );
    //         console.log( '- delete ' + entry.name + ' in your package.json\n- or run $ npm i\n ' );
    //     }
    // }
    if (!registered) {
        let getDependencies = true;
        if (entry.depend) {
            // console.log( entry.depend + ' ' + entry.name );
            getDependencies = getDependDependencies(entry.depend, entry.name);
            //TODO CHECK DEPEND ON DEPENCIES LATER
        }
        if (getDependencies) {
            let version = entry.version = (entry.version !== '' && entry.version !== undefined) ? '@' + entry.version : '';
            dependencies.push({name: entry.name, version: version});
        }
    }
}

function getDependDependencies(type, dependencies) {

    _log('getDependDependencies', 0, true);
    let result = false;
    activeHandler.forEach(entry => {


        entry.data.forEach(file => {
            // console.log( type );
            // console.log( file[ type ] );
            let dependeciesEntry = file[type];
            if (dependeciesEntry !== undefined) {
                result = true;
                // console.log( typeof dependeciesEntry);
                //
                // if ( type === 'babel' && file[ type ].length > 0 ) {
                //
                //     if ( file[ type ].indexOf( dependencies.replace( /babel-preset-/, '' ) ) !== -1 ) {
                //         console.log( 888 );
                //         result = true;
                //     }
                // } else {
                //     result = true;
                // }
            }
        })
    });
    return result;
}

function collect(data) {

    //TODO CHECK IN NODEMODULES

    for (let dependencies in  data) {
        if (data.hasOwnProperty(dependencies)) {
            installedModules.push(dependencies);
        }
    }
}

function getFile(filePath) {

    filePath = path.normalize(filePath);

    return new Promise((resolve => {

        fs.exists(filePath, exist => {

            if (exist) {
                resolve(require(filePath));
            } else {
                error = true;
                console.log('ERROR : ' + filePath + ' not found !');
                resolve();
            }

        })
    }));
}

function readFile(filePath) {
    filePath = path.normalize(filePath);
    return new Promise(((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (error, result) => {

                if (error) {
                    console.log('ERROR on read file : ' + filePath);
                    reject(error);
                } else {
                    resolve(result);
                }
            }
        )
    }));
}
