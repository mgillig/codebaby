const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const spawn = require('child_process').spawn;
const cbInterface = require('../components/readline');
// const fork = require( 'child_process' ).fork;
const _log = require('../utils/logger').log('dependencies', false);
const _info = require('../utils/logger').info();


let errors = '';
let output = '';
let saveExact = '';
let cmd;


module.exports = function (list, npmSaveExact, cb) {

    saveExact = npmSaveExact;
    cbInterface.register('install', (input) => {


        if (input.hasOwnProperty('y')) {

            runNpm(list, () => {
                cb({error: false, success: true});
            });
        } else {
            cb({error: false, success: true});
        }

    });

    cmd = 'install';


    console.log('! ' + chalk.yellow('Your current project configuration requires these npm modules \n'));
    list.forEach(entry => {
        console.log(' - ' + entry.name);
    });

    console.log('\n? ' + chalk.yellow('' + cmd + ' these npm modules\n'));
    console.log('-> ' + chalk.yellow('enter') + ': y | n ');

    cbInterface.setContext('install')
};


function runNpm(toInstall, resolve) {

    console.log('\n' + chalk.green(cmd + ' npm modules ') + '...\n');
    if (process.env.PATH.indexOf(':/usr/local/bin') === -1) {
        process.env.PATH += ':/usr/local/bin';
    }

    let packages = [cmd];
    toInstall.forEach(entry => {
        if (entry.name !== '') {
            let version = entry.version = (entry.version && entry.version !== '') ? '@' + entry.version : '';
            packages.push(entry.name + version);
        }
    });

    packages.push('--save-dev');
    if (saveExact) {
        packages.push('-E');
    }
    // packages.push('--no-shrinkwrap');

    // console.log( process.cwd() );
    // console.log( packages );


    // const ls = spawn( 'npm', packages,{cwd:'/Users/michael/_DATA/WORKSPACES/HTML_WORKSPACES/ME/codebaby-next/'} );
    const ls = spawn('npm', packages, {});

    ls.stdout.on('data', (data) => {

        output += `${data}`;

    });

    ls.stderr.on('data', (data) => {


        errors += `${data}`;
        if (data.indexOf('npm ERR!') !== -1) {
            console.log(errors);
            // process.exit();

        }
    });

    ls.on('close', (code) => {


        console.log(output);
        console.log(errors);
        if (code !== 0) {
            console.log(`${code}`);
        }

        resolve({error: false, success: true});

    });

}




