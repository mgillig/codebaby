const fs = require( 'fs' );
const chalk = require( 'chalk' );
const path = require( 'path' );
const child = require( 'child_process' );
const pathUtil = require( '../utils/path' );
const _log = require( '../utils/logger' ).log( 'modules', false );
const _info = require( '../utils/logger' ).info();

module.exports = function( paths, proj, cb ) {


    let pathModule = path.normalize( paths.pathProj + '/modules' );

    _log( paths );
    _log( pathModule );


    let watchedModules = [];
    if ( fs.existsSync( pathModule ) ) {

        _log( 'has modules' );
        // console.log( 'has modules' );
        // console.log( pathModule );

        fs.readdir( pathModule, ( err, result ) => {

            let folders = result.filter( entry => {
                let stat = fs.statSync( path.normalize( pathModule + '/' + entry ) );
                return stat.isDirectory();
            } );


            let promises = folders.map( folder => {
                return new Promise( ( resolve ) => {

                    let module = require( path.normalize( paths.pathCodebaby + '/modules/' + folder + '/index' ) );
                    let moduleConfigPath = path.normalize( paths.pathProj + '/modules/' + folder + '/module.config.js' );

                    if ( fs.existsSync( moduleConfigPath ) ) {
                        let moduleConfig = require( moduleConfigPath );
                        if ( moduleConfig.active && moduleConfig.watch ) {
                            watchedModules.push( {
                                pathWatch: path.normalize( paths.pathWatch + '/modules/' + folder + '/**/*' ),
                                pathAbs: path.normalize( paths.pathProj + '/modules/' + folder ),
                                type: folder,
                                childProcess: moduleConfig.childProcess || false,
                                cb: _onWatch
                            } );
                        }
                    }

                    if ( module.build ) {
                        module.build( paths, proj ).then( () => {
                            resolve();
                        } )
                    } else {
                        console.log( chalk.red( 'A module should have a build function' ) );
                    }
                } )
            } );

            Promise.all( promises ).then( () => cb( watchedModules ) );

        } )
    } else {
        _log( 'has no modules' );
        cb( watchedModules );
    }
};


function _onWatch( evtData ) {

    // console.log( 'modules on data' );
    // console.log( evtData );
    if ( evtData.type === 'init' ) {
        start( evtData.data )
    } else {

        if ( evtData.data.childProcess ) {
            evtData.data.childProcess.kill();
            evtData.data.childProcess = undefined;
            start( evtData.data )
        }else{
            //TODO
        }
    }
}
function start( data ) {

    let pathRel = pathUtil.getPathRelWs( data.pathAbs );
    let file = pathRel + '/server.js';
    let moduleProcess = child.execFile( 'node', [ file ], ( error, stdout, stderr ) => {

        if ( error ) {

            if ( error.signal && error.signal === 'SIGTERM' ) {
                console.log( ' -> ' + chalk.yellow( 'module express closing for reload' ) );
            } else {
                console.log( error );
            }
        }
    } );
    moduleProcess.stdout.on( 'data', ( data ) => {
        console.log( data );
    } );
    data.childProcess = moduleProcess;
}
