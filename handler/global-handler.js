const pp = require('preprocess');
let dataPreprocess;
module.exports = {

    preprocess: _preprocess,
    initPreprocess: _initPreprocess
};

function _initPreprocess(data) {
    dataPreprocess = data;
}


function _preprocess(data, shared, type, _log, next) {
    _log(' - preprocessCode ' + type + ' : ' + data.path);
    // console.log( dataPreprocess );

    if (!shared.fileData.error) {


        data.content = pp.preprocess(data.content, dataPreprocess, {type: type}, (error) => {
            shared.fileData.error = true;
            process.emit('codebaby.error', 'preprocess', error);

        });
    }
    next();
}
