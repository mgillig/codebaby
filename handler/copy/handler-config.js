module.exports = {

    info: "copy",
    active: true,
    type: 'handler-copy',
    fileType: '',
    ignoreTree: [ '_inactive' ],
    invisibleTreeFiles: true,
    fileTypes: [],
    dependencies: []
};
