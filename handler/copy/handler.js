'use strict';
const fs = require('fs');
// const codebaby = require( '../../core/global' );
const filelHandler = require('../../core/components/FileHandler');
const fileUtil = require('../../core/utils/file');
const _log = require('../../core/utils/logger').log('handler-copy', false);
const _info = require('../../core/utils/logger').info();

let stack = [copy];
let stackReplace = [replace];


module.exports = function (handlerData, cb) {

    let compileList = [];
    let fileData = handlerData.fileData;
    let compileStack = stack;
    let read = false;
    let write = false;


    if (handlerData.evt && handlerData.evt.data !== undefined) {
        compileList.push(handlerData.evt.data);
    } else {
        compileList = handlerData.compileList;
    }

    if (fileData.replace.length > 0) {
        read = true;
        write = true;
        compileStack = stackReplace
    }

    filelHandler({
        async: true,
        read: read,
        write: write,
        targets: compileList,
        stack: compileStack,
        shared: handlerData
    }).then(() => {
        cb(handlerData)

    });

};

function replace(data, shared, next) {


    _log(' - replace : ' + data.path);

    if (shared.fileData.replace) {

        if (!shared.error) {
            let str = data.content;
            shared.fileData.replace.forEach(repl => {
                str = str.replace(repl[0], repl[1]);
                //TODO FIND A BETTER WAY
                let pathTest = new RegExp(repl[2]);

                if (pathTest.test(repl[2])) {
                    if (data.to.indexOf(repl[2]) > -1) {
                        // console.log( 'replace  in path : ' +repl[ 2 ]);
                        // console.log( repl );
                        data.to = data.to.replace(repl[0], repl[1]);
                        // console.log( data.to );
                    }
                }
            });
            data.content = str;
            next();
        } else {
            next();
        }

    } else {
        next();
    }
}


function copy(data, shared, next) {

    _log(' - copy ' + data.path);
    if (!data.error) {
        fileUtil.copyFile(data.from, data.to).then(() => {
            next();
        });
    } else {
        next();
    }
}


