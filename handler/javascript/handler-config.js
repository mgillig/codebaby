module.exports = {

    active: true,
    type: 'handler-javascript',
    fileType: '.js',
    fileTypeDest: '.js',
    fileTypes: [ '.js', '.ts', '.jsx','.vue' , '.tsx','.mod.scss','.mod.css' ],
    babelTypes: [ '.jsx', '.vue' ],
    ignoreTree: [ '_inactive', '_compiled', '_compiled_test', '_blank' ,'rollup-config.js', 'tsconfig.json', '.babelrc'],
    invisibleTreeFiles: false,
    ignoreWatch: [ '_inactive', '_blank', '_compiled', '_compiled_test', 'rollup-config.js', 'tsconfig.json' ],


    dependencies: [
        { name: "uglify-js" },
        { name: "uglify-es" },
        { name: "espree" }
    ],

    // these dependencies can be activated in scr.js
    // e.g babel=true
    optionalDependencies: [
        // { flag: 'typescript', dependencie: { name: "typescript" } }
        // { flag: 'babel', dependencie: { name: "babel-core" } },
        // { flag: 'rollup', dependencie: { name: "rollup" } }
    ]
    ,
    templates: {
        // if typescript = true in src.js a tsconfig.js
        // wil be build from this template
        tsconfig: {
            "compilerOptions": {
                "outDir": "_compiled",
                "removeComments": false,
                "target": "es5",
                "module": "es2015",
                "experimentalDecorators": true,
                "moduleResolution": "node",
                "types": [ "node" ],
                "typeRoots": [ "node_modules/@types", ],
                "isolatedModules": true,
                "skipDefaultLibCheck": true,
                "skipLibCheck": true,
                "watch": true
            },
            "include": [
                "**/*.ts"
            ],
            "exclude": [
                "_blank/**/*",
                "_compiled/**/*",
                "**/*.spec.ts"
            ]
        }
        ,
        babel: {
            "plugins": [],
            "presets": [ [ "es2015",
                {
                    "modules": false
                } ] ],
            "ignore": []
        }
    }
};
