'use strict';
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const uglify = require('uglify-js');
const uglifyEs = require('uglify-es');
const babel = require('@babel/core');
const espree = require('espree');

const {preprocess} = require('../global-handler');
const libRollup = require('./libs/libRollup');
/// optional dependies
let libTypescript = require('./libs/libTypescript');
let modernizr;

const codebaby = require('../../core/global');
const include = require('../../core/utils/include');
const filelHandler = require('../../core/components/FileHandler');
const file = require('../../core/utils/file');
const _log = require('../../core/utils/logger').log('handler-javascript', false);
const _info = require('../../core/utils/logger').info();


let stack = [preprocessCode, includeCode, validateCode, minifyCode];
let stackModernizer = [preprocessCode, modernizer, includeCode, validateCode, minifyCode];
let stackBundle = [bundleCode];
let stackBabel = [preprocessCode, babelCode];
let regexInclude = new RegExp('includes.js$');


module.exports = function (handlerData, cb) {

    let fileData = handlerData.fileData;
    let compileStack = stack;
    let compileList = handlerData.compileList;
    let pathBaseRel = handlerData.fileData.paths.baseRelative;


    if (handlerData.evt.type === 'changed' && !handlerData.fileData.concat) {
        compileList = handlerData.evt.compileList;
    }

    if (fileData.typescript) {


        compileTypescript(handlerData, pathBaseRel, (err) => {


            if (fileData.rollup !== undefined) {
                if (!err) {
                    bundleRollup({fileData: fileData, evt: handlerData.evt, cb: handlerData.cb}, cb);

                }
            } else {
                cb();
            }
        });
    } else {

        if (fileData.rollup) {

            if (fileData.babel) {

                compileJs(compileList, stackBabel, true, true, handlerData, () => bundleRollup(handlerData, cb));

            } else {
                bundleRollup(handlerData, cb);
            }

        } else {

            handlerData.fileTree.paths.abs.some(path => {

                if (path.indexOf('modernizr.config.js') > -1 || path.indexOf('includes.js') > -1) {
                    compileStack = stackModernizer;
                    return true;
                }
            });

            // TODO Babel ???
            compileJs(compileList, compileStack, true, true, handlerData, cb);
        }
    }
};


function compileTypescript(handlerData, pathBaseRel, cb) {


    if (handlerData.evt.path === '') {

        // FIRST BUILD COPY ASSETS
        let configTs = {path: codebaby.paths.pathWatch + 'src/' + handlerData.fileData.path + '/tsconfig.json'};
        let assets = handlerData.compileList.filter(entry => entry.fileType !== '.ts');

        assets.forEach(entry => {

            let pathFrom = entry.fromAbs;
            let pathTo = handlerData.fileData.paths.compiled + pathFrom.substr(handlerData.fileData.paths.read.length);
            file.copyFile(pathFrom, pathTo).then(() => {

            })
        });

        libTypescript(configTs, (error) => {
            handlerData.fileData.error = error;
            if (error) {
                process.emit('codebaby.error', 'typescript', output);
            } else {
                cb(error);
            }
        });
    } else if (handlerData.evt.pathData.ext !== '.ts') {

        let pathFrom = handlerData.evt.path;
        let pathTo = handlerData.fileData.paths.compiled + pathFrom.substr(handlerData.fileData.paths.read.length);

        file.copyFile(pathFrom, pathTo).then(() => {
            cb(false)
        })
    }


    // libTypescript(configTs, (error) => {
    //
    //     // console.log( 'error : '+error );
    //     handlerData.fileData.error = error;
    //     if (error) {
    //
    //         process.emit('codebaby.error', 'typescript', output);
    //         // console.log(chalk.red('!typescript error'));
    //         // console.log(output);
    //     } else {
    //         cb(error);
    //     }
    // });
}

function bundleRollup(handlerData, cb) {

    let compileListRollup = [{
        from: handlerData.fileData.rollup.entry,
        to: handlerData.fileData.paths.write
    }];

    compileJs(compileListRollup, stackBundle, false, false, handlerData, cb)

}

function compileJs(targets, stack, read, write, handlerData, cb) {


    filelHandler({
        targets: targets,
        stack: stack,
        read: read,
        write: write,
        concat: handlerData.fileData.concat,
        shared: handlerData
    }).then(() => {

        // process.exit();
        cb(handlerData);
    });

}

// --------------------------------------------------------------------------
// STACK FUNCTIONS
// --------------------------------------------------------------------------
function bundleCode(data, shared, next) {

    _log(' - bundleCode');

    if (!shared.error) {

        let rollupConfig = shared.fileData.rollup;
        let timeStart;
        if (codebaby.state.test) {
            timeStart = new Date().getTime();
        }

        libRollup(rollupConfig, shared.fileData.paths, codebaby.preprocessEnv, (err, result) => {

            if (codebaby.state.test) {
                let timeEnd = new Date().getTime();
                console.log('\n stat : ' + chalk.yellow(timeEnd - timeStart));
            }

            if (err) {
                shared.error = true;
                process.emit('codebaby.error', 'rollup', err);
                next(err);
            } else {
                next();
            }
        });
    } else {
        next();
    }
}

function modernizer(data, shared, next) {

    _log(' - modernizer ' + data.path);


    function updateModernizr(config) {

        modernizr.build(config.settings, function (result) {
            data.content = result;
            next();
        });

    }

    if (!shared.error && data.name === 'modernizr.config.js') {

        // console.log('\n - modernizer ' + data.path);
        delete require.cache[require.resolve(data.fromAbs)];
        let config = require(data.fromAbs);


        if (config.active) {
            if (modernizr === undefined) {
                try {
                    modernizr = require('modernizr');
                    updateModernizr(config)

                } catch (e) {

                    config.active = false;
                    shared.error = true;
                    console.log(chalk.red('\n\n!!!') + chalk.yellow(' module error ') + ': modernizr not found');
                    console.log('-> ' + chalk.yellow('install it manually, or add it as project dependencie in build.js\n') + ' ');
                    next();
                }
            } else {

                updateModernizr(config)
            }
        } else {
            data.content = '';
            next()
        }
    }
    next();
}


function preprocessCode(data, shared, next) {
    // global handler
    preprocess(data, shared, 'js', _log, next);
}

function validateCode(data, shared, next) {

    _log(' - validateCode');


    if (!shared.error && !shared.fileData.vendor) {
        try {
            //https://github.com/eslint/espree
            espree.parse(data.content, {ecmaVersion: 6, sourceType: 'module'});
        }
        catch (error) {

            shared.error = true;
            process.emit('codebaby.error', 'validation', error);
        }
    }
    next();
}

function includeCode(data, shared, next) {

    _log(' - includeCode ' + data.path);

    if (!shared.error && regexInclude.test(data.name)) {

        include(data.content, ['require(', ')'], function (err, result) {

            if (err) {
                shared.error = true;
                process.emit('codebaby.error', 'include : ' + data.path, err);
                next(err);

            } else {
                if (result) {
                    data.content = result;
                }
                next();
            }
        });

    } else {
        next();
    }

}


function minifyCode(data, shared, next) {

    _log(' - minifyCode ' + shared.fileData.path);


    if (!shared.error) {

        let minify = false;
        if (codebaby.state.production) {
            if (shared.fileData.minify !== false) {
                minify = true;
            }
        } else {
            if (shared.fileData.minify) {
                minify = true;
            }
        }

        if (minify) {

            let options;
            let result;
            if (shared.fileData.es5 === undefined || shared.fileData.es5 ===true) {
                // console.log( '\nuglify :' +shared.fileData.path);
                // console.log(  data.path);
                options = shared.fileData.uglify || {};
                result = uglify.minify(data.content, options);
            } else {
                // console.log( '\nuglify es :' +shared.fileData.path);
                // console.log(  data.path);
                options = shared.fileData.uglifyEs || {};
                result = uglifyEs.minify(data.content,options);

            }

            if (!result.error) {
                data.content = result.code;

            } else {

                shared.error = true;
                process.emit('codebaby.error', 'minify : ' + data.path, result.error);
            }
        }
    }
    next()

}

function babelCode(data, shared, next) {

    _log(' - babelCode');


    if (shared.fileData.babel !== undefined) {

        let options = shared.fileData.babel;
        try {
            let result = babel.transform(data.content, options);
            data.content = result.code;
            next();
        } catch (err) {

            process.emit('codebaby.error', 'babel error ', err);
            shared.error = true;
            next(err);
        }
    } else {
        next()
    }

}