const alias = require( 'rollup-plugin-alias' );
const babel = require( 'rollup-plugin-babel' );
const preprocess = require( 'rollup-plugin-preprocess' ).default;
const nodeResolve = require( 'rollup-plugin-node-resolve' );
const commonjs = require( 'rollup-plugin-commonjs' );
const uglify = require( 'rollup-plugin-uglify' );
const replace = require( 'rollup-plugin-replace' );



let sourcemap = true;
let plugins = [
    // babel( {
    //     "exclude": 'node_modules/**',
    //     "presets": [ [ 'es2015', { 'modules': false } ] ],
    //     "plugins": [ "external-helpers" ]
    // } )
];


if ( process.env.NODE_ENV === 'production' ) {
    plugins.push( uglify() );
    sourcemap = false;
}

module.exports = {
    "input": "entry.js",
    "output": {
        "format": 'iife',
        "name": "app",
        "sourcemap": sourcemap
    },
    "plugins": plugins,
    "globals": {
    },
    "external": [
    ]
};