const { rollup } = require( 'rollup' );
const chalk = require( 'chalk' );
const codebaby = require( '../../../core/global' )
// const fileUtil = require( '../../core/utils/file' );
const _log = require( '../../../core/utils/logger' ).log( 'rollup', false );


let cacheCollection = {};

module.exports = function( options, paths, context, cb ) {

    let error = false;
    let errorMess;
    let cacheEntry;

    // console.log( '' );
    // console.log( process.cwd() );
    // console.log( options );

    _log( options );
    cacheEntry = cacheCollection[ options.input ];
    // console.log( options );
    // console.log( 'cacheEntry' );
    // console.log( cacheEntry);


    //TODO MAKE MAPPING BETTER

    if ( cacheEntry === undefined ) {

        if ( options.output.sourcemap ) {
           // options.output.sourcemap = 'inline'
        }

        cacheEntry = {
            optionsInput: {
                input: options.input,
                external: options.external,
                plugins: options.plugins,
                cache: undefined

            },
            optionsOutput: {
                format: options.output.format,
                name: options.output.name,
                globals: options.globals,
                sourcemap: options.output.sourcemap,
                // sourcemapFile:false
            }
        };
        if ( paths.write ) {
            cacheEntry.optionsOutput.file = paths.write;
        }

        // console.log( cacheEntry.optionsOutput );

        cacheCollection[ options.input ] = cacheEntry;

        if ( cacheEntry.optionsInput.onwarn === undefined ) {
            cacheEntry.optionsInput.onwarn = function( warning ) {

                // Skip certain warnings
                // should intercept ... but doesn't in some rollup versions
                // console.log( warning );

                let log = true;

                if ( typeof warning === 'string' ) {
                    if ( warning.indexOf( "The 'this' keyword is equivalent to 'undefined'" ) > -1 ) {
                        log = false;
                    }
                } else {

                    if ( warning.code === 'THIS_IS_UNDEFINED' || warning.code === 'UNUSED_EXTERNAL_IMPORT' ) {
                        log = false;
                    } else if ( warning.code === 'UNRESOLVED_IMPORT' ) {
                        // console.log( warning );
                        log = false;

                        console.log( '\n-> ' + chalk.yellow( 'rollup warning : ' ) );
                        if ( warning.message ) {
                            console.log( warning.message );

                        } else {
                            console.log( warning );
                        }

                        // let path = warning.message.substr( 0, warning.message.indexOf( ' is imported by' ) );
                        // if ( unresolved[ path ] === undefined ) {
                        //     unresolved[ path ] = path;
                        //     if ( warning.message ) {
                        //         console.log( chalk.yellow( warning.message ) );
                        //     } else {
                        //         console.log( chalk.yellow( warning ) );
                        //     }
                        // }

                    }
                }


                if ( log ) {
                    console.log( chalk.yellow( '\n- rollup warning : ' ) );

                    if ( warning.message ) {
                        console.log( warning.message );
                    } else {
                        console.log( chalk.yellow( warning ) );
                    }
                }
            };
        }
    }

    // console.log( cacheEntry );
    // plugins.push( uglify() );
    // console.log( cacheEntry.optionsInput );
    rollup( cacheEntry.optionsInput ).catch( ( e ) => {
        // ROLLUP BUNDLING ERROR
        // console.log( '\n' );
        // console.log( e );
        console.log( '---- catch e ----' );
        console.log( e );
        // console.log( '---- catch e ----' );
        error = true;
        errorMess = e;

        if ( e.message ) {
            errorMess = e.message;
        }


    } ).then( function( bundle ) {

        cacheEntry.optionsInput.cache = bundle;
        // console.log( bundle );
        codebaby.messureEnd( 'bundle rollup' );
        if ( !error ) {

            if ( cacheEntry.optionsOutput.file ) {
                bundle.write( cacheEntry.optionsOutput ).then( result => {
                    codebaby.messureEnd( 'bundle write rollup' );
                    cb( errorMess, result );
                } );
            } else {
                bundle.generate( cacheEntry.optionsOutput ).then( result => {
                    codebaby.messureEnd( 'bundle generate rollup' );
                    cb( errorMess, result );
                } );
            }

        } else {
            // linebreak
            console.log( '' );

            cb( errorMess );
        }
    } );
};