module.exports = _compile;
const spawn = require('child_process').spawn;
const chalk = require('chalk');
const path = require('path');
// const codebaby = require('../../../core/global');
const collection = {};

function _compile(fileData, cb) {

    fileData.pathBase = path.dirname(fileData.path);


    // let pathBase= path.dirname(fileData.path);
    // console.log( pathBase );
    /*

    fileData = {
    lib:'angular' || ,
    path:''
    pathConfig:''
    }
     */
    tsc(fileData, cb)


    // if (fileData.lib === 'angular') {
    //     if (!codebaby.state.production) {
    //         tsc(fileData, cb)
    //     } else {
    //         ngc(fileData, cb)
    //     }
    // } else {
    //
    //
    //     tsc(fileData, cb)
    //
    // }
}

function ngc(fileData, cb) {

    // console.log(fileData);

    if (collection[fileData.path] === undefined) {
        collection[fileData.path] = {path: fileData.path, watched: true};
        // console.log('ngc ');
        let options = [];
        options.push('--project');
        options.push(codebaby.paths.pathWatch + 'src/' + fileData.path + '/tsconfig-aot.json');
        options.push('--w');
        // console.log(options);
        exec('ngc', options, cb);
    }
}


function tsc(fileData, cb) {

    // console.log( fileData );
    if (collection[fileData.path] === undefined) {
        collection[fileData.path] = {path: fileData.path, watched: true};
        // console.log('tsc ');
        let options = [];
        options.push('--project');
        // options.push(codebaby.paths.pathWatch + 'src/' + fileData.path + '/tsconfig.json');
        options.push(fileData.path);

        let result = '';
        // console.log(options);
        exec('tsc', options, output => {

            // console.log('#####');
            result += '\n' + output;
            // console.log(result);
            // console.log(fileData.pathBase);
            // console.log( '----------' );
            // console.log( '----------' );
            // console.log( '----------' );


            if (result.indexOf('Watching for file changes') > -1) {

                let error = false;
                let errorIndex = result.indexOf(fileData.pathBase);
                if (errorIndex > -1) {
                    error = true;
                }
                // fileData.error = error;
                if (error) {
                    process.emit('codebaby.error', 'typescript', result.substr(errorIndex));
                    // console.log(chalk.red('!typescript error'));
                    
                    // console.log( '#######' );
                    // console.log(result.substr(errorIndex));
                    result = '';
                } else {
                    result = '';
                    cb(error);
                }
            }

            // if (output.length > 8) {
            //     let error = false;
            //     if (output.indexOf(fileData.pathBase) > -1) {
            //         error = true;
            //     }
            //     fileData.error = error;
            //     if (error) {
            //         process.emit('codebaby.error', 'typescript', output);
            //         // console.log(chalk.red('!typescript error'));
            //         // console.log(output);
            //     } else {
            //
            //     }
            // }


        });
    }
}

const tsStates = ['File change detected', 'Watching for file changes'];

function exec(cmd, options, cb) {


    const ls = spawn(cmd, options, {});
    let output = '';
    let errors = '';
    let error = false;

    // console.log( options );
    ls.stdout.on('data', (data) => {
        output += data;
        error = false;
        let textChunk = handleOnData('stdout', data, cb);

        if (textChunk.indexOf('File change detected') === -1) {
            cb(textChunk);

        }

        // console.log('---------------------- STOUT ');
        // console.log('---------------------- STOUT ');
        // console.log('---------------------- STOUT ');
        // console.log(textChunk);

        // if (textChunk.indexOf(pathCode) > -1) {
        //     // console.log( chalk.red( textChunk ) );
        //     // error = true;
        //     // console.log( 'tsc - [typescript error]\n' );
        //     console.log(chalk.red(textChunk));
        //     // fileData.error = true;
        //     // process.emit( 'codebaby.error', 'typescript', textChunk );
        //     output = '';
        //     error = true;
        // }
        // if (textChunk.indexOf('Compilation complete') > -1) {
        //
        //     if (codebaby.state.test) {
        //         // let delta2 = new Date().getTime();
        //         // let duration = (delta2 - delta1);
        //         // console.log( 'tsc - compiled in : ' + duration );
        //         // stats.count++;
        //         // stats.time += duration;
        //         // let averageCompileTime = stats.time / stats.count;
        //         // console.log( chalk.yellow( 'ct = ' ) + (averageCompileTime / 1000) );
        //         // delta1 = delta2;
        //     }
        //
        //     output = '';
        //     errors = '';
        //     // cb(error);
        // }
        // cb(error);
    });

    ls.stderr.on('data', (data) => {
        errors += data;
        console.log('STDERR');
        let textChunk = handleOnData('stderr', data, cb);
        if (textChunk.indexOf('Compilation complete') > -1) {
            console.log(888888);
            cb(null);
        }
    });
    ls.on('close', (code) => {

        console.log('CLOSE');
        // console.log( typeof  code );
        // console.log( code );
        if (code === 0) {
            cb(null);
        } else {
            cb('error code : ' + code);
        }
    });
}

function handleOnData(type, data, cb) {
    return data.toString('utf8');

}