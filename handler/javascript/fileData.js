const fileUtil = require('../../core/utils/file');
const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const _log = require('../../core/utils/logger').log('fileData-js', false);
const _info = require('../../core/utils/logger').info();


module.exports = function (data, handlerConfig, build) {


    // if(data.path === 'js/header'){
    //     console.log( data );
    // }
    // console.log(data.rollup);
    // console.log( handlerConfig );

    if (data.babel && handlerConfig) {
        initBabel(data, handlerConfig.templates.babel);
    }

    if (data.rollup && handlerConfig) {
        data.state.bundle = true;
        initRollup(data, handlerConfig);
    }

    if (data.typescript) {
        initTypeScript(data, handlerConfig);
        // data.fileTypes = ['.ts'];
        // console.log( data );
    }

    if (data.rollup === undefined || !data.rollup) {
        if (data.es5 === undefined) {
            data.es5 = true;
        }
    }

    data.uglify = build.uglify || {};
    data.uglifyEs = build.uglifyEs || {};
    // console.log(data.uglify);
    // console.log(data.uglifyEs);
    // console.log('------------');
};

function initBabel(data, defaultConfig) {

    let pathConfig = data.paths.read + '/.babelrc';
    let config = defaultConfig;
    // data.paths.transformTo = path.normalize(data.paths.baseRelative + path.sep + '_compiled');
    // data.paths.transformToAbs = path.normalize(data.paths.read + path.sep + '_compiled');
    // console.log(data);
    if (!fs.existsSync(pathConfig)) {
        let dataStr = JSON.stringify(defaultConfig, null, 1);
        fileUtil.writeFileSync(pathConfig, dataStr);
        console.log('');
        console.log(' -> ' + chalk.green('created a default babel config') + ' : ' + chalk.blue(path.normalize(data.path + '/.babelrc')) + '\n');
        console.log(defaultConfig);
        // console.log( '\n[ edit this file ]\n' );
        // console.log( chalk.yellow( '\n ( edit this file if needed ! ) ' ) );
        console.log('');

    } else {

        let strConfig = fs.readFileSync(pathConfig, 'utf-8');
        try {
            config = JSON.parse(strConfig);
        } catch (e) {

            console.log(e);
        }

    }
    data.babel = config;
}

function initTypeScript(data, handlerConfig) {

    let pathTypescriptConfig = data.paths.read + '/tsconfig.json';
    let typescriptConfig = handlerConfig.templates.tsconfig;
    let configExist = fs.existsSync(pathTypescriptConfig);
    data.transformTo = '_compiled';
    if (!configExist) {
        let dataStr = JSON.stringify(typescriptConfig, null, 1);
        fileUtil.writeFileSync(pathTypescriptConfig, dataStr);
        console.log('');
        console.log(' -> ' + chalk.green('created a ts config') + ' : ' + chalk.blue(path.normalize(data.path + '/tsconfig.json')) + '\n');
        console.log(typescriptConfig);
        // console.log( chalk.yellow( '\n ( edit this file if needed ! ) ' ) );
        console.log('');
    }
}

function initRollup(data, handlerConfig) {

    let pathRollupConfig = data.paths.read + '/rollup-config.js';
    let rollupConfig = handlerConfig.templates.rollup;
    let configExist = fs.existsSync(pathRollupConfig);

    if (configExist) {
        rollupConfig = require(pathRollupConfig);
    } else {

        let rollupTmpl = fs.readFileSync(__dirname + '/assets/rollup-config.js', 'utf-8');
        rollupConfig = require('./assets/rollup-config');

        if (data.babel || data.typescript) {
            // UNCOMMENT BABEL OPTION
            rollupTmpl = rollupTmpl.replace(/\/\//g, '');
            rollupTmpl = rollupTmpl.replace(/entry.js/g, '_compiled/entry.js');
            rollupConfig.input = '_compiled/entry.js';

        }
        // console.log( rollupTmpl );
        // if ( data.typescript ) {
        //     rollupConfig.entry = path.normalize( '_compiled/' + rollupConfig.entry )
        // }

        // let dataStr = JSON.stringify( rollupConfig, null, 1 );
        // let rollupConfigData = 'module.exports =' + dataStr + ';';
        fileUtil.writeFileSync(pathRollupConfig, rollupTmpl);
        console.log('');
        console.log(' -> ' + chalk.green('created a default rollup config') + ' : ' + chalk.blue(path.normalize(data.path + '/rollup-config.js')) + '\n');
        console.log(rollupConfig);
        // console.log( chalk.yellow( '\n ( edit this file if needed ! ) ' ) );
        console.log('');


    }

    data.rollup = rollupConfig;
    // console.log( rollupConfig );
    data.rollup.input = path.normalize(data.paths.read + '/' + data.rollup.input);
    let entry;
    // let pathData = path.parse( data.rollup.entry );
    // console.log( pathData );

    // data.typescript = false;
    // data.react = false;

    if (/\.ts/.test(data.rollup.entry) || /\.tsx/.test(data.rollup.entry)) {
        data.typescript = true;
        data.fileTypes = ['.ts'];
    }

    if (/\.tsx/.test(data.rollup.entry) || /\.jsx/.test(data.rollup.entry)) {
        data.react = true;
        data.fileTypes = ['.jsx'];
    }
    if (data.typescript && data.react) {
        data.fileTypes = ['.jsx', '.tsx'];
    }

    _log('INIT ROLLUP');
    _log('entry : ' + data.rollup.entry);
    _log('pathRollupConfig : ' + pathRollupConfig);


}
;
