'use strict';

const fs = require('fs');
const sass = require('node-sass');
const {preprocess} = require('../global-handler');
const postcss = require('postcss');
const CleanCSS = require('clean-css');
const autoPrefixer = require('autoprefixer');

const codebaby = require('../../core/global');
const filelHandler = require('../../core/components/FileHandler');
const _log = require('../../core/utils/logger').log('handler-scss', false);
const _info = require('../../core/utils/logger').info();


let stack = [sassCode, preprocessCode, postCss, minifyCss];


module.exports = function (handlerData, cb) {

    let compileList;
    if (handlerData.fileData.state.isFile) {
        compileList = handlerData.compileList;
    } else {
        compileList = handlerData.fileTree.files;
    }
    
    // console.log( compileList );

    filelHandler({
        targets: compileList,
        stack: stack,
        read: false,
        shared: handlerData
    }).then(() => {
        cb(handlerData);
    });

};
// --------------------------------------------------------------------------
// STACK FUNCTIONS
// --------------------------------------------------------------------------


function sassCode(data, shared, next) {

    _log(' - sassCode ' + data.path);

    if (!data.error) {
        if (!shared.fileData.state.isFile) {
            data.to = data.to.replace(/.scss/, shared.fileData.fileTypeDest);
        }

        let options = {file: data.fromAbs};
        if (codebaby.state.production) {
            // options[ 'outputStyle' ] = 'compressed'
        } else {

            // options[ 'sourceComments' ] = true
            options['sourceMap'] = true;
            options['sourceMapContents'] = true;
            options['sourceMapEmbed'] = true;
            // options[ 'outFile' ] = to
        }
        sass.render(options, function (err, result) {

            if (err) {
                shared.fileData.error = true;
                data.error = true;
                process.emit('codebaby.error', 'sass', err);
            } else {
                data.content = result.css.toString();
            }
            next();
        })
    } else {
        next()
    }
}

function preprocessCode(data, shared, next) {
    // global handler
    preprocess(data, shared, 'scss', _log, next);
}

function postCss(data, shared, next) {

    _log(' - postCss');

    if (!data.error) {

        // let to = shared.fileData.paths.write;
        // let from = shared.fileData.paths.read;
        // if ( !shared.fileData.state.isFile ) {
        //     to = data.to.replace( /.scss/, '.css' );
        //     from = data.from;
        // }

        // let config = { from: from, to: to };
        let config = {from: undefined};
        // console.log( '##### POST CSS #####' );
        // console.log( config );

        postcss([autoPrefixer])
            .process(data.content, config)
            .then(result => {
                data.content = result.css;
                next();
            })
            .catch(err => {

                shared.fileData.error = true;
                process.emit('codebaby.error', 'postCss', err);
                next();

            })
    } else {
        next()
    }
}

function minifyCss(data, shared, next) {

    _log(' - minifyCss');
    // console.log( data.content );

    if (!data.error && codebaby.state.production) {


        let options = {compatibility: 'ie9'};
        let result = new CleanCSS(options).minify(data.content);

        if (result.errors.length === 0) {
            data.content = result.styles;
        } else {
            process.emit('codebaby.error', 'minifyCss', result.errors);

        }
        next()
    } else {

        next()
    }
}

