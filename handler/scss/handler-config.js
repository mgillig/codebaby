module.exports = {

    active: true,
    type: 'handler-scss',
    fileType: '.scss',
    fileTypeDest: '.css',
    fileTypes: [ '.scss' ],
    // ignore: [ '_inactive' ],
    ignoreTree: [ '_inactive', '_blank' ],
    invisibleTreeFiles: false,
    ignoreWatch: [  '_inactive', '_blank' ],
    observe: '/**/*.scss',

    dependencies: [
        // { name: "preprocess" },
        { name: "postcss" },
        { name: "autoprefixer" },
        { name: "clean-css" },
        { name: "node-sass" }

    ]
};
