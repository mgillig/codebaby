'use strict';
const { execFile, fork, spawn } = require( 'child_process' );
// const fs = require( 'fs' );
// const path = require( 'path' );
const chalk = require( 'chalk' );
const codebaby = require( '../../core/global' );
const bundleTest = require( './libs/bundleTest' );

const argsParser = require( '../../core/utils/args' );
const _log = require( '../../core/utils/logger' ).log( 'handler-test', false );
const _info = require( '../../core/utils/logger' ).info();

let args;
let timeStart;
let jestRunning = false;

module.exports = function( pathTest, type, cb ) {


    _log( pathTest );

    timeStart = new Date().getTime();

    switch ( type ) {
        case 'jasmine':
            console.log( '' );
            // childSpawn( 'node', [ './codebaby/handler/test/libs/jasmine.js', pathTest ], cb );
            childFork( './codebaby/handler/test/libs/jasmine.js', [ pathTest ], cb );
            // childFork( 'node', [ pathTest ], cb );
            break;
        case 'mocha':
            // childExecFile( './node_modules/mocha/bin/mocha', [ pathTest ], cb );
            childFork( './node_modules/mocha/bin/mocha', [ pathTest ], cb );
            // childSpawn( 'mocha', [ pathTest ] );
            break;
        case 'jest':
            if ( !jestRunning ) {
                jestRunning = true;
                childExec( 'jest', [ '--watch', codebaby.build.name + '\\.*\\.compiled\\.*\\.jest\\.test\\.js$' ] );
            }
            break;
        case 'tape':
            // childSpawn( 'node', [ pathTest ], cb );
            childFork( pathTest, [], cb );
            break;
        default:
            console.log( chalk.red( '!!! ' ) + chalk.yellow( 'unknown test' ) + ' : ' + type );

    }
};


function childFork( file, args, cb ) {

    if ( args === undefined ) {
        args = [];
    }

    const options = {
        cwd: process.cwd(),
        env: process.env,
        // stdio: [ 'pipe', 'pipe', 'pipe', 'ipc' ]
    };
    // console.log( options.env.NODE_ENV );
    // process.env.NODE_ENV = 'test';
    // console.log( options.env.NODE_ENV );
    const child = fork( file, args, options );
    child.on( 'message', config => {
        _log( '##### MESSAGE #######' );

        bundleTest( config ).then( (result) => {
            child.send( result )
        } );
    } );
    child.on( 'data', ( data ) => {
        console.log( data.toString( 'utf8' ) );
    } );
    // child.stderr.on( 'data', ( data ) => {
    //     console.log( chalk.red( data ) );
    // } );
    child.on( 'close', ( code ) => {
        if ( cb ) {
            cb();
        }
    } );

}


function childExec( cmd, args ) {


    let childExec = child.exec( cmd + ' ' + args.join( ' ' ) );
    // childExec.stderr.on( 'data', ( data ) => {
    //     console.log( `${data}` );
    // } );
    childExec.stdout.on( 'data', ( data ) => {
        console.log( `${data}` );
    } );


}
function childExecFile( cmd, args, cb ) {


    const defaults = {
        cwd: process.cwd(),
        env: process.env
    };
    // defaults.env.NODE_ENV = 'test';
    console.log( process.env );
    console.log( cmd );
    let childExec = execFile( cmd, args, defaults );
    childExec.stdout.on( 'data', ( data ) => {
        // console.log( `${data}` );
        process.stdout.write( data )
    } );
    childExec.stderr.on( 'data', ( data ) => {
        // console.log( `${data}` );
        process.stdout.write( data )
    } );
    childExec.stdout.on( 'close', ( data ) => {

        if ( cb ) {
            cb();
        }
    } );


}
function childSpawn( cmd, args, cb ) {

    const defaults = {
        cwd: process.cwd(),
        env: process.env
    };
    defaults.env.NODE_ENV = 'test';

    const ls = spawn( cmd, args, defaults );
    ls.stdout.on( 'data', ( data ) => {
        process.stdout.write( data )
    } );
    ls.stderr.on( 'data', ( data ) => {
        console.log( `${data}` );
    } );
    ls.stderr.on( 'close', ( data ) => {
        // let duration = new Date().getTime() - timeStart;
        // console.log( 'duration : ' + duration );
        if ( cb ) {
            cb();
        }
    } );

}
