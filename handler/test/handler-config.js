module.exports = {

    info: "test",
    active: true,

    dependencies: [
        {name: "jasmine"},
        {name: "jasmine-expect", depends: "jasmine"},
        {name: "mocha"},
        {name: "tape"},
        {name: "colored-tape", depends: "tape"},
        {name: "jsdom", depends: ["tape", "jasmine", "mocha"]},
        {name: "jest"},
        {name: "expect.js"}
    ]
};
