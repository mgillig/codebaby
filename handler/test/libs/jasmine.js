const Jasmine = require( 'jasmine' );
let jasmine = new Jasmine();
jasmine.configureDefaultReporter( {
    showColors: true
} );
const JasmineExpect = require( 'jasmine-expect' );
jasmine.execute( [ process.argv[ 2 ] ] );

//https://jasmine.github.io/2.4/node.html#section-Configuration
