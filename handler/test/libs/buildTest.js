let chalk = require('chalk');
let messure = false;
let timeStart;
let bundleId = 1;
let timeout;


module.exports = {
    buildTest: _buildTest,
    endTest: _endTest
};

function _endTest() {
    if (messure) {
        console.log('duration test : ' + chalk.yellow((new Date().getTime() - timeStart)));
    }
    clearTimeout(timeout);
    process.exit();
}

function _buildTest(config, cb) {
    
    console.log( '_buildTest' );
    if (messure) {
        timeStart = new Date().getTime();
    }

     timeout = setTimeout(() => {
        console.log(chalk.yellow('TEST TIMEOUT'));
        console.log('run : complete() in your test to end the test !');
        _endTest();
    }, 10000);

    if (process.send) {

        if (config.name.indexOf('.js') === -1) {
            config.name += '.js';
        }
        config.bundleId = bundleId;
        bundleId++;
        process.send(config);

    }
    //
    // function complete() {
    //     if (messure) {
    //         console.log('duration test : ' + chalk.yellow((new Date().getTime() - timeStart)));
    //     }
    //     clearTimeout(timeout);
    //     process.exit();
    // }

    process.on('message', message => {
        console.log( 'message from parent:', message );
        if (message.type === 'bundle.ready') {
            cb(require('../../../../' + message.path), _endTest);
        }
        if (message.type === 'bundle.error') {
            _endTest();
        }
    });


}