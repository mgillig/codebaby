const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const {rollup} = require('rollup');
const compileTypescript = require('../../javascript/libs/libTypescript');
const codebaby = require('../../../core/global');
const _log = require('../../../core/utils/logger').log('bundleTest', false);
// const _info = require( '../../../core/utils/logger' ).info();

let cacheBundle = {};
let cacheConfig = {};
let stats = false;

//TODO ? Remove codebaby and set path manually
// let indexProj = projDir.indexOf( '/projects/' );
// pathProj = projDir.substring( (indexProj + 10) );
// pathProj = 'projects/' + pathProj.substring( 0, pathProj.indexOf( '/' ) );
// console.log( pathProj );

module.exports = function (config) {


    return new Promise(resolve => {
        let currEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = 'test';

        let typeScript = config.typescript !== '' && config.typescript !== undefined;
        // console.log('### ' + typeScript);

        if (codebaby.paths.pathWatch === undefined) {
            codebaby.paths.pathWatch = config.pathProject;
        }

        if (typeof config.rollup === 'string') {

            if (cacheConfig[config.name]) {
                _log(chalk.green('USE CACHE CONFIG : ') + config.name);
                config.rollup = cacheConfig[config.name];
            } else {
                // console.log( codebaby.paths );
                let pathRollUp = path.normalize('../../../../' + codebaby.paths.pathWatch + '/' + config.rollup);
                let configApp = require(pathRollUp);

                // let configDataStr = fs.readFileSync(codebaby.paths.pathWatch + '/' + config.rollup,'utf8');
                // console.log( configDataStr );
                let configClone = Object.assign({id: 'test'}, configApp);


                // console.log( configClone );

                configClone.input = path.resolve(path.normalize(codebaby.paths.pathWatch + '/' + config.input));
                configClone.output.format = {format: 'cjs', name: 'test', sourcemap: false};
                // if (fs.existsSync(configClone.input)) {
                //     delete require.cache[ require.resolve( configClone.input ) ];
                // }
                cacheConfig[config.name] = configClone;
                config.rollup = cacheConfig[config.name];
                // console.log( configClone );
                // console.log( '######' );
                // console.log( configApp );
            }
        }
        let timeStart;
        if (stats) {
            timeStart = new Date().getTime();
        }
        if (typeScript) {

            let tsConfig = {path: config.pathProject + '/' + config.typescript};

            compileTypescript(tsConfig, err => {

                console.log(8888);
                console.log(err);
                if (!err) {
                    bundle(config).then((result) => {
                        process.env.NODE_ENV = currEnv;
                        if (stats) {
                            let duration = new Date().getTime() - timeStart;
                            console.log('bundle : ' + chalk.yellow(duration));
                        }

                        if (!result.error) {
                            wrappCjsModule(config, result.code).then((pathSave) => {
                                resolve({type: 'bundle.ready', path: pathSave})
                            });
                        } else {
                            delete cacheConfig[config.input];
                            resolve({type: 'bundle.error'})
                        }
                    });
                }
            })


        } else {


            bundle(config).then((result) => {
                process.env.NODE_ENV = currEnv;
                if (stats) {
                    let duration = new Date().getTime() - timeStart;
                    console.log('bundle : ' + chalk.yellow(duration));
                }

                if (!result.error) {
                    wrappCjsModule(config, result.code).then((pathSave) => {
                        resolve({type: 'bundle.ready', path: pathSave})
                    });
                } else {
                    console.log( result );
                    delete cacheConfig[config.input];
                    resolve({type: 'bundle.error'})
                }
            });
        }
    })
};

function bundle(config) {


    // _log( config );
    let configRollup = config.rollup;

    let input = {input: configRollup.input, plugins: configRollup.plugins, external: configRollup.external || []};
    let error = false;

    if (cacheBundle[config.name]) {
        _log(chalk.green('USE CACHE BUNDLE : ') + config.name);
        input.cache = cacheBundle[config.name];
    }

    return new Promise((resolve => {

        rollup(input).catch((e) => {

            console.log('---- catch e ----');
            console.log(e);
            error = true;
            delete  cacheBundle[config.name];
            resolve({error: true})

        }).then(function (bundle) {


            if (!error) {
                cacheBundle[config.name] = bundle;
                bundle.generate({name: config.name, format: 'cjs'}).then(result => {
                    resolve({error: false, code: result.code})
                });
            } else {

                resolve({error: true})
            }
        });
    }))
}

function wrappCjsModule(config, bundleStr) {


    return new Promise(resolve => {

        let inject = config.inject || [];
        let code = bundleStr.split('module.exports');
        code[0] = 'module.exports = function(' + inject.join(',') + '){' + code[0];
        code[1] = code[1].replace(/=/, ' return ');
        code[1] += '};';

        let codeStr = code.join('');
        let name = config.name || 'bundle';
        let pathTestCompiled = codebaby.paths.pathWatch + '/test/_compiled';
        let pathSave = path.normalize(pathTestCompiled + '/' + name);


        if (!fs.existsSync(pathTestCompiled)) {
            fs.mkdirSync(pathTestCompiled);
        }
        fs.writeFile(pathSave, codeStr, 'utf-8', (err) => {

            if (!err) {
                resolve(pathSave);
            } else {
                console.log(err);
            }
        })
    })
}