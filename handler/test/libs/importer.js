const path = require( 'path' );
const libRollup = require( '../../javascript/libs/libRollup' );
const ROLLUP = 'ROLLUP';
module.exports = {

    ROLLUP: ROLLUP
    ,
    init: function( pathHost, type, pathConfig ) {

        //TODO CLONE
        let pathWs = path.resolve( __dirname, '../../../../' );
        let config = require( pathWs + '/' + pathConfig );


        return function( path ) {

            return new Promise( resolve => {
                // console.log( config );
                if ( type === ROLLUP ) {
                    config.input = path;
                    config.output = { format: 'cjs' };
                    libRollup( config, {}, null, ( err, result ) => {


                        if ( err ) {
                            console.log( err );
                        } else {
                            // console.log( result );
                            let code = eval( result.code );
                            // console.log( code );
                            resolve( code );
                        }
                    } )

                }else{
                    
                    console.log( 'UNKNOWN BUNDLE TYPE' );
                }

            } );
        }
    }
}