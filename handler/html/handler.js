'use strict';
const path = require('path');
const ejs = require('ejs');
const minify = require('html-minifier').minify;
const {preprocess} = require('../global-handler');

const codebaby = require('../../core/global');
const filelHandler = require('../../core/components/FileHandler');
const fileUtil = require('../../core/utils/file');
const _log = require('../../core/utils/logger').log('handler-html', false);


let replaceList;
let stack = [preprocessCode, minifyHtml];
let stackEjs = [htmlEsj, preprocessCode, minifyHtml];


module.exports = function (handlerData, cb) {

    let fileData = handlerData.fileData;
    let compileList = handlerData.compileList.filter(entry => entry.fileType === handlerData.fileData.fileType);
    replaceList = codebaby.preprocessReplace;
    // console.log(handlerData.fileTree);

    if (fileData.ejs) {

        getMainJson(fileData.from + fileData.path, json => {

            let filesJson = handlerData.fileTree.fileTypes['json'];

            compileHtml(compileList, stackEjs, {
                fileData: handlerData.fileData,
                fileTree: handlerData.fileTree,
                evt: handlerData.evt,
                json: json,
                filesJson: filesJson
            }, false, () => {
                cb(handlerData);
            })
        });

    } else {
        compileHtml(compileList, stack, handlerData, true, cb)
    }
};

function compileHtml(compileList, stack, shared, read, cb) {

    if (shared.evt.pathData) {
        if (shared.evt.pathData.ext !== '.html' || shared.evt.pathData.dir.indexOf('/includes/') > -1) {
            shared.evt.data = undefined;
        }
    } else {
        compileList = compileList.filter(entry => entry.from.indexOf('/includes/') === -1);
    }

    filelHandler({
        targets: compileList,
        stack: stack,
        read: read,
        concat: shared.fileData.concat,
        shared: shared
    }).then(() => {

        cb(shared)
    });

}

// --------------------------------------------------------------------------
// STACK FUNCTIONS
// --------------------------------------------------------------------------
function htmlEsj(stackData, shared, next) {
    _log(' - htmlEsj : ' + stackData.path);


    if (!stackData.error) {
        // console.log( 'get json : ' + stackData.path );
        getJson(stackData, shared, json => {

            if (json === undefined) {
                json = shared.json;
            } else {
                _log(' - merge json ' + stackData.path);
                let cloneGlobal = Object.assign({}, shared.json);
                json = Object.assign(cloneGlobal, json);

            }


            ejs.renderFile(stackData.from, json, {"rmWhitespace": false}, function (error, html) {

                if (error) {
                    process.emit('codebaby.error', 'Html esj  Error', error);
                } else {
                    replaceList.forEach(entry => {
                        html = html.replace(entry.regex, entry.val);
                    });

                    stackData.content = html;
                    next()
                }
            });
        });
    } else {
        next()
    }
}

function preprocessCode(data, shared, next) {
    // global handler
    preprocess(data, shared, 'html', _log, next);
}

function minifyHtml(stackData, shared, next) {

    _log(' - minifyHtml');
    if (!stackData.error) {
        try {
            stackData.content = minify(stackData.content, {
                // removeAttributeQuotes: true,
                collapseWhitespace: true,
                // conservativeCollapse: true,
                // customAttrAssign:[],
                removeComments: true,
                // ignoreCustomComments: [ /^!/ ]
            });

            next();
        } catch (error) {
            // shared.fileData.onError( 'html minifiy error', error );
            process.emit('codebaby.error', 'html minifiy error', error);

        }
    } else {
        next();
    }
}

// --------------------------------------------------------------------------
// HELPER
// --------------------------------------------------------------------------
function getMainJson(pathJson, cb) {

    fileUtil.getJson(path.resolve(pathJson + path.sep + '_data-ejs.json'), (err, data) => {

        if (err) {

            if (err.error === 'file not found !') {
                console.log(' warn - no _data-ejs.json found ! ');
            } else {
                console.log(err);
            }
            data = {};
        }
        cb(data);
    });
}

function getJson(stackData, shared, next) {
    _log(' - getDataJson : ' + stackData.path);

    let pathJson = stackData.from.replace(/\.html$/, '.json');
    let jsonData = shared.fileTree.collection.rel[path.resolve(path.sep + pathJson)];

    if (jsonData !== undefined) {

        fileUtil.getJson(pathJson, (error, result) => {

            if (error) {
                process.emit('codebaby.error', 'Json Error', error);
                next();
            } else {
                next(result);
            }
        })
    } else {
        next();
    }

}
