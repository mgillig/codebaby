module.exports = {

    type: "handler-html",
    active: true,
    fileType: '.html',
    fileTypes: [ '.html', '.json' ],
    // ignore: [ '_inactive' ],
    ignoreTree: [ '_inactive', 'includes' ],
    invisibleTreeFiles: false,
    ignoreWatch: [ '_inactive' ],
    observe: '**/*.json',

    dependencies: [
        { name: "ejs" },
        // { name: "preprocess" },
        { name: "html-minifier" }
    ]
};
