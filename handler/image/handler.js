'use strict';
const path = require('path');
const fs = require('fs');

const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminOptipng = require('imagemin-optipng');
const imageminSvgo = require('imagemin-svgo');


const codebaby = require('../../core/global');
const filelHandler = require('../../core/components/FileHandler');
const fileUtil = require('../../core/utils/file');
const _log = require('../../core/utils/logger').log('handler-img', false);
const _info = require('../../core/utils/logger').info();

let folderOpt = '_optimized';
let stackDefault = [check, optimize, copyDist];
let stackUpdate = [optimize, copyDist];
let stackUnlink = [unlink];
const plugins = [
    imageminJpegtran(),
    imageminGifsicle(),
    imageminOptipng(),
    imageminSvgo({
        plugins: [
            {removeViewBox: false}
        ]
    })
]


module.exports = function (handlerData, cb) {


    let fileData = handlerData.fileData;
    let stack;
    let compileList;

    switch (handlerData.evt.type) {

        case 'changed':
            stack = stackUpdate;
            compileList = handlerData.evt.compileList;
            break;
        case 'added':
            stack = stackUpdate;
            compileList = handlerData.evt.compileList;
            break;
        case 'unlink':
            stack = stackUnlink;
            compileList = [{path: handlerData.evt.path}];

            break;
        default:
            stack = stackDefault;
            compileList = handlerData.compileList
    }


    handlerData.pathOpt = path.normalize(fileData.paths.read + '/' + folderOpt + '/');
    handlerData.pathFromRelative = path.normalize(codebaby.paths.pathWatch + '/' + codebaby.build.project.srcFolder + '/' + fileData.path + '/');
    handlerData.exist = false;

    // console.log(handlerData.pathOpt);
    // console.log(handlerData.pathFromRelative);
    // console.log('---');
    // console.log(compileList);

    fileUtil.mkdir(handlerData.pathFromRelative + folderOpt).then(() => {

        filelHandler({
            targets: compileList,
            stack: stack.concat(),
            async: false,
            read: false,
            write: false,
            shared: handlerData,
        }).then(() => {

            cb(handlerData)
        });
    });
};


// --------------------------------------------------------------------------
// STACK FUNCTIONS
// --------------------------------------------------------------------------


function check(data, shared, next) {

    _log(' - check : ' + data.path);

    fs.exists(shared.pathOpt + data.path, exist => {
        shared.exist = exist;
        next();
    });
}

function unlink(data, shared, next) {

    _log(' - unlink : ' + data.path);

    let imgOpt = data.path.substr(shared.fileData.paths.baseRelative.length);

    if (fs.existsSync(shared.pathOpt + imgOpt)) {
        fs.unlinkSync(shared.pathOpt + imgOpt);
    }
    next();
}


function optimize(data, shared, next) {

    _log(' - optimize exist :  ' + shared.exist + ' , ' + data.path);

    if (!data.error) {

        if (!shared.exist) {

            let filePath = path.parse(data.path);

            imagemin([shared.pathFromRelative + data.path], shared.pathOpt + filePath.dir, {
                plugins: plugins
            }).then(files => next())


                .catch(e => {
                    console.log('error optimize');
                    console.log(e);
                    next()

                });
        } else {
            next()
        }
    } else {
        next();
    }
}


function copyDist(data, shared, next) {

    _log(' - copyDist '+data.path);

    if (!data.error) {

        let pathFrom = shared.pathOpt + data.path;

        if (shared.evt.type === 'changed') {
            pathFrom = shared.pathOpt + shared.evt.path.substr(shared.fileData.paths.read.length + 1);
        }

        fileUtil.copyFile(pathFrom, data.to).then(() => next());
    } else {
        next();
    }
}


