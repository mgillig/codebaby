module.exports = {

    active: true,
    type: 'handler-image',
    fileType: '',
    ignoreTree: [ '_inactive','_optimized' ],
    ignoreWatch: [ '_inactive' ,'_optimized'],
    invisibleTreeFiles: false,
    fileTypes: [ '.png','.jpg','.jpeg','.gif','.svg' ],

    dependencies: [
        { name: "imagemin" },
        { name: "imagemin-jpegtran" },
        { name: "imagemin-gifsicle" },
        { name: "imagemin-optipng" },
        { name: "imagemin-svgo" }

    ]
};
