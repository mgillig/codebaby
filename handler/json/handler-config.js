module.exports = {

    type: 'handler-json',
    fileType: '.json',
    fileTypes: [ '.json' ],
    // ignore: [ '_inactive' ],
    ignoreTree: [ '_inactive', 'includes' ],
    invisibleTreeFiles: false,
    ignoreWatch: [ '_inactive' ],

    dependencies: [
        { name: "jsonminify" }


    ]
};
