'use strict';

const jsonminify = require("jsonminify");
// const ejs = require('ejs');
const codebaby = require('../../core/global');
const filelHandler = require('../../core/components/FileHandler');

const _log = require('../../core/utils/logger').log('handler-json', false);
const _info = require('../../core/utils/logger').info();


let stack = [minify];


module.exports = function (handlerData, cb) {


    let compileList = handlerData.compileList;

    if (handlerData.evt.type === 'changed') {
        compileList = handlerData.evt.compileList;
    }

    filelHandler({
        targets: compileList,
        stack: stack,
        shared: handlerData
    }).then(() => {

        cb(handlerData);
    });

};

// --------------------------------------------------------------------------
// STACK FUNCTIONS
// --------------------------------------------------------------------------

function minify(data, shared, next) {

    _log(' - minify');

    if (!data.error) {
        if (codebaby.state.production || shared.fileData.minify) {
            data.content = jsonminify(data.content)
        }
    }
    next()
}

